21
库、怎麼办呢...好好思考啊！
杰克(ジェイク)挥舞着剑睥睨着我。
现在还没有袭击过来的样子。一边發出越来越大的笑声、边挥动长剑斩裂周围的桌布和窗帘而發出了很大的声响。
明显不是正常的傢伙。
巡逻的骑士也都聚集了过来。

在这裡压制杰克是能做到的。但是这必然会引發骚乱。
也不得不让来宾退避。
由此导致舞会的终止将会是侯爵家前所未有的耻辱。
敌人的目的就是这个吗...
[杰克！快住手！]

埃文斯(エバンス)伯出声制止、(但)却没有(得到)回应。

一个身穿骑士礼装的身姿疾驰而来。那是一个有着一头黑色短发、眯眯眼的青年。
他是今天刚从外地城堡赶来赴任的伽雷斯(ガレス)的直属部下。现在是掌握着警卫指挥权的人。

[大小姐、请退下。(由)我们制服他]
[父亲大人怎麼说?]
[(事已至此)、舞会只能终止了。这麼说的]
[库呜....!]

不能因为拘泥於名誉而让来宾受伤。这对杰克来说亦是如此。
我紧咬着嘴唇、
[大小姐]

虽然伽雷斯(看起来)很着急的样子、但是我却被他的佩剑吸引了注意力。
抬起头望了一眼还在挥剑的杰克。
在站在远处观望的客人的最前排可以看到被阿列克斯(アレクス)支撑着的父亲大人和西里斯(シリス)的身影。
一整天都在勉强着自己的父亲大人果然还是坚持不住了、脸色很差。

情况越来越糟了、这时候如果父亲大人倒下就麻烦了。变成那样就不是我能解决的局面了。
必须得现在了结。
[卡莉斯托(カリスト)、剑给我]
[哈啊......?]
[把(你的)剑给我、(另外)为了不让来宾靠近、将骑士配置到周围。]
[但是...]
[没事的、交给我吧。]
我直视着卡莉斯托的黑瞳。

[…我明白了。但是一旦判断是危险的状况、(我们会立刻)杀了他。对於我们来说大小姐的命才是最重要的。]
卡莉斯托单膝跪地後、将腰际的佩剑交给了我。
来宾们看到这顿时议论纷纷。
[还有、在走廊的黑铠骑士恐怕就是主谋者。去逮捕他。]
卡莉斯托颔首之後便离开了。
而骑士们则(听从卡莉斯托的指示)并列着从客席长廊退下了。
是因为有骑士的守护而安心了吗?事到如今客人们才以惊讶的语调开始议论突发事件。

[你想做什麼呢?]
[嘛~居然拿着剑什麼的、真是勇敢的大小姐呢。]
[那个胡闹的男人是谁?]
[埃文斯伯好像喊过(他的名字)....]
议论声逐渐变大了。

我就这样举着剑鞘向父亲大人和西里斯拔出长剑、行了一礼。
父亲大人惊讶的瞪大了眼睛。
不知道有没有理解我的意思.....
西里斯也微微颔首。
我回以一个微笑、他也明白了吧。

西里斯走上前、然後转过身对着吵闹的长廊行礼。
[撒！今晚来参加宴会的绅士、淑女们！今日为您们準备了一点小小的惊喜。]
西里斯指向夸张的摆动身体、正挥剑砍着桌子的杰克。
[在那边行使暴力的恶汉将由利姆威尔(リムウェア)的千金完美制服！(请诸位)欣赏华丽的剑舞！]
宛如对[哦哦哦~~！]的發出欢呼声的长廊回应一般、我低下了头。
事到如今、与其向来宾解释是我方没能预防暴徒的恶行、还不如说这本来就是预定好的。

向正确理解了我的意图的西里斯送去(感谢的)视线。
幸好發生这件事的时候他在场。真是不得不感谢那傢伙。
虽然向西里斯借人情是很危险的事情。嘛、也没办法。
现在先集中精神于眼前的敌人吧。
(那麽)、舞台已经準备好了。
之後只要我表演一场华丽的战鬥就行了。

拔出长剑、将白色的刃尖指向发狂的杰克。
[撒！我来做你的对手！放马过来吧！]

◇◆◇◆◇◆◇◆◇◆
乐团伽娜蒂起了平静轻快的曲子。
配合着音乐、我举起剑摆好架势接近杰克。
真剑沉得要死！
(这就是)能够伤人的重量。
紧紧的握住剑柄、
将我锁定为目标的杰克發出疯狂的笑声袭击而来。
观众席發出一阵悲鸣声。
面对挥下的剑、我扭转半身躲开。
这根本不是什麼剑术、只是任由力量乱挥一气、
(剑)划过地板而發出了巨大的声响。
但是正因为是外行人在乱挥、完全读不出剑的轨迹。

满含杀意的剑击从各个方向刺出、每一击都拥有一击致命的威力。
钻对方的空子解决掉倒是很容易。
但是我的目的并不是杀了杰克。
配合着杰克的攻击、我也以剑迎击。
(杰克)由於冲击而後退了幾步。
观众群响起一阵喝彩声。
为了迎合对手、我挥舞着剑一一弹开。
横斩、逆袈裟斩、所有的一切。

额头上的汗水飘洒开来。
女仆们幸苦打理好的头髮散落的黏在满是汗水的脸颊上。
剑和剑相互交织、巨大的金属音鸣彻大厅。
数回合的交手(之後)。
趁着杰克由於剑刃的交锋而失去平衡的瞬间、我也连忙後退调整呼吸。
然而、好像不允许我这麽做一样、杰克压低身姿突击过来。
令人不快的笑声不断的传来。
这傢伙难道不知道累吗?

我轻盈的一个纵身、用剑柄敲击毫无防备的背部。
因为肺部空气逆流而發出呻吟声的杰克猛烈的倒在地上。
虽然算不上致命伤、但是突然受到如此重击、想必还是很难受的。
做过头了吗?
这麼想的一瞬、像是野兽一样的杰克再次朝我砍来。
立刻架起剑挡住、眼前迸發钢铁撞击的火花。
由於那强力的一击而双手麻痹的我瞬间被压倒、体势崩溃了。
可恶、糟糕！大意了...!

虽然想赶紧後撤、但穿着高跟鞋无法如意的行动。
横斩的一闪再次袭来。
瞬间、反应慢了一拍。
观众群發出阵阵悲鸣。
好像火烧一样的疼痛在左大腿扩散开来。
[hihi、hya 哈哈哈哈]
杰克發出疯狂的恶心笑声。
我保持着姿势、只以眼睛看向大腿。
被剑斩裂的伤口流出的血渗透礼服染成了血红色。
从碎裂的礼服处看到、流血的脚的伤口并不是很深。
只是伤到了一些外皮。
还能再战。
这次轮到我了。
我无视疼痛蹬了一下地面。

从正面突袭而来的剑刃上还附着我的血液、我扭动身体轴心躲开一击。
保持着突击的体势以剑柄突向杰克的胸口。
面对即使身体弯成く形也依然展开反击的杰克、我立刻後撤幾步、然後再次突击。
顺着杰克的剑钻入他的怀内、全力的用剑身击打他的手腕。
杰克不禁後退了幾步。
随後立刻以他的身体为轴心绕到背後、沉下腰收回剑刃等待机会。
(杰克)果然马上抽回剑向後挥下。
意料之内！

我用渗着血的双脚全力向地面踏出。
在这一击上堵上一切、全力的斩击在空中疾驰。
然後....
一声高亢的金属音。
一瞬之後、剑落到地上的声音在重归寂静的大厅中回响。
对於如预料一样被打飞了剑而呆滞的杰克、我轻声嘟囔道
[抱歉了。]

立刻沉下腰放出一击扫堂腿、
仍然呆立着的杰克就轻易的倒下了。
我则对他刺出剑击。

为了平復混乱的呼吸、 深呼吸了幾次。
[暴徒已经被制服了！骑士哟！快把这个傢伙带走！]
在重归静寂的大厅内响起了我堂堂的话语。
先是零落的掌声响起、然後逐渐越来越大。
颤抖着肩膀呼吸的同时、我对欢呼着的观众们低下头。
一时间掌声更大了。
卡莉斯托跑了过来。
父亲大人和西里斯也过来了。
事到如今反而感到腿部的伤口阵阵刺痛。


杰克已经被骑士们带走了。
自从手中的剑掉落以後就一下子老实下来了。
已经没关係了吗....?恢復清醒了吗?...
[太乱来了、伽娜蒂。吓坏我了、伤势怎麼样?]
[没事的、父亲大人。只是擦伤罢了。]
[...是吗。不过真是干的不错。现在就接受治疗好好休息一下吧。]
父亲大人轻轻的触摸了我满是汗水的脸颊。
[对不起。]
然後轻声说了一句。

我则无言的点点头。
[我留下来善後。阿列克斯带伽娜蒂去休息室後传唤莉莉安娜(前去照料)。卡莉斯托派护卫去伽娜蒂的房间加强警备。]
卡莉斯托(立刻)低下头离开了。
阿列克斯则搀扶着我。
[但是、父亲大人的身体也....]
[别说蠢话。]
父亲大人直视着我。
[女兒都这麽拼命努力了、作为父亲怎麼能去休息！这裡就交给我吧。]
父亲大人微笑着说了这些话之後就转身走向来宾的方向。
贵族诸侯们马上都聚集到了父亲大人周围。
[漂亮的剑技！真是令人佩服！]
[相当威风哦！]
[连血浆都準备了真是周到！]
[明明还这麽年轻漂亮、真是令人憧憬啊！]
[务必让我和伽娜蒂大人聊几句。]
[哈哈哈、诸位满意就是再好不过了。伽娜蒂也会高兴的吧。但是要面见诸位还需要整理一下仪容。对女兒的赞赏就由为父代受了。]
父亲大人明亮通透的声音传来。
我边听着父亲大人的话语、边搭着阿列克斯离开了会场。

◇◆◇◆◇◆◇◆◇◆
进到附近的休息室後、紧张和疲劳感瞬时袭来。我一屁股坐在了椅子上。
阿列克斯立刻拿来湿毛巾为我擦拭伤口附近的血液
疼....
伤口还在流着血。
好疼..
好像火烧一样的痛。
啊啊、真是失败呢。

(贵族们)似乎误以为是表演用的血浆了、但是继续待在那裡一旦暴露的话、
把袭击替换成演出的计划就会失败了。
[真是乱来]
靠在墙边的西里斯很开心似的笑着。
[但是就临机应变来说算是很不错的主意了。拜这所赐、对方一定也很恼怒吧。]

黑骑士
恐怕幕後黑手是拉布雷(ラブレ)男爵。
虽然让卡莉斯托去逮捕黑骑士了、但八成会失败吧。

突然、バタン的门被打开了。
以很厲害的气势冲进来的是莉莉安娜和女仆军团。
平时不敲门就会生气的莉莉安娜(现在)也顾不上这些了。
当然、这是不会说出口的事情。

[伽娜蒂大人、平安无事吗！]
[血....血...血、、！脚上有血！]
女仆军团發出盖过莉莉安娜喊声的悲鸣。
我微笑着。
[没事的、莉莉安娜桑、只是一点擦伤.....]
[马上开始治疗。去準备热水和毛巾！]
[不...没事的...]
[伽娜蒂大人！请安分点。]
莉莉安娜取出小刀、大幅撕开了我的礼服裙子。
大概在下身往下10厘米左右吧。大腿上露出一个横向的一文字伤口、鲜血顺着腿流下来。
伤口虽然浅但很宽。因此出血量很大。
我这麽冷静的分析着...


热水一端过来。(莉莉安娜立刻)用濡湿的毛巾擦拭伤口。
果然碰到的话还是很痛...
[现在开始消毒、稍微会有点痛。]
莉莉安娜从药箱中拿出一个小瓶、轻轻涂在我的伤口处。
[啊、库呜..]
呜呜..好痛..
我不禁这麽想着抬起头将目光从伤口上移开。
呜呜呜呜...感觉要哭出来了..呜呜呜..

在忍耐着痛楚时、不经意间看到了盯着我的西里斯。
那个视线直直的死盯着我腿上的伤口、总觉得眼神有点吓人。
是觉得我的反应奇怪吗?莉莉安娜桑回过头去。
啊、和西里斯对上眼了。
[在做什麼?]
静静的、但是含着怒气的莉莉安娜站起身。
[咿呀、嘛..在为伽娜蒂平安无事高兴呢。]
西里斯哈哈哈的笑着。
[...男士请出去。]
莉莉安娜桑好可怕...。


西里斯像是恶作剧被發现了的孩子一样挠着头出去了。
随着西里斯的离场、阿列克斯端来了新的热水。
[阿列克斯就可以吗?]
试着问了一下。
莉莉安娜桑瞥了我一眼说
[阿列克斯是执事、并不算男人。]
[嚯嚯嚯]
阿列克斯像是愉快似的笑出声。

执事是没有性别的吗.....
我在疲劳之中、奇妙的感慨着。
