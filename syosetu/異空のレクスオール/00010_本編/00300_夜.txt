
入夜。
在晚上还持续着嘈杂的宿屋街也完全安静下来，静寂包围着四周。
到了太阳落下的这个时间，要去街道外面的人都已经做好了野营的準备，除非非常着急的人，基本没人会穿过街道大门了。
正所谓对揽客来说“浪费时间”的时段，因此在外面拉人的店员在这个时间都回到宿屋裡面帮忙幹活了。
当然，真的要去拉的话，说不定还能拉到一两个客人，但效率上讲实在是太低了。
有餘裕让员工这麼闲逛的宿屋，是不存在的。
……嘛，也因此，金之皇冠亭附近也一下子安静了下来。
具体形容的话，在旁边的床上睡着的艾丽莎的呼吸声都能传到要的耳边……不过，要还实在是睡不着而在床上转来转去。
蜡烛已经熄灭，也关上了木窗，所以房间裡很暗。
但是，即便如此也多少渗进来的些微的光，也在向眼睛习惯了黑暗的要展示着房间的模样。

“……异世界、吗”

莫名其妙地来到这个世界的，第二个夜晚。
如果没遇到艾丽莎的话，要怕是连能不能这麼踏实地躺在床上都是未知数。
嘛，虽然床也只是硬木板床而已……但这也已经完全好过睡在野外，一想到要身无分文的现状就更是对艾丽莎感激不尽。
而艾丽莎本人，则正带着一副舒服的睡脸沉浸於梦乡之中。
可能是因为身處於紧紧插着门闩的房间裡吧，实在是安稳的睡脸。
虽然在异世界的生活一直是被艾丽莎带着走，但并不算坏。
渡过了那个赤红的夜晚，令人恐惧的未来已然不再存在。
这样的话，就这样开始所谓的冒险者生活也不失为一种选择。
……虽然艾丽莎说过“直到能某种程度自立为止”，但可以的话就算在那之後也。
心中怀抱这样的思虑，要望向艾丽莎的睡脸。

“……咕咻”

听到这奇妙的梦话，想着“到底是梦到了什麼”，要也闭上了双眼。
肯定，明天也会很忙的。如果说出没睡够这种话肯定会被艾丽莎以真没办法的眼神去看的。

“……睡吧”

仿佛自我暗示地低喃着，要闭上了眼睛。
这麼一来，或许自己比想象中还要累吧……要的眼睑自然地愈发沉重，令人惬意的困意袭了上来。
要就这样开始前往梦中的世界……但是，楼下传来的敲门声把要拉了回来。
这个时间宿屋的门是关着的，就是在敲那扇大门的声音。
以旅客来说算是相当粗暴的敲法，但也就是那麼一回事吧，在要这麼想着的时候，艾丽莎也对声音起了反应，睁眼坐了起来。

“……發生了什麼”
“什麼的，不是客人吗？”
“用那种敲法还不会被自警团赶出去的傢伙的话，只有一种啊”

理解不了艾丽莎话裡的意思，要不禁歪了歪头，同时下面传来了在说些什麼的声音。
虽然听不太清，但恐怕是到访的什麼人和宿屋主人之间的对话吧。
接着传来咚咔咚咔，很有重量的什麼跑上台阶的声音……看到艾丽莎将在床边放着的剑拿在手上，要也準备拽出放在床下的弓。
但是，在要取出弓之前，房间的门就被粗暴地敲了起来，应该是男性的粗暴声音从门对门响起。

“冒险者艾丽莎！我等是修涅依鲁骑士团！快点出来！”

想开门，但门闩卡在中间，所以就只好这样了吧。
面对重新响起的粗暴的敲门声，艾丽莎将剑放在床上，叹了口气。

“要呆在那裡不要动。看来是有麻烦事了”
“麻、麻烦事是”
“虽然大致能猜到。不过由他们来说应该比较快吧”

这麼说着，艾丽莎反敲了敲门。
接着敲门的声音止住，艾丽莎把门闩解开扔到一边，打开了门。
门对面站着数名身着金属铠完全武装的骑士，飘散着相当紧张的气氛。

“……这麼晚了，骑士大人们特意驾临是有何贵幹？那件事的话我应该已经向自警团传达了啊”
“那件事确有听闻。但是，又有别的报告传上来了啊……你有着藏匿地下城，并且引起决坏致使普西路村毁灭的嫌疑”
“什……！”

听到骑士的台词要惊讶的叫出声来。
藏匿地下城的应该是在森林裡死掉的冒险者们，而与其同谋的也是普西路村的村长。
明明与艾丽莎八竿子打不着的事，为什麼会变成这样？

“……那个男的是怎麼回事。同伴吗？”
“只是个被盗贼剥光的倒霉蛋而已。和我顺路所以想让我把他带到王都的熟人那裡而已”
“是吗”

骑士这麼说着，从怀裡掏出皮袋朝要那边扔去。
咔嘁、唦啦地响着皮袋似乎装着不少的钱币，但是要无法理解他为什麼会这麼做。

“这个女的无法陪你回去了。但虽然这麼说，也不建议你停留在之後会变得动荡的这条街上。用那些钱做好準备回王都去吧”
“队长，也没必要做到那种地步……说到底还不清楚那傢伙是不是真的不是同伙”
“肃静。难道说要再追加上你们的钱包吗？”

这句话让其他的骑士沉默下来，被称为队长的骑士再次看向艾丽莎。

“看在你人道的行为上，就不拘束你了。希望你能老老实实和我们去骑士团”
“我明白了。但是，我可什麼都没做哦”
“审讯裡也包含了这一点。走吧”

看到艾丽莎要和骑士们一起离开房间，要站起来準备追过去……但是，在艾丽莎强烈的视线下要停下了动作。

“……我没事的。不过，如果我回不来的话”
“喂，快点走！”

被骑士拽着，艾丽莎的身姿从要的视野中消失，门被粗暴地关上了。
该怎麼办，究竟怎麼做才是对的。
要什麼都不清楚。
可能就算在这裡反抗，也不会有什麼结果。
但是，这样的话该怎麼办才好？
不明白。什麼都不明白。
变得混乱的思考与无处释放的愤怒。
就连吼叫也无法宣泄的，整理不清的感情。

“……！”

在沉默地看着地板的要视野的一角……黄金之弓闪耀着钝色的光辉。
同时不知为何，那个奇怪的宝石商二人组，浮现在要的脑海之中。
