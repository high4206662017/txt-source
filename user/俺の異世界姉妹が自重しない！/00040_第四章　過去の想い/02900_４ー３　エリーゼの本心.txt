

几小时後，我们平安无事的到达了苏菲尔家。
顺便说一下，成员是我、爱丽丝、索菲亚还有艾尔莎和米莉妈妈五个人。克蕾雅姐因为工作的缘故留在古兰歇斯家。
虽然我也不是在玩，但是把工作都推给克蕾雅姐真心抱歉。
艾莉婕桑这件事处理完之後，让克蕾雅姐稍微休息一会儿吧。想着这些东西的时候，埃里克先生来到入口处迎接我们。
「利昂君，听说给母亲做药的材料收集齐了，真的吗？」
「嗯，真的哦，米莉——」
向作为女仆的米莉妈妈發出指示。米莉妈妈马上明白了，把三个材料取出来展示。
「各自分别是，琉克司伽鲁熊的肝、地龙的爪子，世界树之叶」。
「哦……好厲害，不愧是利昂君。不愧是人称这个国家的最大权力者」
「不，只是运气好而已」
……话说，最大的权力还是头一次听说啊。但是，我认为这个国家最大的权力者是国王……大概。（梦：就这麼一句费了半个小时……‘但是’的原文是‘さすがに’，应该写作‘流石’才对。就好像我对着地图（朝阳区）念asahi一样……尴尬的不行……）
「即使如此……还真是相当可观的量啊？」
「哎，因为不知道到底需要多少。肝和爪子各只有一个，世界树之叶子有一大把……应该够了吧？」
「大概足够了，我马上去交给药师可以吗？」
「诶，当然了。我也想去拜访一下，可以同行吗？」
「当然可以」
「谢谢你……你们两个怎麼办？」
後半部分是问向爱丽丝和索菲亚的。
「索菲亚想去看看母亲可以吗？」
「我是没问题……」
艾莉婕桑的病情恶化了的话不能不去看望呢。索菲亚是亲生女儿，虽然应该不会谢绝会面……但是能不能还是要看埃里克先生。
「病情不理想，但也不是不能会面。因为有可能在睡觉，先让佣人去确认一下吧」
「——他是这麼说的」
「那麼，索菲亚要前去母亲在的地方了。请代我向赛思先生问好」
「嗯，我知道了。那麼……爱丽丝呢？」
「我……我跟着索菲亚酱一起去好了」
「这样啊，就这样做吧」
於是，索菲亚和爱丽丝一起离去了。我和埃里克先生一起，按照原预定去找苏菲尔家雇佣的药师。
於是来到了家中重要的人居住的区域。虽说如此，也没有什麼太大差别，地毯也是一直铺到走廊。
「说起来，药师从很久以前就在服侍苏菲尔家么？」
「啊啊。你说赛思啊，作为药师，是在代代服侍我们家的家系中出生的。我和索菲亚小的时候经常受其关照」
「……受其关照吗？」
虽然不清楚埃里克怎麼样，不过索菲亚从相遇的时候身体就很健康。听到受药师关照也不会反应不过来。
「……有好好的努力了啊」
「啊……」
说的是谁啊，说的是什麼事啊，听到含糊其辞的话的时候做了各种各样的推测。
恐怕是练剑时受伤後，治疗时说的话吧。然後对方大概是，原骑士队长的执事——被索菲亚杀掉的雷吉斯吧。
作为闲聊来说这种话实在过於沉重，所以赶快把话岔开了。
「顺便问一下，既然你说小时候经常照顾你们，这麼说索菲亚跟那个药师很熟吗？」
「嗯？那是当然的。刚才也说了，他是世世代代服侍我们的家系中出生的，索菲亚当然也信赖他」
「欸……索菲亚也？」
在苏菲尔家时的索菲亚，因为恩惠的力量的缘故，封闭了内心。能让那样的索菲亚信赖的人很少见。
还在想药师是怎样的人呢……看起来是好人啊。
「那麼那个人，今後会作为药师继续服侍下去么」
「当然……虽然想这样说，不过到他这一代应该就结束了吧」
「……出了什麼问题吗？」
「赛思的兒子成为了侍奉苏菲尔家的骑士」。
「啊，原来如此，今後会作为骑士世代代服侍啊」
并不是说一定要做双亲的後继者，如果同样是服侍的话，也并不是不可以。正这麼想时，埃里克先生静静地摇了摇头。
「在讨伐盗贼的任务中死了」
「这样是吗？……对不起，我说了多余的事」
「不，没有关係……哦，到了，就是这个房间」
在说话之前埃里克就敲了门。不久之後伴随着「谁啊」的说话声，门打开了，出现了一个年过半百的男人。
「埃里克大人，怎麼了吗？」
「啊，我送喜讯来了。但在那之前要介绍一下这位。古兰歇斯家当主利昂君」
「利昂？古兰歇斯」
报上姓名的同时轻轻地点头。但是赛思并没有立刻回答，而是瞪大了眼睛。
不知道在吃惊什麼，那麼该怎麼办呢？这样想着，埃里克先生咳了一声。赛思回过神来。
「失、失礼了。初次见面，我是在苏菲尔家担任药师的赛思。然後……今天是有什麼事吗？」
「治疗艾莉婕桑的病的必要的药的材料全收集齐了。想请您制作药物」
「真、真的把材料都收集齐了吗？」
「嗯，在这裡」
拜托米莉妈妈把药的材料全部拿过来给他看。於是赛思就好像看到了什麼不可思议的东西，身体哆哆嗦嗦的颤抖起来。
「这、这个是……真的吗？」
「赛思呦，怀疑古兰歇斯伯爵很失礼啊」
「失、失礼了！」
赛思赶快低下头道歉，看到之後我露出苦笑。
「没什麼关係，难以置信的心情能够理解。但是没有问题，琉克司伽鲁熊的肝，地龙的爪子，还有就是世界树之叶」
「难、难道真的收集齐了……」
赛思上半身摇摇晃晃的要摔倒。埃里克先生赶忙去扶他。
「没问题吗？」
「啊、啊啊……非常抱歉。没想到，只是没想到这些素材能全部集齐，让我大吃一惊啊」
「哈哈，赛思会惊讶也不是没道理。比较容易得到的肝，入手却是最困难的什麼的，真是超乎常理」
应该是想稍微缓和一下气氛吧。埃里克先生微微笑了。
「……不是世界树的叶子吗？」
「据说世界树的叶子最简单。说实话，还在想是不是玩笑话呢。不愧是古兰歇斯伯爵，常识异於常人」
被说的好过分——虽然这麼想，不过这麼想的估计只有我一个吧，赛思他‘原来如此’地表示理解。
「——咳咳。那麼赛思，药需要多久才能完成？」
我为了把话题转回来咳了一生，向赛思询问。然後是回神了吗，他慌张的做出反应。
「失，失礼了。药的话……我想一下。一个星期……不，三天就可以完成了吧」
「三天吗？……相当久啊」
「因为有乾燥的必要呢」
「原来如此……这样的话就没办法了。很担心艾莉婕桑的病情，所以请尽快」
「明白了」
拜托其制作药物之後，我为了去看看索菲亚的情况，和埃里克先生分别，走去别的地方，不过——
「为什麼？为什麼不能来见你？」
「跟你说了好幾次吧？没有话能对你说」
在走廊中走着，从艾莉婕桑的房间裡传来了争吵一样的声音。索菲亚和艾莉婕桑好像在吵架。
虽然就此止步的话是比较好的选择，但是无法判断的情况就很难办了。总之先偷听她们的对话，见机行事。
