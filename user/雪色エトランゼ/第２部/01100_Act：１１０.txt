110
王城昏暗的地下通道、跟随著国王陛下巨大的身影。
　我的旁边是西利斯。
　三人的脚步声、在狭窄的通道回响着。
　我在王都生活一阵子、城堡中到处走过、但这个地下没有来过。平常是严格的警备、不能进入的区域。
　在其最深处。
　看起来普通的门、陛下亲手打开。
　西利斯迅速地打开灯。
　我不了解状况、感到不安。
　灯光淡淡的照射室内、许多的剑与枪、盔甲排列著。闪亮耀眼、华丽的装饰發出光辉。
　简直是、宝物的房间。
　那中央。
　台座上摆放著长剣、陛下拿起来。
　鞘和构造看不出有什麼特别。跟在这地方排列的武器相比的话、相当樸素。
　陛下把那个、递给了西利斯。
「这把剑、是建国的国王携带著的勇者神器。是世世代代王位继承人继承的神器」
　西利斯接住、西利斯银気的光辉、淡淡的光辉注入长剣。
「这把剑的一撃、听说可打败军队、打破城墙。是铃古多维斯的至宝」
　陛下的男中音、比平时更低沉。
「西利斯。这把剑借给你。用这力量、消灭祸之魔獣。守护伽娜蒂。然後、两人要安全的回来」
「哥哥……」
　西利斯突然握住了我的手。
「必定！」
　然後对陛下敬礼。
　我也慌忙仿效。
「本来、应该要準备充分的戦力……」
　总是刚毅的陛下、脸蒙上阴影。看著我。
「伽娜蒂。还是、你们不要去……」
　但是我、静静的摇摇头。
　今早的会议上我提出的作戦建议。
　用新利寇特号奇袭祸之魔兽之塔的方案、冲进敌人正中央、是敢死队。
　无法保证平安回来。
　这个作戦是我、优人、陆、夏奈、唯一起的建议。
　然後我是、执行指挥。
　作为提议这个鲁莽作战的人、不能推卸责任。另外、为了把优人们送回原来的世界、我也要背後支持……。
　可是、首先反对那个组成人员的是、希兹娜大姐与西利斯。
「伽娜蒂大人。我们和优是一同前往危险地的团队。如果优他们去、我们也要去」
　希兹娜大姐眼神像要射穿我。
「哼、我的船是必要的、当然我也要去呢」
　利寇特一贯的板著脸、双手叉腰。在旁边、劳尔君并排一直点头。
　全体……。
「如果伽娜蒂去、我也去」
　旁边的西利斯低头看我。
「但是、西利斯……！」
　西利斯、这时需要防卫王都。
　话说不出来。
　为什麼。
　觉得有点高兴。
　西利斯来的话能安心。
　西利斯……。
　那样想著……。
「打扰了」
　我们互相交换视线、西公大人举起了手。
　陛下用视线催促、西公大人恭敬地敬礼。
「现在特攻、是不是有点操之过急。应该稍微看清情况不是吗？」
　西公大人的意见、王统府组涌起了同意声音。
　我看著西公殿下。
　一边在脑袋裡整理、一边慢慢地编织着语言。
「急著讨伐祸之魔獣、防止大地黒化以外、还有一个目的。现在、世界各处泛滥著魔兽群。说不定、可以阻止的」
　周围人声嘈杂、疑问的视线转向我。
「之前、黑骑士说过。魔獣、是作为祸之魔兽的爪牙而工作的东西。现在魔獣泛滥、似乎不是它们的意图、不过祸之魔獣对魔獣还是有保持一定的影响力」
　如果不是那样、全世界的魔獣会一齐以地裂峡谷为目标。
「总之、讨灭祸之魔獣、蜂拥而至的魔兽群、其功能应该……」
「嗯、确实啊！」
　马瑞斯坦博士拍拍手。
「根據欧鲁巴陇岛的记录、千年前、梵・勇者用自身换取封印祸之祠的魔獣之後、有纪录世界的魔獣锐减。祸之魔獣讨伐时候、也许没有魔獣就此消失这等好事、但是大幅程度削弱是可期待的」
　我对博士点点头。
「明白了」
　国王陛下郑重地点头。
　等待陛下的下一句话的沉默时间、耳朵都发痒了。
「参谋部长」
「是」
「从银気持有者招募志愿者。假如有幾个小队、也能挤进空飞船吧。作为突入队的援护」
「陛下！」
　我禁不住發出了声音。
　银気持有者是贵重的戦力。现在从王都防卫队抽出、是不理想的。
「伽娜蒂。我认为你的策略是有利的。是为了使那作战成功的措施」
　与平时的陛下不同、勇猛也冷静而透彻的声音、令我畏惧。
「那裡的尖帽子」
「呃、我？」
　利寇特指着自己。
「王都到祸之魔獣的塔、用你的船需要多久时间」
「……两、两天吧」
　陛下站起来，铠甲鸣响。
　大家一起立正、仰望着陛下。
「那麼、突入组明天出發。三天後、与祸之魔獣讨伐了的同时、王直骑士団转为攻势。和相关部门联络要确实」
「「是！」」
　王统府组敬礼。
　陛下凝视著希兹娜大姐们。
「勇敢的冒険者。有什麼是我们能做的，请说。竭尽全力支援」
　希兹娜大姐代表走上前去、低下头。
「那麼、各自、尽最大努力」
　大家匆忙的离开大厅。
　我打算与优人们商谈详细作战计画、陛下招唤我与西利斯。
「西利斯、伽娜蒂。有要交付的东西。跟我来吧」
　陛下告知的声音、与平时一样低沉轰鸣、充满了威严。
　但是那脸上、没有严肃的表情、只有担心家人身体的表情。
　那张脸、转向我们。
　谢谢、陛下。
　不只是担心、把你弟弟带去危险的地方……。
　察觉到了我的心情、陛下温柔地眯着眼睛、坚硬的大手抚摸着我的头。
　并且、带我们到地下的宝物库去了。
「西利斯。交给你了」
「好的、兄长」
　黑暗的宝物库、西利斯手上的勇者神器银色的光、闪闪发亮。
　啟程是明天。
　我们必须挑战强大的敌人。
　能否取胜呢。
　不。
　一定要胜利。
　即使梵·勇者档住去路。
　从伊莉斯姐姐的死开始的这个鬥争。
　在这裡、一决胜负！
　晚餐後、与希兹娜大姐们商量準备的我、看时钟幾点时吓了一跳。
　已经这个时间。
　如果不早点就寝。
　明天开始又要忙了。
　我一定会疲惫不堪的、至少今晚要在香喷喷的床好好地休息。
　陆和夏奈已经睡呢。
　唯、帮两人盖被褥。
　利寇特和劳尔君在做船的整备。
　优人与希兹娜大姐脸靠近、不知在长谈什麼。
　两人都开心地笑着。
　因为不应该妨碍、我在优人们房间的入口门帘放下对秃头先生鞠躬、传达今晩已经结束。
　秃头先生轻轻地笑了笑、点点头。
「伽娜蒂。明天见」
「晚安、伽娜蒂大人」
　与注意到我的动静的优人和希兹娜大姐打招呼、我离开了客厅。
　从王直骑士团本部出来、即使日落下去也不变冷的的夏日晚上的空气、猛的涌来。
　王城的用地、到处燃起篝火、像白天一样明亮。那个原因吧、不太看的到星空。
　在堆积的物资和帐篷的之间跑过去的兵士也还很多、夜晚也是忙碌的样子。
　我从之间小跑穿过、与熟识的近卫骑士打招呼、进入了王族的私人区域。
　在王城裡、与外面比万籁俱寂。
　也没遇到女仆。
　我在那样往有西利斯房间的三楼跑。厚脸皮的、西利斯一人占领了幾个房间。我现在、是在其中一个房间就寝。
　进入与卧室相连的客厅。
　淡淡的灯光、穿著休闲的西利斯坐在沙發上、看著文件。
「啊、我回来了、西利斯」
　我一边取发夹、一边把文件放在桌子上。
「茶、喝吗？」
　自己想喝的、所以不等待回答「咯嚓咯嚓」的準备。
「伽娜蒂。来这裡坐下」
　西利斯的低沉声音呼唤我。
　虽然有点奇怪、但是我拿了茶具坐在对面的位子。
　西利斯放下文件看著我。
「伽娜蒂。明天的出撃、你还是留下」
　一瞬间、没能理解这句话的意思。
「诶……、谢谢你担心我，不过……」
　我展现笑容。　
　可是西利斯没笑。
「聪明的伽娜蒂，你应该明白的。你是、累赘」
　那句话刺痛。
　用力紧握著茶杯。
　那样的事……我明白。
　没有银气的我。
　能做为对手的、只有小型魔獣。
　优人们的战鬥无法介入、连魔獣群的阻挡都无法。
　明白。
　明白的。
　我一瞬间闭上眼睛、但是、一定要看著西利斯。
「正如陛下说的那样、我的作戦方案。我要负责任……」
「我去」
　西利斯抢去我的话。
「我代替你去。然後完成这个责任」
「……这样」
　对我、对我、西利斯到底想说什麼。
「伽娜蒂留下。留下来、在雷古鲁斯公与父亲、母亲旁边」
　摇摇头。银色头髮挂在肩上摇著。
　看西利斯的眼神就明白。
　即使是严厲的话、也是因为牵挂著我。
　高兴的、眼泪快要溢出了。
　心裡有温暖的东西。
　如果。
　如果、被允许的话。
　但是。
「西利斯」
　声音、稍微有些颤抖。
「西利斯。我、有责任。不只是因为这个作战。这个战鬥。优人他们的战鬥、是最後的责任。因为，这是我来这裡的意义」
　我是、为了填补被黑骑士误杀的伊莉斯姐姐而、被这个世界招唤过来。
　那就是所谓的命运吗。
　结果我、引导优人他们与魔獣战鬥。
　正因为如此、最後送走那些傢伙。
　在那裡送他们回家。
　我觉得那是我的责任。
　而且……。
「我是怎样的存在、有时也不明白。那时、支撑我的、是西利斯说的话。是接受我的父亲大人的温柔。所以、想报答大家。不想把各种责任推给西利斯和优人」
　直视着西利斯的眼睛、在心裡深處互相缠绕着的思念、想办法说出。
　不过、大概是失败。
　西利斯不知道我的事情、一定不知道我在说什麼吧。
「……我觉得有充分报答」
　西利斯小声嘟囔着。
　我不知如何回答只好看著手边。
　西利斯的茶泡好、把杯子递出。
「那」
　我微笑。
　肩膀没力气、轻飘飘的。
　稍微感到为难。
　为了传达我的任性。

「西利斯出發时、请不要留下我」

　西利斯目不转睛地看著我的脸。
　害羞了、稍微移开视线、喝茶。
　稍微偏浓的苦茶。
　西利斯、躺到椅背抬头看天花板。
「啊啊啊啊啊啊啊」
　發出奇怪的呻吟声。
　这样一段时间後。
　再次啪地起身的西利斯、直直地看着我。
　那个动作、我吓得浑身发抖。
「两个条件」
　条件？
「明天的作战、伽娜蒂同行要承诺的条件」
　我等着西利斯接下来的话。
「第一个、被我守护」
「西利斯、我、可以保护我自己！」
　我不由自主的大声。
　握剑者的骄傲、这样的决心。
「别發怒。不是说战鬥。意思是我可以看见的範围。不要如往常一样、注意到时已在突撃」
「我……！」
　那个我、没有那麼莽撞吧。
　充满抗议、瞄著西利斯。
「第二个」
　无视那样的我、西利斯竖起第二只手指。
「回来後、请给我听一下那个答案」
　那个？
　楞了一楞。
　那个……那个……那个……。
　那个？
　那个……。
　……。
　……哇啊啊啊啊。
　无法直视西利斯的眼睛。
　全身忽然发烫、开始颤抖。
　什麼都说不出口。
　就是说、那时候的告白的回答……。
　忘了反驳和异议、注意到时、我、机械式的点点头。
　……啊。
　西利斯满意地咧着嘴笑了。
　呜呜……。
　祸之魔獣以外的、另一个试練……。
　我握着杯子、低著头。
　第二天早晨。
　从清晨开始、吸收著展开的蓝天。
　绿色的气味儿、今天也预感到炎热的空气。
　快要忘记现在确实被魔兽包围的事、是心情很好的夏日清晨。
　我、蕾蒂西亚与阿莉莎帮忙、穿上铠甲。
「伽娜蒂大人……」
　对著镜子裡看起来表情不安的阿莉莎、我轻轻地微笑。
　蕾蒂西亚与阿莉莎带领、我来到了王直骑士团本部旁的训练场地。
　为了送别我们的出征、新利寇特号周囲、众多的人们聚集着。
　国王陛下和家人。部份的近卫骑士。以西公为首的、十数个贵族。全都是我的相识。然後、军务省的同事们、坦尼布鲁克商会的分行行长等等的一般人聚集。
　大部分的王统府职员为了对付魔兽很忙碌的时刻、有这麼多人聚集为我们送行，是非常高兴的事。
　对今後会遇到激战的骑士们来说、是很大的鼓励。
　当然对我来说也是。
「伽娜蒂大人！」
「要平安无事！」
「伽娜蒂大人、平安回来！」
　从送行的大家飞来温暖的声音。
　我咬住嘴唇为了不溢出泪水、感谢的话内心念著、继续前进。
　新利寇特号的前面、冒险者组的优人们已经集合了。
　我和所有人一个一个的目光交汇、点点头。
「伽娜蒂酱！」
　那裡、国王陛下的旁边、突然玛莉亚母亲大人跑出来了。以及、从铠甲上方、紧紧的抱住我。
「伽娜蒂酱。你不要去吧。在这裡和我们一起等待吧」
「……谢谢」
　我一边微笑着、紧紧地抱住了玛莉亚母亲大人。
　布莱斯特父亲先生、温柔地抱住玛莉亚母亲大人的肩膀、从我这拉开。
　玛莉亚母亲大人、用悲伤的脸一直注视我。
「伽娜蒂。是时间了」
　全身穿著白银的铠甲、佩带著铃古多维斯的至宝之剣的西利斯、把手放在我的肩膀上。
　西利斯的背後等候的是、从王直骑士団各部队选抜出的精锐全副武装骑士三十六名。他们、是志愿与我们一起讨伐祸之魔兽的骑士们。
　蕾蒂西亚也加入那队、领头站著的是格拉斯。
　包含格拉斯在内、骑士队的各位没有悲壮感。
　有的只是、充满鬥志的脸。
「我们坚信勇士们的胜利！」
　陛下的激励回响。
　在西利斯的信号下、骑士们陆续的登上新利寇特号。
　我、摆弄大衣的下摆、也登上新利寇特号。
　不回头看背後。
　即使想回头看、也不回头看。
　我与西利斯和优人们一起、前往了驾驶舱。
　镶著玻璃窗的驾驶舱、眩目的朝阳倾注而来着。
　利寇特与劳尔君、海因德主任已经利落地做出发的準备。优人们也分别去当、利寇特和海因德主任的帮手。
　我也打算过去那裡。
　不经意间、注意到。
　驾驶舱的窗户外面。
　与送行的国王陛下队伍的另一边。
　莉莉安娜小姐和希巴鲁兹陪伴著那个身影。
「父亲大人……」
　是来、送我的。
　我、与父亲大人视线交流。
　言语无法到达的距离。
　但是不可思议的、父亲大人是这样说的。
　平安归来吧。
　我点了点头。
　是、是在心裡。
　感到父亲大人点头了。
　我轻轻地微笑。
　向著父亲大人。
「小姐、準备起飞了！」
　利寇特从前面的座位、回头看。
　我看了西利斯一眼。
　然後、后脚跟响著、往前走。
「来吧、大家！」
　看著操縦席诸位的脸。

「出發吧！」

「了解！」
「啊！」
「喔！」
「是的！」
　声音重叠起来。
　机関声音大响。
　我们、要结束这场长期与魔獣的战鬥、飞舞上夏天的苍穹。
