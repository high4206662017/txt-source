45
于夜深才返回到宅邸的我，躲开着想询问晚餐的聚会的结果的女仆们，开开心心的洗了澡。然後，连头髮也未弄干就爬上床如虫子一般钻进被子裡。
「请您好好休息，伽娜蒂大人」
将灯熄灭準备走出房间的莉莉安娜小姐，我用着像似要昏过去一样小的声音叫住了她。
「莉莉安娜小姐，我今天，那个，诶多...被、被、被求婚了」
用着不知道莉莉安娜能否好好的听到的微小的声音说出了被求婚的这件事，脸庞就好像迸發火焰了一般的炙热。
莉莉安娜小姐转身回到了我的床边站着。
「是吗？」
虽然周遭很暗看不清楚表情，不过莉莉安娜小姐的声音很是平静。
明明并没有明确的说出是谁，但是莉莉安娜小姐却好像已经知道了一样。这是为什麼呢？
「莉莉安娜小姐，我，要怎麼做才好呢...？」
「伽娜蒂大人」
莉莉安娜小姐的声音温柔的回响着。
「伽娜蒂大人、您自己是怎麼想的呢？首先要请好好地自行确认自身的想法。之後的，请在回到因贝鲁斯特家後，和家主大人慢慢地商谈吧」
「怎样想吗...？」
我再次重複地(说)着，望着天花板。
把鼻子埋入被子之中。
对着呆呆的沉浸在思考之中的我，在寒暄就寝之後莉莉安娜小姐便静悄悄地离开了卧室。我对此未感到在意，开始回忆起在之前所發生的事情。
在白色的花朵的温室裡受到冲击性的告白的我，脑袋咕噜咕噜的陷入了混乱。以为是这是在开玩笑而打算一笑了之。
但是，凝视着我的西利斯的目光是那样地笔直坚定。在说出结婚後，西利斯呈现出紧张与自信与不安所混杂的複杂地心情。
自然能够明白这句话绝对不是信口开河的玩笑。
是的，我能够感觉得到这样的话，我也决不能够敷衍地将这事了决。
...如果不从正面断然的拒绝的话是不行的。
对着在认真的传达着自己想法的西利斯，我也不能做出隐瞒才行。
这便是，作为西利斯的战友所应有的礼仪。
要好好的，说出明确的理由去拒绝才是。
我闭上眼睛，缓缓地深呼吸着。
向腹部注入了力量回看着西利斯。
「谢谢你，西利斯」
声音稍许有些颤抖。
「对你那认真的话语，我也不会以谎言相对。所以，实话实说，我……其实……」
「伽娜蒂！！」
并不是利穆威尓侯爵的父亲大人的孩子。
原来的我是个男孩子。
本应说出口的这些台词，在西利斯的呼喊声之中被打消了。
「伽娜蒂，对你想要认真的去回应我的事感到十分开心。但是那个告白，是只属於你的秘密吧？那个秘密如果说出来给人听的话，不会给什麼人添麻烦吗？以我们的立场来说，这不仅仅只是自己一个人的责任了啊」
西利斯的话令我心跳高涨起来，并咬紧了嘴唇。
倘若揭露有关我出生的事的话，会让接受了我的利穆威尓侯爵家裡的各位添麻烦了吧。
「伽娜蒂，没关係的。什麼也不说也可以的。不管那个真实意味着什麼，我喜欢的是现在的你。此时此刻，在我眼前的你。」
西利斯温柔的微笑着。
看着那个笑颜，好像有着什麼我所无法理解的感情正源源不断地涌现出来，我没办法去正视西利斯的脸而低下了视线。
现在的我……
不再是篠崎奏士。
奏士也不再是我了。
而是成为了伽娜蒂·利穆威尓……
「现在正是辛苦的时候。也不需要现在就给予我答复，希望在下次再会之时能够听到你的回答」
对發出喃喃细语般的声音的西利斯，我除了轻轻地点了点头之外没有其他办法了。
真是的……
要怎麼做才好呢。
不过犹如洪流一般闹腾着的内心逐渐无法制御下去，我不由得穿过西利斯的身旁跑向了温室的出口。
但在这一瞬之间西利斯抓住了我的手。
接着对我悄悄地说了。
「不要太乱来啊」
我无言地点了点头。
从走廊中离开，我快步走下了楼梯。总之，我想找到一个安静的地方冷静一下心情。
经过楼梯的平台转角处时，与优人擦肩而过。
现在却什麼话也说不出来。
然而準备从一旁通过的时候我的肩膀被优人抓住了。稍微有点痛。
「伽娜蒂，怎麼了？」
我只摇了摇头回应着。
「那个欧吉桑对你做了什麼吗？」
优人的脸因积累的怒气而变的扭曲。
「不是的...」
我很小声的否定着。
「那个混蛋……绝对要揍飞他！！」
像是在显现着优人的愤怒一样，在他的拳头上出现了摇晃着的银色光芒。
「不是的！！停下来！」
我立即抓住了準备往楼梯上跑去的优人的拳头。
「没事的，什麼都没有。放心吧，优人……」
我牵着优人的手，从楼梯上下去。
对於优人那关心的话语我是怎麼回答的，我已经记不清楚了。
我呆呆地眺望着卧室的天花板。
我到底想要做些什麼。
为什麼会呆在这裡。
至今为止發生并经歷过的各种各样的事情在脑中回想着。
如今的我。
被西利斯所认可的心情是發自内心地感到高兴。
自从来到这个世界之後所拼命努力的结果被承认了，我感到了放心与安乐。
父亲大人与西利斯和现在的各位都在这儿，我也觉得作为伽娜蒂没什麼不好的。
但是我原本是个男孩子啊……
跟西利斯交、交往什麼的、是不可能的嘛，再说，结、结婚什麼的，更不用去考虑了。
呜呜……
嘎啊啊啊啊啊啊！！
我（俺）缩在被窝裡慌闹着。
我（私）到底该怎麼办才好啊!!
我（俺）是男的！
我（俺）是奏士！
男人！
男人！
男人......
......
西利斯...
脸像是boom一声爆炸一般。
将西利斯的名字说出口的那瞬间，糟糕的察觉到之後耳朵已经变得通红的了。
该怎麼办才好呢，优人。
怎麼做才是最好呢，父亲大人。
爷爷……
我（私）该怎麼办……
清晨的空气凉飕飕的，在公馆裡微微的呼吸一口气也会变成白色的雾。
礼服和大衣搭配着长筒皮靴的形象，将旅行的装备全部整理好的我，摇晃着系好的头髮进入了希兹娜他们等待着的房间。
在空间大小正合适的客厅中，感受到壁炉带来的暖洋洋的热度。
季节也差不多开始入冬了，无法离开那个壁炉的火焰的日子正慢慢的降临着。
「各位，早上好！」
「早上好，伽娜蒂小姐」
「伽娜蒂酱，今天也十分可爱！」
将竖起了大拇指的夏奈无视掉、无视。
「那麼，各位，我就先行一步回因贝鲁斯特去了。优人，劳尔君就拜托你了」
优人跟平时一样地露出了牙齿笑着。
「了解，放心地交给我吧」
我稍微感到了安心。
「劳尔君，如果明白了黑海啸的具体含义，还请您告诉我们」
「嗯，好的。那个，承蒙关照了，大小姐」
感到不好意思地低着头的少年劳尔，我表示没关係的摇了摇头，这孩子从今开始一定会变得忙碌起来吧。
「希兹娜小姐，在劳尔君调查结束了之後打算做些什麼？」
「是呢~想让劳尔君和利寇特去调查拉普雷男爵领的魔兽，护卫的话就由我来当。说不定能够在男爵领中找到玛穆斯坦博士有关的线索。」
我也的确认为有这个可能。
但是突然闯入敌人的领地说不定是一项十分危险的赌博。可能说不定那个黑骑士会再次出现。
黑骑士的妨碍...
我突然想到了办法。
「希兹娜小姐。如果去男爵领的话，和王统府的监察队一起行动怎麼样？」
「监察队...不错呢...」
希兹娜小姐细长而清秀的眼睛轻快的流动着陷入了沉思。这种大人的感觉，是我完全无法做到的呢。
「是呢，这说不定是个不错办法」
希兹娜对我点了点头。
希兹娜小姐一行和监察队一同前往的话，万一有魔兽以及黑骑士的干扰的时候也可以助一臂之力。希兹娜小姐只要得到公爵的保证的话也是能够进入到男爵领裡的。
但是，我在提出了那个方案之後感到了内疚。
这样就只是把优人他们当做方便的战力在利用着啊。
如果可以的话，并不很想让优人和夏奈去冒险的。
「伽娜蒂小姐？」
我重新地振作起来对着希兹娜小姐点了点头。
「希兹娜小姐你们还请多加小心，特别是优人。」
把优人[哈？]的抗议当作耳边风无视了。
「这个屋子，在王统府委托下一个借款人到来之前可以随意使用」
「为什麼净是些坏事」
我对着希兹娜微微地笑着。
「夏奈，要好好地利用厨房，饭菜要自己去準备了哦。因为女仆她们也会跟我一起回去」
对於我的警告，夏奈却是“诶嘿”地挺起了胸膛。
「不要紧滴！我们的餐会的话有莫里森在，莫里森的料理是最棒的啊！」
莫里森？
听到不是很耳熟的名字我感到了纳闷。
优人轻轻的用手指着房间的角落。
站在那裡的是在希兹娜小姐的宴会中出现过的秃头先生露出了牙齿笑着对我点了点头。
原来在啊。
而且名字居然是叫莫里森啊...！
我对这一冲击性的事实不禁後退了一步。
「伽娜蒂小姐，不回去的话也可以的哟？」
我听见了希兹娜小姐的那句话才想起我该回去了。
在门口，大家都是来和我送别的。
我和希兹娜小姐、劳尔君、利寇特互相握了下手。利寇特还是和以往一样绷着脸。
像夏奈平时做的那样我伸出手[pon]地抚摸着她的头。然後，夏奈也[pon]地回敬了。我想着要不要再去回击一下的时候，感觉没什麼意义就放弃了。
因兴头变差而呜呜地这样嘟着嘴的夏奈置之不理，我用力地敲打了优人的胸膛。
「虽说变的很强了，但是不要太乱来哦」
我小声地说着，优人却[哈]地笑了起来。
「还是跟平时一样的伽娜蒂呢稍微安心了。你也是，不要勉强啊」
我们互相点了点头。
我转过脚後跟离开了屋子。
走到外面、抬起头仰望着屋子。
沐浴在温和的秋天的照样之下、在数日间承蒙了关照的房子正散发着太阳的光辉。
接下来，因贝鲁斯特，我要回来了。
跟过来的时候一样没有坐在马车之中而是一个人骑着马的我，和护卫的骑士队与女仆们乘坐的马车一同前进着。
吹拂着的风很是清爽，上衣的下摆、马的鬃毛轻轻摇晃着。微微地飘散着甜蜜的香味的花朵。人们那愉快的喧嚣声混杂在风中传递了过来。
王都的街道上来往的行人一如既往的热闹。
因魔法人偶的骚乱而造成的伤痕被封锁的地方、对外出的行人似乎没有什麼影响。因贝鲁斯特也好王都也好，生活在那些地方的居民远远比我想象中要坚强。
眺望着具有特徵性的王城的尖塔，接近着内门时，看见数骑的王直骑士团的骑士在那等待着。
在他们之中，十分有特徵性的赤髮的少女骑士走了出来。
「利穆威尔侯爵的千金与同行的各位。我们是王直骑士团王都防卫大队，请允许我们在前方引导至到外城门」
我礼节性地对蕾蒂西娅的话微笑着点了点头。
「非常感谢，那麼就拜托您了」
我和蕾蒂西娅骑着的马并列前进着，一边窥视着王直骑士团的各位。
西利斯不在呢...
大概是很忙碌，所以才没来为我送行吧...
「但是，这就要与伽娜蒂大人分别了，实在是太快了」
「我也想继续和蕾蒂西娅小姐再多说说话呢」
我呼地露出了微笑。我想要与蕾蒂西娅和王宫的贵妇人学习更多的作为贵族子女的举止。
「欸嘿嘿。我也想要成为伽娜蒂大人那般的美丽、强大，与西利斯迪耶鲁副队长一样帅气的男人交往的成熟女性呢。要更加努力才行！」
蕾蒂西娅无意中的一句话，我的脸發出[pishi]一样的声音後固定住了。
「交、交、交、交往！？」
「啊咧？难道不是吗？」
「不是啊！西利斯只是战友而已！是一同战鬥过的战友！」
我不知不觉地大声地反驳了。
「哎呀，可是魔法人偶对策会议的时候，听说两个人并身站着一同利落地發出指示、[多麼帅气的夫妻啊]之类的同事们的孩子都是这麼说的哟」
将啊哈哈地悠闲笑着的蕾蒂西娅置於视线之外，我稍微加快了马步。
夫、夫、夫妻什麼的、是笨蛋吗！
希望不要再说那种多餘的事了。
我和那个傢伙仅仅是战友而已，对他的告白我也并没有打算去接受，这种谣言还是希望能好好的控制一下...之类的。
「啊，伽娜蒂大人。西利斯迪耶鲁副队长就在那裡哦。从早上开始就一直看不见身影，原来是在这种地方偷懒啊」
我听见了蕾蒂西娅的话猛地抬起头来看。
在街道稍前一点的地方。
在宽敞的街道一旁的树荫下，穿着王直骑士团铠甲的西利斯正抱着胳膊站在那儿。
「......早上好」
「嗯，啊啊。早上好」
西利斯文雅大方地点了点头。嘴角挂着与平常相同的大胆的笑容。
「...西利斯，在各个方面上感谢您的帮助」
「不，要感谢的人是我才对。多亏了你我才能阐明魔兽使役事件的动向。这一步意义重大」
我看着西利斯点了点头。
由於对拉普雷男爵的调查，事态也变得明晰了。已经在因贝鲁斯特發生的悲剧但愿不会再出现第二次。
「那麼说起来，那个监察我有个提案。拉普雷男爵领的监察队让优人他们一同联合行动怎麼样？」
西利斯将手放在下颚之上，稍微看着上面考虑了一下。
「原来如此，那个少年的话，作为对魔兽和黑骑士的对策也令人安心」
「是的，希望让负责魔兽研究的劳尔君一同前往男爵领进行调查」
「我明白了，监察队那边就交给我来说服吧」
我示以拜托了地点了点头。
在谈的入神的过程之中，在我的旁边马车队正在缓缓地通过着。
...要是迟到了可不好了。
我打算再次乘上马匹时，毅然地将手伸入到礼服大衣的口袋中。
在魔法人偶的骚乱前，準备父亲大人和莉莉安娜小姐的礼物之时一起买下来的吊坠。
在那个骚动结束之後包装变得皱巴巴的了，在今天早上，努力地鼓起勇气伸了出去。
「什麼啊，这个」
西利斯伸手接过了包装。
「...是护身符。武运长久一类的。因为承蒙了您许多的关照，所以给你了。虽然是和王族不相称的便宜货就是了」
我不去看着西利斯的脸迅速的做出了解释。
被稍微粗暴了一点的方法乘坐的马惊吓地震了下身躯。
对不起……
抓着缰绳的我的手，不经意之间被伸出的巨大的手掌包容了。
一瞬间心跳高速跃动着。
西利斯牵引着手而将我转过身去。
接着笔直地看向我的眼睛。
「伽娜蒂，相信之後很快就会见面了吧」
接着笑了起来。
那个笑颜实在过於自然了。
嘟嘟哝哝地思考着的自己好像有点傻似得。
我也，深深地呼吸了一口气後回以自然的笑容。
然後静静的点了点头。
西利斯也鬆开了手。
多少感到心情变得轻鬆起来了。
我踢了踢马的肚子，头也不回的向着队伍追了过去。
各种各样的人，相遇分别又相遇分别如此地反覆着。
但是，在这次分别之後，也一定还能再次相会。
背後正逐渐远离的王都与西利斯我是不会回头去看的。
因为马上又会相见的。
我为了让下次的再回能够更加的美好，现在所必须要做的，是前往因贝鲁斯特，回到父亲大人的身边。
在令人感到心情愉快的秋风中仰望着天空中鱼鳞般流动着的云朵。
