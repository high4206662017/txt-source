此时吸引着我的完全不是“魔女”这两个字。

与这件事相比，魔女什麼的根本不足挂齿。

我正在被手枪指着？

这究竟是怎麼一回事？

我看向黑衣青年手裡的黑色物体……

虽然形状有点怪，但是确实是手枪。

脑子有点转不过来了……

这裡为什麼会有用枪的人？

会不会是哪裡存在我不知道的技术？

不可能……这个世界的技术大概只有原本世界7,8世纪的水平。

虽然因为魔法的原因导致一些现代技术也能被模仿使用。

但是要造出手枪目前是绝不可能的……

更何况青年手上的枪看上去比原本世界的要更加先进，全黑的枪托和大口径的枪口间夹着半透明的玻璃，不知道为什麼要这样设计。

“怎麼了？第一次见这个东西吧。”

在我愣住盯着手枪的时候，青年抬起了手将枪收了回去。

“我也是的……明明不知道就没有威胁，那麼这样吧……”

“刚刚那个是手枪吧？”

我發出了声音打断了他的话。

但是这一反应看来却大大出乎他的意料。

“……你是从哪听到这个词的？”

“那个……”

当然是原本就知道的了，但是现在还弄不清楚对方的意图要不要把真相说出来呢。

但是又不能放过这个珍贵的情报源。

“算了，在这裡说不太方便。”

我跟着他的视线向着巷子外面看去，人们渐渐开始被刚才的声音吸引过来。

抓住我的手腕，青年大步离开。

“喂，等一下啦！很疼啊……别走那麼快！！”

…………

现在位於芬里尔的上颚的松之间。

非常够呛的，我幾乎是被拖回酒馆的。

而且貌似妮柯仍然没有回来。

“所以呢，为什麼要回妾身的房间？”

轻轻揉了下有点勒痕的左手腕，因为这个原因，现在我的心情很不好。

“非常抱歉啊……我是在镇子外露营的，所以无法作为商量这种话的地方。”

也不知道这个道歉是针对哪方面的意思，但是他看上去确实一副给人添了麻烦不好有意思的样子。

“不必了，妾身不是会计较这些小事的人。那麼，请先从您开始介绍一下自己吧。”

还是先了解对方的情报才是最好的，之後再判断说不说自己过去是异世界人的事实。

於是我便先下手为强了。

“可以，不过请先回答我两个问题可以吗？”

“真是小气的男人……”

“嘛~~没办法，毕竟我也不能随便透露自己的情报，更何况你的身份是魔女。也请稍微妥协一下吧。”

“为什麼知道妾身就是魔女？”

“之前我们制作过魔力的探测仪，稍微改造一下便可以分别出魔女的魔力。”

轻轻点了一下头表示明白，我示意他可以继续提问了。

“好的，那麼第一，你的识别ID是多少？”

“不……我没有那种东西。”

是指什麼软件或者遊戏的ID？为什麼要问这个。

“明白了，那麼第二，稍微请问一下年纪。”

“真是失礼的问题呢。”

“对此只能之後再道歉了……”

青年的眼神中完全没有开玩笑的意思。

我知道了，我知道了~~那麼就说一半实话吧。

“十二岁……”

“是真实的吗？我知道魔女拥有近乎无限的寿命。”

“没错，因为妾身成为魔女还没有多长时间。”

“原来如此。”

这样说下毫无进展，而且很不小心的是自己的情报被先一步套出去了。

不过随後他便开口叙述了。

“那麼就从名字开始吧，我叫做莲·月硝……称呼莲就可以了。”

“妾身是克莉斯，那麼莲先生是哪个国家的人呢？对了，妾身指的是您最初所在的国家。”

对於我的提问，莲皱了一下眉毛。

“试探差不多可以结束了吧，我觉得自己并不是你的敌人。因为我大概明白，你并不是属於这个时代的人。”

欸？这个时代……什麼意思，姑且我算是一个地球上的现代人……

“等一下，您指的是妾身是从别的什麼时间段来到这裡的吗？”

“难道不是吗？”

“不，完全不是，妾身是从别的世界来到这裡的。準确来说，来到这裡的仅仅只有人格和记忆。”

现在隐瞒也没有太大的意义，对方掌握了比我更多的情报。

面对我的话，他低下头揉了一下自己的太阳穴，显得有些困扰。

但是思索片刻之後，再次倏地一下抬起头来。

“我大致明白了，你是从异世界转生到这裡的人类吧。”

“欸……您猜的真准，但是不怀疑吗？”

“测谎仪的结果显示体温，心跳都很正常，也没有异常的表情和小动作。99.7%的可能是你没有说谎。”

“看起来您那边有着妾身所不拥有的高技术……”

虽然在原本的世界是看过关於测谎的一些新闻，但是要随身携带这样的仪器并不被对方发现目前还是无法实现的事情。

“我们所在的世界虽然不同，但是看来有一些共同的地方。那麼我就这样问吧，PC还有网络……明白吗？”

“欸，已经普及整个世界了。”

“嗯，那麼植入式PC呢……”

“不，没有这样的东西，但是大概可以想象得到您指的是什麼。”

“原来如此，大概50年吧……”

“？”

面对突然而来的数字，我是一头雾水。

“我指的是我们之间科技相差的时间……大体来说还有很多可以交流的地方。克莉斯的情况已经大致可以理解了，那麼我来简单地说明一下这边的情况吧。”

“好的，请说。”

…………

之後的一个小时内，我静静听着莲单方面的叙述。

根據莲所说的，他们是出生于这个世界远古时代的人，大概时间根據他们的仪器显示是在5000年以前，一个文明高度发达的时代。

现在记载的史料并没有这段歷史……圣之历之前完全是一片空白。

那是一个和地球文明相似，不，甚至是超越了前者的文明。

量子电脑的出现加速了世界的旋转……

生物，电子技术高速发展的时期，幾乎每天都有新的技术诞生。

这个时代可以说让一切按着难以想象的速度发展着，渐渐地，新的事物彻底改写了规则。

通信不需要手机……所有信息和财产都被电子信息取代，癌症可以被轻鬆治癒。

人类可以从基因的角度上更改性别。甚至延缓衰老，达到不死的地步。

当然，这些也都是双刃剑……

最方便的技术只会提供给位於顶端的人们，长生不死，强化身体，肆意改变性别与容貌，操纵记忆，合成自己想象中的生物。他们就像神一样在这个世界上享受着最大的乐趣。

但是贫困乃至一般人依然过着有限的生命，除了生活更便利了一些之外和数十年前一般没有多大差别。不满和嫉妒在大多数人们的潜意识裡堆积着。

为了获得最大的利润，顶端的人们垄断着最顶尖的技术，以天价出售制品与服务。

这样一来，世界的矛盾也是越来越大……

开始频频出现战争，随後而来的自然不必多说了……贫穷，饥饿，死亡，憎恨。

还有真正的神的……愤怒。

——“巴别塔高及云端，直达天界。”

创世神无法再这样看着自己的造物肆意妄为，於是他让这个世界毁灭了……

轻描淡写的，就像神话裡说的那样。乌云遮蔽全部的天空，洪水淹没所有的土地。

在最原始的大自然之力之下，人们的抵抗却显得极为弱小。

没有办法的有钱人们让自己住进可以被保护的密封舱中指望逃过一劫。

很可惜，那些密封舱集中的地方现在应该成为了汪洋大海了吧。

而莲他们却很幸运地活了下来，只剩下16个人，12年前出现在同一个地方復甦……这也许是神明大人最後的慈悲了。

16个人就此分散到世界各处，用着残留的科技道具远距离共享着情报。

现在他们以古代人自称，想通过各种手段提高自己的名声再次復兴文明。

嘛，虽然莲本人好像并不支持这种做法，但是其余人包括自己的父亲都没有太多的反对。

他也只能先走一步算一步了。

而目前，由於即将到来的沃尔帕吉斯之夜，聚集的魔力吸引了强大的魔物们。莲也在为了平息这场灾难四处寻找魔女的踪迹。虽说貌似有一名伙伴非常偏激。

但是总体来说他不想杀死魔女，更希望通过交流解决问题，实在不行时再诉诸武力。

…………

大致情况就是以上那样了……

为了证实自己的说法，莲拿出了一枚耳环递给了我。

“这是什麼？”

“算是通信器吧……你戴上试试。”

我并没有戴耳环的经验，本以为必须要先做出耳洞才行。

结果把耳环放在耳垂下时它便自动卡住了。

好厲害！！

“真正的厲害的用法是这样的哦。”

欸？刚才我应该没有说话才对。

难道说……

“就是意念通话了……”

唔，感觉完全没有隐私权啊。

“多练习一下就可以控制了，轻点一下装饰的宝石就可以强行取消通话装置。登陆其他用户後也可以进行多人通话，当然那只已经登录我了。”

“好方便！真不愧是未来科技！”

看着我因为新奇开心的表情，莲也是莞尔一笑。

“那个就作为见面礼了，虽然想说的事情还有很多，但是今天时间已经不早了。

回去太晚的话有一个同伴会发很大的脾气。如果想要联繫我的话用那个通信器就可以了，不想用的话可以直接去楼下的老板娘那裡说想与‘狼’见面亦可……”

“那个！！”

“还有什麼事情吗？”

“非常谢谢你！！这个耳环妾身会好好使用的，关於沃尔帕吉斯之夜的事情妾身也会来帮忙。”

“你愿意相信我了吗？”

“嗯，因为您也相信了妾身了不是吗？”

我让自己露出一个友善的笑容。

“呼呼……说的不错呢，那麼明天再见了，可爱的魔女小姐”

回应着我的莲也让嘴角弯出弧度说出了暂别的辞令。

打完招呼後，莲轻轻挥了下手後便离开了这裡。

看来是可以理解的人呢。

之後也和妮柯说一下吧……或许莲他们能成为重要的战力

我也不希望看着魔物毁灭这个世界。

感觉脸颊稍微有点热……这一定是错觉吧。

==========================