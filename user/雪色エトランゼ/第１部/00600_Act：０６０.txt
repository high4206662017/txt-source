60
门铃声在空气裡传来。

「伽娜蒂酱，围裙！」

被唯叫住的我才刚注意到自己的样子，然後迅速脱下了围裙。

好险，好险。

那个是莉莉安娜桑準备给我的东西，但是如果像刚刚那个穿着在空中飘舞的粉红色围裙的身姿被优人和夏奈看到的话，估计今天就是我的存在本


身的危机了。

在这之前也被唯狠狠的取笑过了，在做菜的途中，也好幾次被偶然路过的父亲大人仔细的凝视了....。

我在与平时不同的没有人向我打招呼的宅邸裡“叭哒叭哒”地奔跑着。


在诺艾露斯菲亚的年末年初中，有着要和家人一起度过的惯例。在这个地方和日本也一样。因此，在一年的尽头的时候，幾乎所有的佣人都放假回


到家裡去了。

现在的房子和行政府里剩下的，就只有在新年开始的今天却还在当任着警备的可伶的骑士们。还有为了我和父亲大人自愿报名参加的暂时居住的佣


人们他们而已。

我走到了大门所在的大厅，打开了门。


因为还只是新春仍冰冷又清新的空气，刷的一下都流了进来。


「新年快乐！」

我对着在门前的两个人微笑着。

优人發出了“哟”的将手举起。

夏奈则是發出了“恭喜”的喊叫。

然後是负责教我料理的唯。

利用新年的机会，我将大家招待到宅邸来了。


在年初的休假结束後，马上就要开始战争了。这样的话，和大家见面的机会也会慢慢的变少了。所以，在这样的时期，得到了父亲大人的许可後


，决定举办一个小型的新年的宴会。

夏奈和优人今天也没有穿着铠甲，而是穿着便服。带着脱掉大衣的二人，我领着他们来到接待室中的一个里。（最後一句原文：コートを脱ぐ２


人を、俺は応接室の一つに案内した。顺序不确定，或许是在接待室脱的衣服）

「哇，好暖和哦」


在红黑色、暄腾腾的地毯铺装的室内，夏奈好奇的四处观望着。

「先稍微在这等一下。现在正在做料理」

「唯已经来了吗？」

优人“扑通”的坐到了上沙發上。


「恩。现在正在準备中」


我向二人挥了下手後回到了厨房。

在这途中，又偶然遇见了父亲大人。

「父亲大人，请问怎麼了吗？」

在手背着後面交叉着的父亲大人皱着眉头。

「不，只是在散步中」

.....在房子裡面？

如果没猜错的话，父亲大人现在好像很闲。

明明最近的事情繁多变得非常忙碌，所以要趁着新年好好休息一下才对吧。

在目送了父亲大人离开後，我和唯一起将料理运往了接待室。


因为没负责料理的佣人在，所以今天的食物是由我和唯準备的。即使我用尽全力，但能準备的也只是一种味增汤而已......。（好想吃）


虽然还想準备一下年菜什麼的，但是对我来说门槛太高了点。

因为各种各样的尝试而导致了错误的结果，还有因为切太碎了的一些食材搁置了下来。为了不浪费这些大量準备的食材，姑且只能用全部拿去


炖汤来解决了。



不幸中的万幸是，有着做味噌汤的调味料。毕竟是侯爵家。调料什麼东西也不至于会不齐全。

我笨拙的将圆柱形深底锅抱住放入了推车裡然後运过去。


就算是加上优人的还有夏奈的份，这个数量也太令人感到哑然了。

....不过也已经没有办法了嘛。毕竟都已经發生了。（好想喝伽娜蒂的汤）

在接待室的桌子上排列着散發出令人胃口大开的香味的食物。只有味增汤是我的作品。在後面的菜全是唯做的。

「好厲害啊。全部都是和食啊」

优人發出了感叹的声音。

「这个，是伽娜蒂酱亲手做的料理吗？」

夏奈马上就伸出了筷子。


「幾乎都是唯做的。我自己就做了一个味增汤而已。慢慢喝吧。」

「啊..恩。谢谢了。那我开动了」


当我指向巨大的锅时，夏奈将身体稍微往後退了一步。

为什麼.

「这麼说起来也过得很快啊」

「在说什麼呢，唯姐」

夏奈大口大口的吃着烤鱼。

「嗯，我们是从另外一个世界来到这边的吧」


「是啊」

远远的看去（遠い目をした），优人已经将碗里味增汤喝完了然後我立刻将他的碗里继续盛上了新的一份。（真爱...）


「现在优人酱也，夏奈酱也是，都成为了有名的冒险者了呢」

「唯也成为了很抢手的治癒术师吧」

在我继续喝茶的时候，偶然看到了冲我笑着的唯。 （俺の茶々に、唯はふっと笑って俺を見た。）


「你在说什麼啊。伽娜蒂酱不是变化最大的吗。变成了这麼可爱的女孩。而且还是侯爵大人的千金。姐姐我好高兴哦」

将视线从唯的露出的恶作剧一样的笑容那移开后，我挠了挠脸颊。

「....有着各种各样的事情，总之也是很辛苦的」

我“呼呼”的自嘲的笑了下。

如果看了我现在的身姿的话，祖父会说些什麼话呢。（说不定祖父也是女兒控）

「伽娜蒂酱」

「哇！」

夏奈突然从後面紧紧的抱了上来。


正因为这样，读不到银气持有者的行动会感到很困扰呐。

特别是这傢伙.....！

「伽娜蒂酱也很可爱的努力向上呢，在街上也成为了话题哦」

我挣脱了夏奈的熊抱。

「虽然被赞赏是很高兴...」

但是在结果上，还是没有能防止住魔兽的灾害和拉伯雷男爵的讨伐的骚乱。

虽然自己知道这是没有办法的事情，但是在心裡还是相当的苦恼。

如果再有稍微不同的办法，如果有再稍微和平的解决方式的话.....

「话说回来 」

优人一点一点吮吸着味增汤裡面的没有切断的蔬菜。


「重要的不是外表。还是努力到了这样的程度」

优人将已经变空了的碗放了下来。我继续往裡注入了新的味增汤。（一想到结局，翻到这就胃疼）

「....就是这样啊。如果有存在着评价的人的话，而想着那个评价觉得不适合自己，就会变得为了得到适合自己的评价而继续努力着。」 （「……そうだよな。評価してくれてる人がいるなら、その評価に自分が相応しくないと思ったら、相応しくなるように頑張るしかないよな）

我点了点头，将味增汤送入嘴裡。

味道好像有点儿淡。

「啊。如果有陸在这裡的话，大家就聚齐了呐。但是战争一开始的话，陸的寻找就得被中断了呢」


夏奈重重的坐在了自己的椅子上。

陆和夏奈是姐弟。担心至今都下落不明的陆的心情，夏奈是在这之中最强烈的吧。


「抱歉，夏奈。陆的事情，我也拜托了父亲大人。不过到现在都没有情报」

我将肩膀垂下，夏奈“嗡嗡”地挥了挥手。


「并不是伽娜蒂酱的错。是那个到现在都还没有出来的那傢伙的问题」

「陆感觉真可伶」

优人把味增汤喝光了。

我再次在碗里注满了汤。（捂住眼睛，然後去找胃药）


「大家」

在这个时候，唯用一个很严重的声音说着。

「实际上，有braver出现了传闻，不过也不完全确定是不是陆酱的事情，毕竟原本连是不是braver的事情都不清楚.....」（原文：実は、ブレイバーが現れたって噂なんだけど、全然確かじゃなくて、陸ちゃんの事か、そもそもブレイバーの事かも分からない話なんだけど……）


我为此感到惊讶。然後对唯的下一句表示期待的样子，将身子探出。

优人也停下来原本在吮吸味增汤的动作。


「是比我们这还要遥远的北方。在那个比王都更往北的地方，出现了像优人酱那样强力的银气使用者的之类的传闻。是在教会的本部那裡听到的」


异世界的来访者，跟braver拥有并列相同的操纵银气的能力。根據那传言裡面的主角，确实有着是braver的可能性。

或许的话。

或许，真的可能是陆？

就算是个不可信的传闻，我还是感觉看到了那隐约的闪烁的光。

「北方...吗」


优人自己一个人嘟哝着。

「在战争结束後，去和希兹娜桑商量看看吧，优人」

在夏奈的声音感觉到了她微弱的希望。

在我刚才为止还静止的心裡也确定到了那个从内心裡涌上的淡薄的希望。

为了把那个希望变成确实的东西，首先必须要突破掉战争的这个阻碍。

优人们的队伍也作为战力的一部分。唯也作为了治癒术师决定参加了这次的战争。


「优人，夏奈。就算你们有再强，也不能随便乱来。」

「明白了」

「了解！」

我看向了唯。

「唯，受伤的人的治疗就拜托你了。请好好的看护他们」


「我知道了」

唯点了点头。

我们的说话声回响在宅邸的空气中。

这是新年独特的静谧的空气流淌在宅邸中。

这个寂静只是在暴风雨前的最後的寂静的这件事，谁都明白着。



像笨蛋一样的在一直喝着味增汤的优人因为太饱变得身体动不了，然後在夏奈猛吃後而残留下的食物後，我和唯就负责把那些东西收拾掉返回厨房裡去，而在锅内的味增汤仍然残留着大批的数量。

「该这麼办....」

我正交叉着胳膊自言自语的时候，父亲大人又再次偶然的路过了。


「这麼了，伽娜蒂。如果是剩下来什麼的话，我可以......」

「啊，不是，没问题的，父亲大人。父亲大人的那一份，有好好的另外準备起来。」

不过是拜托唯做的体面的料理。

「哦，是吗，是这样啊」

“呼”的吐了一气後，父亲大人就离开了。（果然是女兒控）

......所以到底是这麼了嘛。

「对了，伽娜蒂酱！用还暖和的味增汤来招待还在警备的骑士的各位如何？」

唯“啪”的敲了下手。


原来如此。

毕竟是在新年还仍然继续的工作着。所以给各位送去味增汤当做慰劳品什麼的也比较好。

然後，我和唯一起推着载着锅的平板车，将味增汤来回发散给了大家。

「大家，这是伽娜蒂大人特制的汤哟。是大小姐给各位的慰劳品。」

唯高声呼喊着。

我因为感到害羞，脸颊一下子染上绯红色，一边将头低了下去，一边默默的给碗倒入味增汤。

因为是第一次看到的味增汤，骑士他们先是露出不能理解的表情，在我和他们的视线相遇的时候，大家的脸上开始慢慢露出了笑容并赞赏的点了点


头。（卡了15分钟，後半句还是没想出更好的意思，大概是这个意思了。原文：初めての味噌汁に、騎士たちは初めは不可解そうな顔をしていたが


、俺と目が合うと満面の笑みで肯いていくれた。）


太好了。


味道好像没有问题。



在这之後。


行政府的值班回到了平常后，在行政府食堂裡，【伽娜蒂大人的汤】作为味增汤加入进了隐藏菜单的这件事情，是在很久以後才知道了。
