「…於是有了178名部下，想要个能够让她们居住的宽广的家」
「太多了吧？」

和教会结束纠纷後，部下突然增加——需要抚养178名家人，於是我就找阁下商谈了。

「發生了许多事情」
「这倒是可以…不过能够住这麼多人的家的話，需要中等规模以上的房屋呢」
「给我」
「…再怎麼说也没法免费给啊」
「要多少钱？」
「买的話至少需要1000枚以上的金币」

100枚金币换算成日元就是约10亿元。

「毕竟有做S级魔法士的工作和魔王的工作，3000枚金币的話能够现金支付」
「…原来魔王的工作也能赚钱啊」
「……」

从大魔王那裡获得的报酬基本上都是『对我的秘密保密』,所以并没有得到多少。
硬要说的話是和塔吉尼亚特测试胜负时获得的报酬，以及莎米艾露拜托工作时的报酬很丰富。
他们轻易就拿出了数百枚金币。
并且加上作为S级魔法士赚取的金钱，差不多有3000枚左右的金币。

「……」

虽然这种事情怎样都好。不过『机械・人偶』的原人偶价值5万枚金币，而拥有178体完成品的我为，什麼却只有3000枚金币？
不。3000枚金币也是笔巨款了。

「总之为我介绍宽广的房屋吧」
「啊～…S级魔法士的話没关係吧」
「不。要住的是我的178名部下…不，是177名。我和索菲娅会继续住在原来的家裡」
「…诶？」

和索菲娅亲热并不需要宽广的房屋。
虽然将1名女仆留在身边倒是可以，不过在此之上的話房间就不够了。

「唔～恩。名义上是『S级魔法士的房屋』就有办法解决了」
「那麼就拜托你了呢♪」

这麼一来，总之『机械・人偶』就不用在街头流浪了。

「話说回来，向教会找茬真的不要紧吧？」
「有我家178名『女兒』在的話，就算100万的军团进攻过来，30分钟就能全灭哦？」
「那是什麼好可怕」

名为『超强功率光束』的『等离子・放射』就连地平线的尽头也都能攻击。

「…世界末日吧？」
「谁知道？」

虽然就算死掉100万人，我想世界也不会怎样吧。总之歪了歪脑袋。

☆辉夜

我是辉夜。
被託付『魔王的女兒们』指挥的上位个体，是所有『龙人』们的姐姐。

「哈啊哈啊…」

现在我在『父亲大人（主人）』家裡作为女仆，不过夜晚时就会在夫妻寝室旁边的房间裡，将耳朵贴在墙上进行偷看。

「啊嗯♡」
『…这边是辉夜。今天主人和索菲娅大人也在『享乐』中。现在利用『念話』将声音和图像进行同步』
『『『『『咽口水』』』』』

怎麼说才好呢——我们處於对H十分有兴趣的年龄。
使用『震动感知』窥视墙壁另一侧生动的描写，将这个共有给『机械・人偶』――太长了就简称『人偶』——一起享受是我每天必须做的事情。

『主人…好厲害。居然连那种事情都…！』
『好羡慕…索菲娅大人』

平时都是清纯表情的『人偶』们，178体全员理所当然拥有着『个性』，与不同的思考方式。

『念話』让意思疏通上没有障碍，虽然平时不会有分歧，不过『这种时候』就会出现各种不同的地方。
不过全员都『对H的事情感兴趣』是不会有错的。

『我、我们要什麼时候才能受到主人的宠爱？』
『不可以急躁。我们和主人都没有寿命这一概念，也能等上100年的』
『…在此之前，身为『人偶』的我们能够生下孩子吗？』

每天晚上都讨论这种——名为『亲密話题』的会议。

『要是索菲娅大人怀孕了的話，我想就需要为主人处理性欲的人才！』
『这…的确如此！』
『那样的話…趁现在预先演习和学习并不为时过早！』
『是呢！』
『正是如此呢！』
「～～～っ♡」
『『『『『！！！！！』』』』』

这样讨论的时候，在听见索菲娅大人『最後的娇喘声』的瞬间回归了宁静。

『…主人…好厲害』
『嗯。每天晚上…都这样做好多次…』
『咽口水』

我们是绝对忠实于主人的『魔王的女兒们』。

正處於对H感兴趣的年龄——兴趣是『偷窥』。

★

「稍微想了一下…」

那天不知为何莎米艾露来到我家，喝着女仆辉夜泡的茶和我闲聊。

「不仅是2名勇者，更加削弱教会的战鬥力不是更好吗？」
「…莎米艾露大人不是很忙的吗？」
「很忙的！但是…偶尔也需要休息吧？」
「…是呢」

我是你的茶友啊！
虽然不想和莎米艾露敌对，也需要保持某种程度上的友好。为什麼我会变成莎米艾露的『友人』了？
这个人明明活了很长的时间，但是却没有朋友？

「……」

嘛——虽然我也没朋友呢！
我只要有索菲娅就够了。

「那麼，是在说什麼来着？」
「所以说！至少全灭4名勇者不是更好吗！」
「不，那不行吧」
「…为什麼？」

这人真的放弃『自己思考』了吧？

「对大魔王大人来说，希望反抗势力集中在一个地方」
「？？？」
「要是反抗大魔王大人的势力散落到世界各地的話，那麼要把握形势不是很麻烦吗。所以乾脆让他们集中在一起，只要把握那边的形势就可以了」
「全灭不是更轻鬆吗？」
「全灭的話，世界各地会产生反抗势力。结局又会创造出『教会』那样的组织再次聚集到一起，这样不就花了两道功夫了吗。只要世界中的生物没有死绝，反抗势力就一直都会出现的」
「那样的話…击溃勇者不是很不妙吗？」
「应该说教会的势力稍微大过头了，削减一半的勇者衰减一半的统率力不如说正好」
「嘿～」
「……」
「我、我当然是知道的！只是在试探你！」
「是呢～」

如果这人不暴走的話，倒是个挺愉快的人。

「莎米艾露大人，要来份『茶泡饭』吗？」
「啊。谢谢」

然後索菲娅每次都向莎米艾露传达『快点回去』的意思。

「每次过来的时候都会拿出茶泡饭呢，这是某种风俗吗？」
「…谁知道？」

总之真的想让她快点回去。

莎米艾露回去後，我久违地前往冒险者公会露脸了。
虽然前往教会也是个原因，不过回来时为确保『人偶』们的住所，并耗费时间整理住宅，於是就没时间去冒险者公会了。
虽然那并不是不得不去的地方，但不知不觉得～就往那边走了。
於是进入到冒险者公会裡後。不知为何『姐姐』的视线从我身上移到了在我身後的辉夜…。

「居然是…女仆」

不知为何颤栗了。

「照顾弟弟是『姐姐』的职责，但却将女仆带了过来…这是要宣战吗」
「…真是麻烦的『姐姐』啊」
「是呢」

总之很麻烦所以无视她，然後坐在指定的座位上和索菲娅亲热。

「噢噢。也就是说只是穿着女仆装，实际上却是弟弟的『妹妹角色』吗」
「…到底是听说了什麼才会这样解释？」
『姐姐』的思考依然很突飞猛进。

「并非女仆而是『妹妹』的話就没有问题，要和『姐姐』一起照顾『怠惰的弟弟』才行」
「…是呢」

总感觉麻烦得怎样都好了。

★

在冒险者公会悠闲度的过一段时间後，前往『人偶』的住宅看看情况。
到达之後，『人偶』们在住宅的宽广庭园中进行战鬥训练。
虽然我并没有叫她们这麼做，不过好像是太闲了。
顺便一提，教会分配给她们的装备是戴在右手上的特殊手环，输送魔力就能自由自在地变化成术者想象中的武器。
虽然并没有关係，不过这个装备1个就值300枚金币（约3亿元）。

再加上她们穿在身上的女仆服也是特别定制的装备，低级剑士的斩击以及低级魔法使的魔法是无法伤及到的。
虽然完全没关係，不过价值为500枚金币（5亿元）。

「……」

我看了看腰间毫无变化的短刀，以及平时爱用毫无变化的外套。

「…父亲、母亲。我并没有感到不满」

才不是想哭呢！
将特殊装备的手环变换成枪剑进行模拟战，『人偶』们中也有在远离的场所做其它训练的女兒。

「嗯～…！」

参拜那样双手重合在胸前集中精神，然後慢慢分开——双手之间出现了发光球体。
这是让内藏在她们体内的『核融合炉』临界所产生的等离子能量…。

「（使之临界并不需要集中或是长时间蓄力，说到底全身都巡回着能量，身体的各个部位都能够发射出去）」

也就是说，她们所做的只是『姿势』。

然後忽然睁开眼睛…。

「龙核热炮！」

朝着空中射出『超强功率光束』――不如说是收束的『等离子能量』。

射出去倒是可以…。

「（虽然我姑且命名为『等离子・放射』的）」

不知不觉间被她们当作『必杀技』改名了。
嘛，姑且这个名字与作为龙人的她们相应——吧？
这倒也是可以。

「主人！」
「啊。主人！」
「欢迎光临。主人」

就这样一边见学着，被『人偶』们发现後不断围过来…。

「嘿♡」

抱住我将『胸部』压过来。

「我也来♡」
「嘿♡嘿♡」

恩。虽然当然很高兴…。

「亲・爱・的？」

「伊！」

索菲娅的眼睛好可怕。
这绝对要比勇者亚克斯自称的『绝对零度』还要冷！
然後我被索菲娅捕获的同时，『人偶』们一起逃跑了。
别在这种时候连携得这麼完美啊。

「那麼回去吧♡」
「…是」

於是我被索菲娅带回了自宅。

「…明明我也想将胸部压向主人的」

感觉身後的辉夜好像说了什麼，但现在就装作没听见吧。

★

『人偶』们使用的『等离子・放射』又名『龙核热炮・放射』，是利用内藏在她们体内的『核融合炉』临界所产生等离子现象的必杀技，而与『核攻击』有所不同。
与之相对的我是直接用魔法制御力控制『核融合』作为武器，可以称得上是真正意义上的『核攻击』。

顺便一提我产生的『核融合』只是『物理现象』，与『魔法』些许不同。
和名字不同的是，『原子・镭射』是『核融合寸前』100%的魔法，每当使用时都会消耗作为术者的我的魔法力。
不过要产生『核融合』需要使用魔法，产生後虽说是用魔法制御力进行制御，不过那也只是制御『物理现象』而不会消耗魔法力。
也就是说一旦产生『核融合』後，之後就无需消耗魔法力随意使用。可谓称得上是『作弊攻击』。

虽然普通人无法产生也无法制御。
於是产生这个『核融合』後，能够使用的攻击技有3种。

1是『原子・放射』。

和『人偶』们使用的『龙核热炮・放射』相似，呈直线放出『光之炮击』的技能。
那并非是魔法只能称之为技能。
威力也和『人偶』的『龙核热炮・放射』相同，不过我能够自力用魔法力制御进行控制，所以『在某种程度上』能够曲线射击。
并没有『原子・镭射』那麼快的速度，也不像『远程操纵・镭射』那样能够自动追踪对象——不过威力完全不同。
被这个命中的情况下，基本一瞬间身体就会『融化』『气化』『蒸发』。

就算对方是莎米艾露，被这个命中也不会平安无事。
嘛——虽然会被转移避开。

2是『原子・球体』。

解放掌上制御的『核融合』产生的『光之集合体』，膨胀成直径数米～数十米的球体，将对象关在其中的技能。
消灭勇者亚克斯就是使用了这个技能，被关在裡面的傢伙会暴露在超高温+放射能的暴风下，基本上都会被溶化消灭。
虽然论攻击力話『原子・放射』威力更大，不过被关闭在持续受到核的暴虐中可不是个开玩笑的技能。被关在裡面的話，就算是莎米艾露也不会平安无事。
嘛——虽然会用转移逃走的。

3是狂妄的『核爆发』。

故意让掌上制御的『核融合』产生的『光之集合体』暴走，然後使用转移石逃到影响範围外的『逃跑技』。

这个威力正如『核爆发』。半径数十公里～数百公里不仅会被超高温超爆炸卷入，而且还会洒下放射能是非常麻烦的必杀技（？）。

嘛——莎米艾露的話（以下略）。

对我来说，莎米艾露真是不妙啊。
明明具备这麼多可怕的必杀技，但却完全没用真是没道理。
虽然像大魔王那样光是存在就没道理的只能放弃，不过作为同格的『魔王』规格外也要有限度。
该说不得不找这种傢伙打架的勇者真是有勇气吗——还是说他们是抖M吧。
至少对我来说绝对不要。
比如说勇者编织出了『非常厲害的必杀技』。

对莎米艾露使用这个必杀技→转移躲开→转移到身边→用怪力击穿身体→结束

这对勇者来说『要怎麼办才好！』的程度。
光是『转移』就很棘手了，但却能够『一瞬』并且『连续』转移，并且还拥有着『没道理的怪力』。

会挑战这种傢伙的只有『勇者（笨蛋）』吧。

「所以大魔王大人不要说，并且也绝对不要找莎米艾露大人的茬。记得让『人偶』们彻底贯彻这一点」
「我明白了，主人」

辉夜老实地点头，用『念話』向其它『人偶』们传达这件事。

「……」

说实在的，现实就是就算我和索菲娅+178体『机械・人偶』也无法胜过莎米艾露1人。
总觉得，莎米艾露1个人就够了吧？

