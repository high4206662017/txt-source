
　在暑气已然消散无踪的夜间气氛中，闪烁的交通信号显得颇为纷扰。
　由於立刻要前往异界，於是连忙做好出门準备，现在刚好是离开公寓的时候。距离睡觉预定时间十一点还剩下一小时左右。考虑到明天还有工作，没办法无谓地浪费时间。
。
　明明之前还那麼否决，一知道有现金收入就是这样子。这就是所谓恶魔的甜言蜜语吧。
　在坐在肩膀上的少女带领下，以异界为目标逐渐步行而去。

「诶——我从那边感觉到气息。在那边左转……嗯，就是那裡」
「真的假的，我每天都路过来着」

　抵达的地方是平时经常光顾的便利店後面的巷子裡。眼前所见之处平淡无奇，有点难以相信这种日常生活中居然有恶魔栖息的奇异空间。
　环视周遭。
。
　除了便利店的灯光外，仅有路灯上的灯火以及居民楼房泄露的零散亮光。这麼漆黑的巷子裡有着设好路障的车辆、观叶植物、空调室外机，是个十分普通的地方。
　望着面向大马路的便利店停车场，只见有幾位年轻人坐在车档上嘻嘻哈哈地聊着天。怎麼看都不像是名为异界的特殊场地。
　亘起疑地左顾右盼，边往前面走着。

「好，停下来，那边就是入口」
「看起来什麼也没有……既然有入口就代表有可以进入异界的地方吗」
「姆ー我可没有说谎哦。稍微等一下――好了，走吧」

　那边有电线杆与从中延伸出塗上黄色的支撑线，少女飞到其附近，在空无一物的虚空中仿佛敲着门一般敲了幾下。
　於是那块空间仿若水面般泛起一阵涟漪，张贴在对面围墙上的防盗广告，其人物的表情刚刚好扭曲成在嘲笑一般。
　亘不禁發出惊讶的声响。

「噢噢！总觉得很有感觉」
「怎麼样，这就是入口哦。好了啦，别發呆了，快点进去吧」
「哦、哦」

　在她催促下，亘半信半疑地走近那块摇曳的空间――闭上眼睛闯入其中。不知会發生什麼事，他有点害怕。
　不过并没有特殊的感觉。
　不如说也就撞上刚才看到的围墙而已。

「主人还真是冒失鬼呢」
「好痛。喂，这是怎麼回事啊，什麼……变化也……」

　正当亘想要抱怨时，他意识到周围莫名的明亮。除了有光照以外，原本暗淡的巷子裡显得略微明亮，得以望到巷子尽头。
　他不禁抬头仰望天空，理应一片漆黑的夜空变成灰色。就这样垂下目光眺望远方，前方几十米处的景色全都化为朦胧的灰色绘画。
　更重要的是十分宁静。
。
　以往纵使是在巷子裡多少还是得以听闻喧嚣声。如今既没有公路上相互行驶的车辆声响，亦没有年轻人的傻笑声。望向便利店，就连原本集聚在停车场的年轻人们都消失得无影无踪。
　巷子一旁的民居楼房也仅是回以静谧，甚至是空调室外机都没有运转的声音。无论是何处都听不见半点声响，人们生活的声音都全盘消失殆尽。

「原来如此。虽然跟我想象中的不一样，不过的确是有着异界的氛围」

　他对於异界这一字眼所抱有的印象是荒凉之地。可实际上呢，无非只是普通的街区风景。
　然而宛如闯入绘画或是照片当中，令人感到这异质的世界实在是毛骨悚然。

「来吧！鼓起精神打倒恶魔！」
「好吧，重要的是跟恶魔战鬥。虽然到现在才说，我有点害怕起来了，好紧张」

　少女精神奕奕地举起单手，可亘则是考虑到与恶魔的战鬥而有所不安。尽管战鬥这个字眼说的倒是简单，可接下来要付诸行动的是拼上性命的厮杀，有可能会导致死亡。纵使已然做好觉悟，一旦到了紧要关头一般还是会感到害怕。
。
　为了重振精神，他深呼吸一下。随後确认自己的装备。
　手上握持的武器是放在公寓裡用来护身的金属棒。防具则是安全头盔、军用手套以及安全鞋。衣服是运动服搭配牛仔裤，以便於行动为主的打扮。为了尽量提高防御力，外头还披上一件不合季节时宜的厚重防寒衣物。

「話说回来，这种打扮完全就是可疑人物吧」

　虽说是这种穿着而来，不过要是在路上被人看到说不定就报警了。
　能够平安无事地来於此地，是因为带路的少女具备不可思议的力量。没想到她能够探知周围人的动向，这是多麼方便的能力。
　这时他才注意到一件重要的事情。

「说起来我还没问过你的名字吧。顺便一提，我叫五条亘。你呢？」
「这种事情应该是最先开始问的吧」
「抱歉，接二连三的震惊事，我没想到这点」
「哎，算了。不过我呢，其实是没有名字的啦。毕竟从魔的名字是由召唤者决定的。所以说你来想好名字吧」
「是这样吗。可是啊，就算你这麼说……」

　他抱着胳膊深入思考。
　名字会背负一生，足以影响到自己的人生。不能因为兴趣以及潮流就乱取名。能够给自己的孩子取个好名字的机会已然剩下十年不到。
。
　她的眼神闪闪發光，激动不已地等待这边开口。压力真大。
　重新端详一下，她是位精神开朗的女孩子。活泼的气质与那身不可思议的巫女打扮十分相配。机会难得，虽说日式的名字是不错，不过还是潮气蓬勃的名字比较好。比较再怎麼说她也不适合文静或公主这种淑女般的气质。
　论及巫女，那便是告知祈祷亦或是占卜神谕的存在，藉由跳起神乐侍奉众神。这时他想到一个简单的名字。

「是啊……那就神乐怎麼样？」
「嗯！我就这名字好了。谢谢你，主人！」

　神乐扬起嘴角，像是衷心感到开心似的露出笑容。
　她回转身体，犹如在跳舞一般，连尽到取名重任的亘都为此感到高兴。在做着这种事情的期间，亘所感受到的紧张与不安也变得相当淡薄。

「寻找恶魔的时候就不要离这裡太远，保持随时都能逃跑的状态吧」
「嗯，可是没必要寻找哦」
「为什麼？」
「你看，客人已经来了呀」
「诶？」

　随後听到了细微的脚步声。由於异界十分安静，因此一点小声都觉得很响亮。在周围应该也能清楚听见刚才亘与神乐之间的谈話吧。
　前方暗淡无光，当中出现一道朦胧的人影。
　身高仅有小孩子的程度，接近过来後轮廓才一目了然，那是与方才提到的小孩子截然不同的生物。
　它肌肤犹如死人般呈现土棕色，仅有一张皮肤的骷髅头部已然秃落大半的头髮；身体只以破布缠绕于腰上，尽管瘦弱得肋骨都显而易见 然而腹部却涨得颇为异样。
　在那双黄色浑浊的眼神注视下，亘意识到对方将自己视为猎物而起鸡皮疙瘩。

「这就是图鉴上的饿鬼，我可以用魔法打倒它，所以没事的。『雷魔法』」

　神乐制造出噼啪作响的雷球，以自然而然的举动释放而出。
　威力正有她撂下“没事的”程度，而惨遭攻击的饿鬼则發出悲痛声倒着地面上。然而，它并没有一击毙命。

「咦？还在动弹。嗯~比起我想象中还要顽强呢」

　倒下的饿鬼就这样像是挣扎般往这边爬过来。它的目光始终在亘的身上，甚至都感觉到它的一种“至少要满足肚子”的疯狂渴望。

「主人不要在發呆了，快到安全的地方、诶诶诶诶!?　等一下主人！你想做什麼呀！」
「噢呀啊啊啊啊啊！」

　亘發出宏叫，飞奔而出。
　他使尽浑身的力气用金属棒砸向爬行的饿鬼。
　手感沉重而又湿哒哒的。与此同时，某种液体溅洒而出，然而他依然忘我地反覆举起金属棒挥下去。即便骨头折断、皮肤裂开、肉块和臟器裸露而出也毫不手软。
　当饿鬼变成细碎的粒子消失不见後，就响起金属棒敲击到柏油路的声音。

「哈啊、哈啊……怎麼样，我打倒了喔」
「主人居然自己战鬥，我的存在意义……哎，无所谓了。比起这件事，主人也差不多该拿出手机了」
「嗯？为什麼啊」
「因为一定要在DP消失之前储存起来」

　闻言，亘慌张地取出手机。用仍在颤抖的手拿好手机面向那边後，便响起一声短促的电子音，屏幕上也显示着3DP。
　尽管不知道是怎麼做到的，不过DP这种物质似乎被手机储存进去了。

「好，这样就回收DP了。太好了呢，恭喜主人首次战鬥胜利ー」
「噢噢，是吗」

　听到神乐拍手和讲話的声音，亘终於放鬆身体。
　刚刚打倒的饿鬼身体已经完全消失不见。由DP构建出的概念存在应该是回归成DP原本的状态吧。死活连残骸都不复存在，这是多麼变化无常的事情呢。
