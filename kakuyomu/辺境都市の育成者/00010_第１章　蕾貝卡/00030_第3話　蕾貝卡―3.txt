内心不由得感到动摇，没想到来得这麼突然。

「那个……我是公会来送东西的」
「什麼啊，那傢伙连这个工作都偷懒了啊。……下次就不给他灸疗了」
「灸疗？」
「啊，这是这边的事情。谢谢你，帮大忙了」

男性轻轻挥挥手回答道。
……总感觉是个奇怪的傢伙，迅速递给他後就回去吧」

我从腰间的物品袋子中取出信封和小箱子。

「这个给你。还有我不想在这之後产生纠纷，所以拜托你找张纸在上面签字」
「好好，稍微等一下呢」

男性摸了摸外套的口袋。
然後很抱歉的说道。

「抱歉，手上没有呢。先进来喝口茶吧，那傢伙每次也会来这裡喝茶呢」
「哈、哈啊……不，但是我……」
「好啦好啦，偶尔和你这样可爱的中坚冒险者聊聊天也很有趣」

男性这样说着进入到废弃教会裡面。
……说实話不太想进去。
不过关键的东西还没有交给他，而且也稍微有点在意。

――到底是谁经常来这裡喝茶？

那个完全没有男人气息的无聊女性来这裡喝茶？
如果这是事实的話，光是这个捏他就能玩上一年了。
这样做好决意後，我推开门进入到裡面。

「这边哦，快点过来吧」

男人在裡面挥了挥手。
看来这个宽广的空间以前作为礼拜堂，男性所居住的空间在更裡面的样子。
嘛，看来他没有在这种地方生活。
於是追上去询问他。

「为什麼住在这种地方？」
「虽然说起来会很长，不过要说的話这只是意外的相逢吧。还有就是这裡的房间很宽敞，便於放东西哦」
「放东西？」
「比起嘴上说不如亲眼一见呢，请吧」

这麼说着打开了门。在那裡的是——

「！？」
「这个反应真是不错。最近那傢伙都没什麼反应真是讽刺，明明以前是那麼天真烂漫的……时间的流逝真是残酷啊」
「这、这是、什麼啊……」

无法用話语来形容。
眼前的是延伸到天花板的巨大货物架，并且拥有好幾列。
上面陈列着无数的剑、枪、斧等武器，和我所拥有的级别完全不同。这些全部都是魔剑……。
其中还有明显是上级魔石与宝石的原石。就连在拍卖会也没见到过这种情况。
以及还有带着强大魔力的无数书籍。并且就连以前还在家裡时，只见到过一次的禁书也存在。
再加上……难道说——小心翼翼地靠近过去询问道。

「……这不是龙鳞吧？」
「啊，那个啊。好像是赤龙的样子。从王都寄过来『抱歉，杀掉了呢！』这样的书信呢」
「…………」

无法理解他在说什麼感到十分茫然。
刚才说了真龙了吧。
只要志愿成为冒险者，所有人的梦想都是打倒真龙。

「以前我稍微推了一把的孩子们，到现在都还在将各种东西寄过来呢。虽然我说过寄信过来就可以了……不过大家都不听我说的話呢」

我注视着男性的脸。
这个瞬间——回想起作为职员的那孩子所说的話。

『那名男性自称培养者』
『拜托那名男性培养的冒险者，现在所有人都成为了大陆级别』

难道说这是真的？
被我这样注视之後，男性露出了困扰的表情。

「怎麼了？」
「啊……什、什麼事都没有」
「是吗？你现在带过来的东西恐怕就是这个呢。要是在意裡面的东西的話，之後再打开看吧。快来这边」

过道到底是怎麼回事，明明只有一列但却感到十分宽敞。
过道的尽头又是一扇门。
打开之後，裡面明显有着生活的痕迹。
男性指着椅子向我搭話道。

「坐在这裡吧，你喜欢红茶还是咖啡呢？」
「那就红茶吧」
「好好，喜欢吃甜的东西吗？」

我点了点头。男性露出一副笑脸吹着口哨。
……感觉他完全就不像冒险者。更看不出他是打倒真龙的冒险者的师傅。
因为闲得无聊，於是坐在椅子上看了看周围。
这是漂亮又整洁的房间。无论如何都无法想像这是独自一人生活的（并且是男性的）房间。
比我所住宿的房间要漂亮不少。怎麼回事，这种败北感。
然後忽然察觉到。咦，从客观上来看——

（我被陌生的男性带到了房间裡！？）

抬起腰，椅子發出咔嗒的声音。
感到十分动摇。要说的話至今为止都没有经歷过这样的事情。我还是第一次进入到男性的房间裡。

「怎麼了吗？给你，如果合你口味的話就好了」
「啊、呜、那、那个……」
「真是个奇怪的孩子。快吃吧」

男性递出小盘子，上面盛放着三角形白色的点心？在那正中央有着小小的野莓。

「有吃过吗？这被称为草莓蛋糕哦。不过再怎麼说都没能获取到草莓呢，於是就用野莓代替。啊，还有红茶也给你」
「……我开动了」

不会突然就在裡面下毒吧。……大概。
将点心送入口中——然後感受到一股强烈的冲击感。什麼啊这是！
非常得甜，下方部分非常柔软。在那其中也放有野莓。
从、从来没有吃到过这样的东西。
当我沉浸在其中的时候，感受到一股柔和的视线。

「还喜欢吗？」
「呜……非、非常美味、谢谢」
「那真是太好了。给你这是签名」

男性递出小纸张。我接过手後，将信封和小箱子交给了他。
――签上去的名字叫做『春』，真是少见的名字。

「还没有自我介绍呢。我叫做春，姑且作为一名培养者。在这裡和你相识也是某种缘分，需要有益的建议吗？第8阶位冒险者蕾贝卡」
