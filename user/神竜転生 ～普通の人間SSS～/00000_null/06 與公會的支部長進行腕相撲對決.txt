
（这裡我要解释一下腕相扑，简而言之就是掰手腕！手腕！腕！腕……）

“喂，那个，这是怎麼了啊”

其他接待小姐看着散落满地的水晶碎片，都好奇發生了什麼事情。

“……那个，这个孩子碰到水晶的时候，不知怎的就碎掉了……”
“欸！？难不成是拥有B等级魔力的人！？”
“不会吧，即使是B等级，也不会变成这个样子的，应该是坏了吧。稍微帮忙收拾一下吧。啊，艾莉桑，没有受伤吧？没事么？”

艾莉与神龙的时候相比大幅度的变弱了。
再加上克雷阿蜜拉给的吊坠，有大幅度的弱体化了。
即便如此，在水晶破碎的情况也是不会受伤的。
即使受伤了，也会瞬间再生的，谁都不会注意到。

“是的，我没事”
“是么，那太好了，你这样可爱的女孩子如果受伤了的話就不好了呢……虽然不是对志愿当冒险者的孩子该说的話呢”

接待小姐苦笑道。
接着将接待任务交给一个人，剩下的两人用扫帚和簸箕将破碎的水晶碎片收拾乾净。

“嗯？突然开始打扫了，是發生了什麼事情了麼？”

在那裡的，是一个壮硕的男子。
年龄是五十岁前後。
长着壮观的鬍子以及不输给鬍子的壮观的肌肉。
是个像熊一样的男人。（译者：优奈表示这种熊熊并不需要）

但是，看其穿着，给人一种粗暴的印象。

“啊，支部长。观测水晶好像出了故障。这孩子一碰就碎掉了”
“故障？咦？才使用了幾年而已，难道混入了劣质品么？这次看来得向总部提出索赔呢。我来拿新的水晶吧”
“得救了，支部长”

看来，这个像熊一样的男人是公会裡很伟大的人呢。
但却自己出去买杂物。
弱气，被部下任意使唤--的样子好像没有。
单纯是直爽的性格的样子。

“艾莉桑，刚才的是这个城镇冒险者公会最伟大的人，支部长哟。虽然我觉得新人并没有什麼机会和支部长说話，但是机会难得，得记住他哦。还有，刚才和你说这个城市中有一个C等级魔力的人吧，说的就是支部长哟。最强大，最伟大，不论是公会的职员还是冒险者们，都很尊敬他哟”
“原来如此，我明白了”

艾莉没有深入思考地点了点头。
接待小姐们打算完後，正好支部长带着新的水晶回来了。
在水晶上，“C”这样的文字高高的浮现出来。
如果能好好测量魔力的話会这样现实的么？

“谢谢，支部长”
“没什麼，能稍微减少忙碌的你们的负担的話，圆滑是公会运营最重要的东西了呢。接下来，让我们看看让水晶破碎的少女的魔力等级是多少吧”
“真是的，支部长，不是这个孩子弄碎的，是因为故障所以才会碎掉的啦”
“我知道的哦，但是……怎麼说呢。我从这个少女的氛围中感受到了不可思议的气息。将来一定是不得了的大人物”（译者：为啥不认为现在就是不得了的大人物呢？）

“嘛，支部长这麼说的話，也许就是那样吧。但是……艾莉桑。即使真的拥有作为冒险者的才能，但是现在还很小，勉强的事情不能做哦！”
“umu，正如阿忒露（アデル）桑所说的，无论多麼强的冒险者，也会因为一时的疏忽而丧命的。疏忽是大忌。嘛，变成说教了呢。我已经确认过了这个水晶能正常运作，接下来就轮到你了”（译者：这裡叫阿忒露用的是君，这裡是对晚辈的称呼，不过因为是妹子，我就用桑了）

“那麼”

艾莉再一次触碰了水晶。
啪哩噫噫噫吟！（バリィィィィン！）（译者：水晶破碎声）

又变得粉碎了呢。

“……那个。这个水晶也坏了麼？”

艾莉认真地询问。
但是，阿忒露和支部长看着水晶的碎片，没有回答。
於是，艾莉将视线转向其他的接待员。
但是，她们也是一脸呆滞。
不仅如此，在接待处附近的冒险者们也一声不响的固定住了。
艾莉有一种不祥的预感。
这个水晶只能检测到C等级的魔力。

所以别说艾莉原来拥有SSS级的魔力，就连用吊坠变成A等级的魔力触碰後会变成什麼样也不知道。
和B等级一样响起哔哔的声音，然後被认为是B等级然後结束--这是艾莉原本的预定。
但是，从大家的反应看来，感觉好像比B等级还要高。

“支部长……这也就是说……？”
“这个孩子……拥有B等级以上的魔力啊！”

对於阿忒露的问题，支部长颤抖着回答道。
那个瞬间，公会裡的人都深吸了一口气，然後炸开了。

“B等级以上是真的假的……”
“那麼小的孩子么……”
“但是实际上看到了水晶炸裂了”
“B等级以上的話，也有可能是A等级么……？”
“不，果然应该没有到那个程度吧……”
“也是啊……”

好像并没有人认为是A等级。
真是太好了，艾莉松了一口气。
反过来说，只要发现是B等级，就能造成如此程度的骚动。
看来在这个世界，引起骚动的难度微妙的很低呢。
光是跳跃到云层，最高神和天使就会铁青着脸看着的时间点就察觉到了，但是真是想象之上呢。
艾莉才刚变成人类，要如何生活还没有决定好。
想着首先要看看这个世界的时候已经变得太显眼了。
但是，如果因为不想要引人注目而四处都暗藏的話，那就不知为什麼而转生成人类了。
控制平衡真是个难题啊。

“要检测B等级以上的魔力的話，要去王都叫来拥有魔力鉴定技能的人才可以呢……对了，你叫什麼名字？”
“啊。艾莉·艾玛迪斯”

说起来，我还没向支部长作自我介绍呢。

“原来如此，艾莉桑么。先将你作为C等级魔力持有者来看待吧。但是！正常运转的水晶在接触到你的瞬间就發生了故障而无法测定魔力。那麼……和我进行掰手腕对决吧！用魔力强化筋力！”（译者：这裡叫艾莉也是用来君，理由同上，下文不再说明）

随着支部长如此叫喊，周围响起了欢呼声。

“太棒了！有可能是B等级的孩子和支部长进行掰手腕！竟然能看到这样的战鬥！今天真是太棒了！”

如此感受到的冒险者们骚动起来，不知从哪搬来了桌子，为掰手腕做準备。
接着，艾莉就这麼被带到了桌子边，与支部长拳头相互握紧。

“ready……go！”

伴随着阿忒露的意外的呼声，掰手腕开始了。
冒险者和公会的职员都很闹腾。
支部长脸颊通红，想要将艾莉打倒。
当然，艾莉则是十分有餘裕。
虽说用魔力强化来增强力量，但看起来像用单纯的筋力来进行掰手腕。
现状是，从开始到现在，双方的胳膊都没有颤抖。
那是因为艾莉正在烦恼到底是该赢还是该输呢。
如果战胜了这个城市最强的支部长的話，会十分引人注目。
但是，如果输了的話，可能会被认为因为水晶發生故障，不可以介绍冒险者工作。
如果艾莉打算作为人类继续生活下去，无论如何都需要金钱。
这裡应该不太花俏地赢吧。
这麼决定了的艾莉，将支部长的腕压了下去
公会内响起了了一声啪叽的声音。

“呜……呜呀！断了啦！”

支部长的胳膊朝奇怪的方向弯曲了。
骨头折断了。

“啊，不好了！现在就帮你治好呢！”

艾莉慌慌张张朝着在地板上忍受着疼痛的支部长使用了回復魔法。
於是，支部长的胳膊的角度回到了原来的模样。
太好了，太好了，艾莉安心地吐了一口气。
但是。

“治好了……？那麼严重的骨折一瞬间就……？多麼高等级的回復魔法啊！艾莉桑！你毫无疑问持有B等级以上魔力！”

支部长握着艾莉的手，热情的说道。
然後周围的冒险者也“呜啊啊啊啊啊！”的大声叫喊着。
艾莉察觉到自己失败了，但这已经太迟了。
没想到只是治疗个骨折就大吵大闹的。
而且，在这个城市裡最强的人，那麼简单就骨折也是预料之外。

“不仅仅用力量压倒性地战胜了支部长，还会使用那麼厲害的回復魔法……B等级真厲害！”

艾莉自己并不认为这是一件很厲害的事情，但是周围的人却胡乱地将她抬起。
那时，艾莉第一次学到了一种叫做『恥ずかしい』（害羞）的感情。
脸一下子热得仿佛要喷出火来。
羞得无地自容，恨不得马上找个地缝钻进去，於是猛得逃向公会深处。
-------------------------------------------
如果稍微觉得有趣的話能“评价”或者“书签”的話我会很高兴的！
