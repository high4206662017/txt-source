「──啊呀，真沒想到『行世之死』只是唱歌就能奪取亡靈軍團啊」

在「米凱涅的遺跡街」，平安的度過與邪惡的亡靈術士不期而遇，我回到斯巴達的家中。現在正優雅的單手拿著咖啡杯，對先一步回來的沙利叶，說著之前發生的事情。
看起來，沙利叶也是才剛回來，就這樣穿著修道服。為了迎接我似乎都沒空換成女僕服。不過，只有圍裙是好好的穿在身上，不由得感受到一股獻身精神。

「屍靈術也有數個系統。恐怕、主人能輕易的奪取控制權，是因為對手的術式針對同時操縱大量奴僕進行特化」
「啊啊，那傢伙一個人就驅使數量龐大的僵屍群，正因為如此總覺得魔法很『薄』」

亡靈術士左袒・索魯達的強大之處，正如沙利叶說的那樣。總之就是數量龐大的魔物，同時、並且短時間內亡靈化，轉變成自己的奴僕。因為沒有任何同伴，他真的是只身一人支配了「米凱涅的遺跡街」近半數的魔物。
我依靠「行世之死」，把他最突出的長處「數量」完全無效化所以才如此輕鬆，實際上、真是令人恐懼的支配能力。

「據他說所，讓自己的奴僕去撕咬對方，似乎就能通過術式進行支配」
「⋯生化」
「正是如此！」

聯想到的，就是那個著名的與僵屍戰鬥的系列作品。聽到這個感染式的屍靈術的秘密之時，不由得喊出作品名字，左袒和西蒙，都不明白那是什麼，一臉不明所以的表情，有點寂寞。

「知道日本的人，只有我」
「大家都不知道覺得很遺憾⋯嘛，還是有人知道，就算不錯了」

即使是沙利叶也好希望得到他人同意，所以才說這些事。雖然不甘心，只有這個部分，會不由得會容許她踏入。開拓村和沙利叶共同生活終究也挺開心的，因為是同鄉，有不少能互相理解的地方是一大要因。

「總之，是個擁有恐怖力量的人」
「是的，最糟糕的情況下，都市裡的所有居民都有可能會亡靈化」

真的就是生化，不對，是左袒危機。
順便一提，被咬就能感染的僅限魔法耐性很低的一般人或者是低級魔物，多少有些抵抗力或者強大的意志力就不會變成僵屍。嘛，即使有耐性，被其他僵屍襲擊而死的話，結果也會變成僵屍復甦，愉快的變成他們之中的一員。

「相對的，懸賞金也相當有份量」
「取得項上人頭了嗎」
「不，活捉了」

老實說、因「行世之死」而失去僵屍軍團的左袒，可憐的連還手餘地都沒有。姑且、有自保的精銳部隊，留有幾隻詛咒之歌都無法奪走控制權的強力奴僕⋯不過並沒有禁得住「貪慾」砲火的頑強奴僕。
之後，都不用我動手，被奪取支配權的僵屍們擅自就嘿咻嘿咻的把左袒抬了出來，然後扔到我面前。也不知道被僵屍們怎麼蹂躪了，倒在我面前的左袒只有一條內褲的破破爛爛狀，身體上還有「我乃不死者之王」的涂鴉。

「同情、嗎」
「怎麼可能。那個感染式的屍靈術實在太危險了。必須要從本人那裡得到包括解咒方法在內，了解到所有秘密才行，要不然怎麼能夠安心」

如果，就這麼直接殺掉，結果在什麼地方漏了幾隻左袒的奴僕開始進行大增殖的話就大條了。或者，有其他編制出類似術式的人，會來到台面上。
這傢伙傾注畢生精力做出來的，這個危險的原初魔法，是針對屍靈術犯罪的貴重參考。付出巨大犧牲得來的魔法。今後，應該為了拯救更多的人進行利用。

「沒有犧牲者，逮捕犯人。作為狀況的結局，我認為是完美了」
「就因為這事，古代哥雷姆的調查幾乎是毫無進展」

因為大量的亡靈軍團支配了「米凱涅的遺跡街」的魔物全都不在了，暫時逃跑的魔物一口氣開始跑回來，為了決出新的支配者變成一個巨大大逃殺賽場。至少得等到魔物之間的領地之戰穩定下來，要不然無法進行調查。

「話說回來，你那邊怎麼樣了？」
「是，升到等級４了」

沙利叶毫無感想的回答道。確實，要說當然也是當然的結果⋯⋯同一天出發，比我更早回來然後達成升級條件，果然耗時非常短啊。到底是有多高效練級的感覺。

「不愧是你，恭喜」
「⋯非常感謝」

眨眼之後，又慢了一拍才回答道。我是率直的褒獎啊，就這麼意外嗎。嘛，說的也是。

「你的實力，等級４以下的魔物之流想必是很輕鬆⋯有什麼，收獲嗎？」
「有。從BOSS魔物那裡得到了高級防具」

原來如此，防具嗎。這麼說來沙利叶從開拓村教會那裡拿出來的，普通的修道服就是任務中穿著的服裝了。成為冒険者之時，菲奧娜為了讓她整頓裝備，給了十萬克蘭，結果、沒用那些錢買防具。之後的冒険生活裡沙利叶自身也有收入──當然，我和菲奧娜、都沒有沒收沙利叶自己賺到的錢──因此，應該攢有不小的金額才對，是因為覺得沒必要嗎，沒聽說有買防具。要說，就沙利叶的實力有最高級品的「反逆十字槍」和作為雙腳的天馬白，就足以應付多數任務了。
這樣的沙利叶特意入手了防具，也就是說，沒有賣掉而是拿來自己用，是與元使徒相稱的逸品。不可能不在意。

「是嗎，是怎麼樣的防具？」
「現在，就著裝著⋯您要過目嗎？」

讓我看看，立刻就點頭了。
總之，現在沙利叶是修道服與圍裙。不是「暴君鎧甲」那種渾厚的防具，也沒有披著魔術士的長袍。這麼說，就是一眼看不出來的，僅僅只有護手，或者是小型的飾品之類──就在我這麼想的瞬間，沙利叶脫了。
連阻止的機會都沒有，純白的圍裙也藏青的修道服在空中飛舞。

「是『墮落宮的淫魔鎧』」

那是、能襯托沙利叶那純白裸體的，漆黒的皮革制品。泛著黒光又光滑的皮革，長手套與過膝長筒靴，完美的包裹著沙利叶那已經完全再生了的美麗手腳。不過，胴體那邊就就是僅僅只有最低限度的布料面基。我覺得這個比菲奧娜一臉得意的在晚上穿出來的決勝內衣，都要小一圈。

「喂，這個工口的內衣是」
「是淫魔鎧」
「比基尼盔甲？」
「是淫魔鎧」

沙利叶頑固的強調正式名稱。是因為不想承認這是糟糕的裝扮嗎，這是沙利叶本人的想法，還是說這是白崎桑殘留思念的效果。那樣的話就別穿啊。
總之，無法直視。沙利叶的裸體已經看慣了，還無法如此斷言，在開拓村生活那會兒洗澡的時候是看過好幾次了。即使如此，對我來說還是覺得誘惑。可以說是太誘惑了。不過無法移開視線這點，更恐怖。
哇，這是啥，仔細看還能看到身後有條像是惡魔的那種尾巴。鞭子一樣細長的尾巴，尖端是銳利的倒心形。那玩意在沙利叶腿後邊，慢慢的左右晃動。
這還真是有夠無聊的機關啊。不過可愛，超可愛，沙利叶可愛到──哈！？冷靜下來，我、這不是中了魅惑了嗎！

「知、知道了⋯已經可以了，穿上衣服」
「這個裝備屬於鎧甲。不是內衣」

不，我沒有在尋求「因為不是內褲所以不覺得羞恥」這種理論。

「別廢話，快點穿衣服」
「明白了⋯不過，在穿之前有一事相求，主人」
「穿好之後不行嗎？」
「不是這個樣子，就沒有意義」

還真是強硬啊，罕見感受到毫不退讓的意志。在這裡，與一身痴女樣的沙利叶進行問答也沒有意義，那就試著去做吧。

「知道了，什麼事？」
「希望能幫我戴上項圈」

就這樣，沙利叶遞出來的，是黒色的皮革制項圈。從那個色澤，設計樣式來看，肯定是和這個淫魔鎧成套的。

「這種事，你自己弄就好了吧」
「不是主人戴上的話，就無法發揮出裝備的力量」
「真有這事？」
「是的，這個鎧甲──」
「不，算了，詳細的說明就免了」

所以說，以這個樣子慢慢瞎扯可不行。我的理性，已經不剩多少了。

「戴上項圈就好了吧⋯」
「非常感謝您」

無可奈何的從沙利亞那裡，接過項圈──誒，這個項圈，帶有濃郁的魔力啊。不僅僅是個變態裝備呢。
不是，無論是帶有多強魔力的高性能裝備，外觀很變態這點是不變的。為什麼偏偏就要，選了這種玩意兒啊，現在的沙利叶應該還有不少其他選擇的吧。

「哈，真是的，為什麼是這種──怎麼樣？」
「請再稍微，勒緊一點」
「沒事嗎？」
「沒事」
「知道了⋯這回呢？」
「可以了」
「可以嗎？」
「可以了」
「好，那麼就這樣固定──」
「我回來了」

咔嚓一聲，客廳的門打開了。那個聲音與台詞，能想到的僅有一人。

「啊啊，歡迎回來，菲奧娜」這樣，我理應如此回答，然而我僵住了。
為何？
重新審視現在的狀況吧。
在客廳正中央，沙利叶穿著只在漫畫裡見過的下作比基尼，然後我正給她戴上項圈。看到這幅畫面，到底誰可以得出「原來如此，因為是特殊裝備所以正在戴項圈呢」這種結論呢。
不管怎麼看，都只會讓人誤解吧。
不過，即使知道如此，我也只能這麼說了。

「等等，菲奧娜！這、這是個誤會──」

一回神，時光如梭，遠雷の月３１日。我們的生活，沒有太多變化。
今天也是從早上開始，幾乎每日都和沙利叶對打。

「──唔哦哦！」

揮過眼前的是，沙利叶的指尖。應該被黒色皮革完全包裹著，甩開破空聲橫掃而過之後緊跟著空中回旋踢，基本上和利刃一樣的斬擊依舊不變。還附帶著噼里啪啦的紫電，似乎在揮舞著雷之魔劍。
即使如此，這是普通的紫電，就說明沙利叶並未使出全力。認真的話，就會變成像我的疑似黒色魔力相似的赤黒雷電，這是已知事實。
即使如此給逮個正著的話，即使戴著「暴君之鎧」的頭盔，也會無法避免腦震蕩。附帶的，因為雷擊的效果視覺影像在內的諸多機能，會一時間強制停機。
可以輕易想像出深刻的傷害，已經，受不了輕易吃招了。現在看穿回避的概率只有五成，這次是賭贏了。
一次跳躍中，三、四次就像龍卷風一樣的猛烈踢擊，撐過去之後終於等到貴重的反擊機會。輕飄飄的，沙利叶著地了讓人感覺不出重量。不過，這不可避免的會出現空隙。
為了不錯過她雙腳著地的瞬間，迅速、銳利的出拳。以全力，不、再加上「暴君之鎧」的腕部搭載的輔助噴射器的推進，獲得更快與更強的破壊力。

「呼」

沙利叶漏出小小的呼吸聲，我的拳頭命中了。她舉起來的，掌心。
已經明白我會瞄準著地瞬間吧。無論多麼迅速的攻擊，只要能夠預測就能進行準備。
不過，沙利叶應該可以應對的吧，這已經清楚到讓我覺得厭煩。要突破防御，給予她打機的方法有兩個。以無法防御的威力打中，瞄準致命性的空隙。
然後，這次是前者。以一只右手，施加我的全力，被噴射器加速的渾身直拳也能擋住嗎──

「──額！？」

沖擊的感覺，驚人的輕。硬是突破強烈的反動力，在沙利叶那平緩的胸口用盡全力，就用這個包裹著黒鋼的拳頭打上去。
失誤了，不是這麼回事。我的拳頭，準確的徑直擊出。沒錯，是沙利叶那邊動了。
接住拳頭的手掌，滑動般向下移動。沒什麼大不了的，僅僅、後仰上半身而已。
不過，那個幅度可不一般。不是要折斷腰肢一般後仰的上身，是膝蓋呈現直角，就這樣仰面倒下的氣勢。以這種變態的動作撐過必殺的一擊。
不過，即使是沙利叶，這個體式也太過了。要起來，必須、先讓手著地。以人體的構造，這個狀態下是不可能直接起身的。
即使，回避了這一擊，也沒有後續了。至少，我還沒有遲鈍到、容許她反擊。

「推進器！」

提升腕部推進器的出力，我強行、不過比起普通運動更快一拍的，進入追擊狀態。運用這個鎧甲的機動力，就可以抵消全力打出直拳之後的空隙。
然後，我的右拳這次以，會變成錘扁沙利叶的漆黒鐵錘，揮下，毫不猶豫與毫不留情。
沒錯，即使沙利叶身穿「墮落宮的淫魔鎧」這個比全裸還要工口的裝扮。即使這是雙腿大開，頭都要著地的後仰著，那個陷進去的比基尼胖次正沖著我的煽情姿勢。
我也全力，擊潰你。這樣，我終於贏了，沙利叶！

「是我的勝利，主人」

冰冷的宣言，無情的貫穿我的耳朵。
終究，我的拳頭沒有揮落。

「咕，是我、輸了⋯」

向沙利叶那毫無防備的身體揮拳之前的瞬間，就有利刃抵上我的喉頭。
這次的對戰，雙方之使用防具，不用武器的格闘戰。使用帶刃的凶器，所以算是沙利叶違規，沒這回事。
要說為何，那個利刃是沙利叶穿著的防具，也就是「墮落宮的淫魔鎧」的一部分。
那是，那個魅惑的魅魔尾巴。
穿過大開的雙股之間，長槍一樣挺立著。這個尾巴不是單純的裝飾，通過魔力伸縮自如，能硬化到比肩鋼鐵，聽是聽說過⋯沒錯，我在給穿成這樣的沙利叶戴項圈那時，菲奧娜剛好回來了，空虛的誤解與悲傷的誤會，結果暴動不已，沙利叶在一邊自滿的濤濤說著「墮落宮的淫魔鎧」的機能時聽到的。
到現在那已經變成令人懷念的記憶。也是想要遺忘的悲慘體驗。
總之，沙利叶使用這個魅魔尾巴來攻擊，在這次僅限防具的對戰中是有效的。畢竟我也全力使用鎧甲的精靈推進。
話說回來，我認為自己已經相當習慣「暴君之鎧」的高速機動了，在此之上沙利叶更快的適應並且活用「墮落宮的淫魔鎧」。維持成長，並不僅僅是我。

「哈啊⋯還以為，這次能贏的啊」
「原本，主人和我之間，就沒有多大的能力差距。再習慣一段時間，勝負就會對半開了」

確實，最近終於不會被沙利叶單方面壓著打了。理由很簡單。習慣了。被揍、被踢、被丟出去，和機動實驗有的一拼的痛苦經歷，不是理論，有種模糊的感覺，能夠明白、沙利叶會如何行動。
然後，之後細細推敲的結果，雖然僅有這個感覺的一小部分，能夠理解了。現在終於，沙利叶的基本招式，這種東西可以模模糊糊的理解了。

「啊啊，完全不覺得，能夠短期內追上呢──頭盔解放」

脫下頭盔，呼，呼出一口氣。明天就是六月的新陽の月了，早早的吹來讓人想到夏天的爽朗微風輕拂著面頰。
沙利叶的對戰有這堪比實戰的緊張感而被迫高度集中，即使是短時間，也很累。真的是，大汗淋漓啊。

「請用，主人大人」
「謝謝，小柩」

影子之中擅自出現漆黒而又滑溜的觸手，將先端抓著的裝入冰水的瓶子交給我。盛大的雪上插畫為背景，阿斯貝魯的天然水，寫著這種標語。

「沙利醬也請用」
「謝謝您，女僕長」

一開始對新任女僕燃起對抗心的小柩，商討的結果，小柩作為最初服侍我女僕毫無疑問的是前輩，立場更上位。所以，是沙利叶的上司，也就是說，是女僕長。這種理論，在雙方同意的基礎上成立，二人的關係就變得良好了。
不可思議的。那個第七使徒沙利叶，變成我的奴隷，菲奧娜的女僕，最後變成小柩的部下。讓我來說也有點那啥，沙利叶的立場這樣真的好嗎。
即使去想也無可奈何所以暫且擱置，現在、就想想其他事情吧。

「吶，沙利叶⋯昨天那事，你也聽說了吧」
「是的」

在後院的草坪正中間坐下。穿著厚重鎧甲隨意坐著的我，與穿著與內衣無異但是守規矩的正坐著的沙利叶面對面，在旁人看來或許相當詭異。
不過，隔壁令居的夫妻，已經完全習慣我們每天以這個樣子對打了。之前在休息日，與孩子一起三人一起愉快的觀看我與沙利叶的戰鬥。這不是作秀──不，現在就暫且不提鄰里關係。
重點，是嚴肅的事情。

「亞里亞教會，是十字軍指使的嗎？」
「沒有證據。不過，毫無疑問是十字軍的關係者」

作為我入手「暴君之鎧」的契機的那個襲擊事件。那時候，新興宗教企圖破壊運輸中的詛咒盔甲，菲奧娜給我看了其信者拿著的聖書。
這毫無疑問的，只能是十字軍的聖書。所以，進行了調查。這個聖書的出處。他們到底，從哪裡，從誰那裡，入手這個聖書的。
菲奧娜通過莉莉中意的情報屋，委託進行調查。我這邊也以我的方式，姑且去了冒険者公会發出調查任務。
不過，山人自有妙計，應該這麼說嗎，先得到情報的，是情報屋。
那個結果表明了是自稱「亞里亞修道會」的，阿瓦隆的新興宗教團體。聖書，作為亞里亞修道會的教典配給的。

「似乎很流行。稍微、有點異常的程度⋯明明、他們出現都還不滿半年」

亞里亞修道會的名字在阿瓦隆打響，是在曙光之月即將過去之時。阿庫萊多公爵家，似乎是阿瓦隆十二貴族之首的偉大貴族的代表，數十名貴族與大商人們進行巨額捐款，一夜成名。
一開始，是在郊外的古代遺跡建造的又破又小的神殿，稱之為「教會」來進行活動。不過，聚集了謎團重重的捐款成為話題，然後，不知道用了什麼手段，成功獲得大量信者。
現在得到更多的捐款，預定在阿瓦隆主幹道建起巨大教會，不對、應該成為聖堂。

「教祖是自稱聖人名叫魯戴爾的少年。年齡為１５歳，嬌小、茶色頭髮青色眼瞳。少女般可愛的面容，這種面容⋯認識嗎？」
「不，記憶中沒有這名人物。至少，教會大司祭以上的人中沒有這樣外貌特徵的人」

沙利叶作為使徒，十字教大人物的臉與名字都記在腦中。其他還有、轉戰各地之時，見過的人是非常多的。不過，這樣的沙利叶都完全不知道的人物，看來並不是十字教的重鎮直接過來了⋯不過，也不能說死。

「將魯戴爾的少年作為裝飾。操縱亞里亞修道會，十字教企圖在潘多拉布教的可能性很高」
「嘛，是呢⋯」

昨晚，菲奧娜委託的情報屋給出續報，然後我委託的冒険者們，也給出了佐證的情報，果然在阿瓦隆名叫亞里亞修道會的組織正在散布聖書聲名鵲起，這個狀況已經確定了。
然後，教祖魯戴爾僅僅是個招牌，應該有和十字軍有聯繫的幕後黒手，菲奧娜也這麼認為。

「問題是，他們是作為阿瓦隆人存在的」

也就是說「你們這幫傢伙是十字軍的先鋒吧！」遮掩斷言，我直接跑到阿瓦隆抹殺他們是行不通的。
辛克莱的人類，與斯巴達、阿瓦隆的潘多拉人類之間，在樣貌與身體特徵上沒有明確的區別，現在正在不好的意義上發揮著作用。僅僅依靠外貌，是完全無法區分辛克莱人與阿瓦隆人的。
即使，像蕾琪那樣金髮赤瞳的帕魯帕多斯人，烏露斯拉那樣褐膚銀髮碧眼的伊必拉姆人，辛克莱人也有這種獨有特徵，在這個各種發色、瞳色、膚色的人們來來往往的潘多拉大陸，無法成為決定性要素。
順便一提，臉型是棱角明確的西洋系較多，熟悉的日本人那種東洋系，完全不熟悉的阿拉伯系也零零星星存在。這個異世界，總之就是難以判斷人種。

「亞里亞修道會已經贏得阿瓦隆貴族與名人的好意，檢舉會非常困難。台下準備，已經完成了」
「可惡，我們滯留斯巴達期間，居然容許這種暗地活動」
「這已經不是個人可以防備的範疇了。該追究責任的，是阿瓦隆反情報部門」

確實，即使我移居阿瓦隆，擦亮眼睛防備間諜入侵，也不會有任何效果吧。能防止敵人的潛入的，不是個人戰鬥能力，而是国家的組織能力。特別是情報戰，我完全就是個門外漢。

「沒在斯巴達建立教會，是應為監視很嚴嗎」
「這個可能性很高。沒有臨戰爭的国家，危機意識比當事国低，成正比的反間諜能力也低，有這種傾向」

這樣是內行看門道嗎。不對，沙利叶一直在最前線，單純是因為知道辛克莱的歷史嗎。

「阿瓦隆會變成什麼樣」
「我針對阿瓦隆這個国家，還不太了解。政治、經濟、文化、宗教。綜合這些要素，正確的阿瓦隆危機應對能力為不明。不過，假設與曾經無數存在於辛克莱共和国周邊的小国同等的話──」
「毀滅，嗎」
「十字教的布教具有的侵略性，就像、之前說過的那樣」

開拓村，包含在那床上毫無氣氛的交談之中。沒錯，我確實聽過。辛克莱共和国是怎樣，支配一半天穹大陸的。
然後，十字教的布教開始了，從敵国內部開始侵蝕，這個恐怖的方法也聽說了。

「怎麼辦才好」
「亞里亞修道會，在阿瓦隆築巢的癌細胞。一旦，容許其存在，就很難殲滅了。這個事態已經超過主人這種冒険者個人的力量範疇」
「我們能做的，頂多、就是提供情報與提醒其威脅性、嗎」
「是的，在此之上的武力行動──」
「我懂，即使是我，也沒有那麼欠缺思考」

和阿瓦隆整個国家為敵，只能說是超越下策的愚策了。最糟，也被斯巴達盯上，或者阿瓦隆和斯巴達之間起衝突。
現在我能做的事情很少⋯總之，這事就先和威爾談談吧。

「不過，在這麼短的時間內完成這麼龐大的事情，那些傢伙，到底用了什麼魔法啊。總不可能只憑一張嘴，就做得到這種事吧」

最不可思議的，就是亞里亞修道會成為話題的，名門貴族的捐款。到底如何博得這麼龐大的金額援助。在那之前，是怎麼牽上線的。

「不是魔法，或許⋯是詛咒」
「詛咒、嗎？」

只要聽到詛咒，我就不能悶不做聲，還沒有形成這種演藝形象，至少現在我這麼認為。我在意的，是盡力避免曖昧說辭的沙利叶，特意的說「或許是詛咒」

「三年前，一名司祭被施與異端審判，處刑了」

異端審判的罪狀，歷來是叛神罪。然後，必定死刑。
問題是，怎樣的走向，變成被烙上這種大罪。

「十字教的聖書，附有一種詛咒，因為發表這這種研究結果，司祭被判決為叛神罪」
「那是，什麼詛咒？」

雖然沒有讀完，也大致看了一眼。打招呼與日常對話會用到的定式句，現在也好記得。
不過，不覺得有什麼異樣。頂多，就是真的和基督教很像，這種抄襲嫌疑而已。詛咒的氣息，完全沒有。

「最接近的效果是、魅惑」

沙利叶說了。十字教的聖書，對人類這個種族，是多麼方便的東西。
聖書裡說。人類這個種族，是多麼多麼優秀。
唯一絶對的創造神，白神。由他創造出來的，僅僅一個完成品的至高種族。其他人──就蔑稱為「魔族」，僅僅、是混有創世之前的混沌殘渣，不乾淨的邪惡存在。和人類絶不相容，不共戴天之敵。
聖書還繼續說。人類的神聖，魔族的邪惡。那邊是正義，那邊是邪惡。從遙遠的古代至今，聖書對人類，一直呢喃著這種甜言蜜語。

「──就這樣，人類被十字教的魅力籠絡了。一旦，承認的話，至今相信的事物都會變成謊言。承蒙啟迪。開目明事。所以，司祭們稱之為『覺醒』」
「不過，我倒沒有開眼啊？」
「司祭的研究，並沒有得到證明。我也、沒有見過，變成『覺醒』這種狀態的人」

原來如此，只要讀了聖書就會突然間變成「哦哦，這才是真正的教誨！神明大人萬歳！」這樣淚流滿面狂喜之人。嘛，有很高概率出現這種狀態的話，毫無辯解餘地的帶有「魅惑」詛咒的吧。

「聖書並不會劇烈的誘發『覺醒』，在字裡行間，下意識的，誘導至十字教的信仰，也有是一種催眠效果的說法」
「只要看看就會被催眠，這麼方便的東西會──」

有。毫無疑問，有這種東西。
第四試練，慾望玫瑰。那傢伙在洞窟裡鋪的，荊棘的紋樣。正是、僅僅依靠視覺，就具備誘發深度催眠的魔法效果。那個恐怖的淫夢世界，現在回想起來都會汗毛直立。

「聖書的詛咒、嗎⋯連你都知道了，直到最近才被檢舉出來，也真是奇怪」
「不，主張聖書帶有詛咒的人，在正規記錄裡超過千人。從古代至今，發表相同研究結果的人，在那個時代都有」
「喂，那麼⋯就是真傢伙了」
「聖書隱藏有某種魔法效果，是實事。聖書詛咒的處刑，全都是秘密進行的。我知道的記錄，如果不是使徒都無法過目，是秘密資料」

因為第七使徒沙利叶的背叛，十字軍的機密情報大泄露。嘛，一開始我就認為不是什麼靠譜的宗教，會撼動十字教正統性的實事，不管發現多少都只給我「果然啊」這一種感想。

「那麼，襲擊鎧甲的傢伙，也是應為聖書發狂的？」
「那還不能斷定。以十字教為源流的小規模宗教團體，從以前開始在斯巴達就確認了好幾個。即使沒有聖書，與亞里亞修道會接觸，而被誘導從事過激行為，如此推斷更為實際」

因為聖書的詛咒沒有速效性、嗎。為了普及信仰，為了更加穩固而緩慢浸透，這種效果會更好嗎。

「問題的重點，比起聖書，果然還是亞里亞修道會⋯」
「黒乃桑，差不多到時間了」

就在我快要陷在找不到解決方案的思考泥潭之中時，菲奧娜出聲叫我。
抬起頭，那裡站著黒色上衣與百皺裙的西裝型神學院制服，這種穿著的菲奧娜。

「啊啊，已經、這個時間了嗎」
「是的，再不出發的話，就趕不上畢業式了」

沒錯，其實今天，遠雷の月３１日，是我與菲奧娜的畢業式。
就在幾天前，終於湊夠畢業的必要學分，畢業考試也平安合格，冒険者科終於畢業了。神學院原本的畢業式是在春天，不過冒険者科只要集齊學分與完成考試隨時都能畢業。
所以說，要說是畢業式，也僅僅是頒發畢業證書這種事務性的手續而已⋯不過還是，讓我說成畢業式吧。
那麼，漫長而又短促，為了和異世界的學校告別吧，出發吧。

────────────────────

說道惡墮女主角的話，果然還是工口設計的衣服呢！越是相信女主角的傢伙，越會受到精神攻擊，NTR屬性的攻擊。大家都知道呢。