驱馬走了一会儿後，终於能看到熟悉的盧比法爾宅邸。
結果自从进入盧比法爾后就再也没见到魔物。
說不定魔物的被害比我想的还要少。當然，一路上到处都能看到变成一滩烂泥的田地，在大雨的影响下并不是完全无伤。不过还是避免了最糟糕的情况。

途中我询问了凱因大人到達盧比法爾宅邸之後的打算，說是在换一次馬後，就会立即动身返回雷恩福雷斯特。
虽然本想劝他至少休息一晚再走，但一想到凱因大人急于回去的心情，我實在难以开口。只能答应将宅邸裡最好的馬借给凱因大人。

我本以为安吉爾小姐会和凱因大人一起返回雷恩福雷斯特，但她却說自己希望留下来，所以之後会和我们一起待在盧比法爾。說起来，安吉爾小姐是站在之前想要火柴的那个好像很伟大的王国騎士旁边的人。难道說，是想把这裡制作的火柴带回王城。「才不会把優质的火柴交给那时城裡的那種傢伙呢！」虽然我也有这種想法，但是因为必须真诚地感謝她一路陪我们到这，所以如果安吉爾小姐想要的話，我还是打算交出最高品质的火柴。

對盧比法爾宅邸来說，我们这是突然的回乡，所以出迎的人數非常少，只有幾个人慌慌张张地出来迎接我们。
隨後就缓缓将頭压到地上，开始一如既往的那个仪式。

不要啊。今天凱因大人也在，千万不要这样。

我看向旁边，凱因大人稍微與我拉开了点距离。无论發生什麼事都不忘關注（フォロー）之心的那位神之關注大師──凱因大人，竟然会这样。

并不是我让他们这样做的！是他们擅自跪下的啊！我當然有阻止过！

「凱因大人，不是这样的！并不是我让他们这麼做的！」听到我拼命的解释，凱因大人暧昧地点了下頭。真的相信了吗，我非常不安。

我在伏地的人群中看到一个非常熟悉的秃頭，於是出声问道。

「塔古沙克先生，巴修大人在吗？」

「是！现在正在房子裡努力工作……明明莉尤大人回乡了，却无法出来迎接，非常抱歉，请您务必宽恕」

「不，別这麼說，宽恕什麼的………巴修大人的地位更高，根本轮不到我来宽恕」

真的，现在凱因大人就在旁边，不要說什麼奇怪的話。稍微看看气氛啊。
虽然我试着冷静地开导塔古沙克，可是他却感叹着「莉尤大人真是慈悲为怀」并露出怀念的塔古式微笑。

一看到那个表情，我就感到气不打一处来。我的慈悲只在對着塔古沙克氏的时候可没那麼深哦。

「诶，这不是塔古沙克吗！你额頭贴着泥幹什麼？」

听到修哥哥轻鬆隨意的搭話，塔古沙克稍微看来他一眼後一脸惊讶。

「还以为是谁，这不是修那个自大小鬼吗。你在这裡做什麼！？」

果然，修哥哥在其他村民眼中也是个自大小鬼吗………

「诶嘿，嘛，刚好呢。我正赶赴解救妹妹的危机！」

虽然修哥哥这麼說着竖起拇指，可我反而覺得是自己参與到哥哥的危机中了呢。没想到竟然会在人身介绍所遇到并且買下自己的亲哥哥。

总之，为了尽早确认状况，我急忙让佣人带我们进入宅邸。
光媽媽、安吉爾小姐和修哥哥不用說，凱因大人當然也在一起。因为凱因大人预定在與巴修先生碰个面後就立馬啟程。
我们被带到会客室後过了一会儿，就听到一陣急促的脚步声，隨後巴修先生神色匆忙地走进房間。

「莉尤君！啊啊，还有光樹，你们都回来了！帮大忙了！」

虽然巴修先生非常高兴地这說道，可神色却不太轻鬆，一脸疲勞。

因为进入盧比法爾后完全没有遇见魔物，所以还有点乐观，或许實际情况并没有我想的那麼好。

「巴修大人，領地的情况……！不，在那之前，这位是雷恩福雷斯特出身的凱因大人，一路護送我到这，可以为他安排一匹回程的馬吗？」

在询问領地的情况前，必须先送凱因大人回去。
溫柔的關注专家，凱因大人，如果知道了盧比法爾的窘境，或许会难以下定決心返回雷恩福雷斯特。
虽說如果真能留下来的話，我是很高兴……但是，盧比法爾情况很不妙。真的很糟糕，非常需要優秀的人，如果像这样充满期待地宣传类似的事，总覺得非常狡猾………不，真的，虽說我确實非常期待……但是严峻的情况无论哪个領地应该都一样。

「噢噢，是那样吗？真是辛苦了！如果方便的話，能否休息一晚再走？您看上去也非常疲勞」

虽然巴修先生和蔼可亲地这麼提案，但凱因大人仍然摇了摇頭。

「非常感謝。难得您这麼邀请我，但是即使快一刻也好，我想尽早回到領地，我必须守護我的家人」

「是吗。……真是遺憾，那就如此吧。一路送莉尤到这，非常感謝」

巴修先生，說着很有伯爵風范的慰勞話語，隨後命令佣人去準備食物與宅邸裡最好馬匹。
凱因大人對巴修先生的關心道謝後，很有騎士風格地请求告辞，隨後快步离开屋子，我为了送別凱因大人，也與他一同离开房間。

「凱因大人，真的非常感謝。总有一天一定会回报您」

「不用在意，没關係。因为这是我自己想做才做的，能顺利送莉尤回到家真是太好了。也能给艾倫一个好交代」

还是一如既往的溫柔貴公子。
回乡的路上，也有非常辛苦的事。在进入盧比法爾前，魔物實在是非常多，我总是想些最糟糕的情况。但是，每當那时，凱因大人都会露出一成不变的溫和笑容，让我能够安心。
明明凱因大人也非常担心自己家人和領地的情况………

「凱因大人，那个，请多保重」
「嗯，一路过来遇到的魔物都不怎麼多，而且一个人騎馬的話，就算万一遇到了魔物也总会有办法。最壊的情况只要逃走就好了」

就和凱因大人說的一样，应该没什麼问题。因为凱因大人很強，同时也不骄傲，當判断不行的时候，就会採取回避正面的迂回做法。至今为止的旅途中也偶爾见过幾次这種判断的優益之处。

「凱因大人，请带我向艾倫和艾琳夫人，以及雷恩福雷斯特的各位问好」

「交给我吧」

說完，凱因大人露出一如既往的让人安心的微笑，离开了宅邸。