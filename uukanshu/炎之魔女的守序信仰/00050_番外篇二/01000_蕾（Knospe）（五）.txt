“小贺……你觉得蛭涟她怎麼样？”

“诶？什麼怎麼样？”

就在一起放学回家的路上，不知为什麼芥檩突然说出了这句话。

“因为今天小贺和小涟她相处得很不错的样子，虽然最後弄哭了人家。”

“只是普通地在聊天而已啦……关於她为什麼哭我也不太清楚。”

之後回到教室之後我们也没有接着对话，但是蛭涟已经恢復了以往的那种冷淡的表情，当时是被故事感动了吗？应该没到那种程度吧，实在是搞不明白。

“小贺要温柔地对待小涟哦……因为……”

“诶？什麼。”

她的声音在途中变得越来越小，让我无法听清。

“不，没什麼~~”

“话别说一半啊，多让人在意！”

“有些话不说出来比较好哦……对於现在的小贺来说。”

“唔……”

那从一开始就别说啊。

“对了~~小贺想要女朋友吗？”

“诶！难道说小檩你要当我的女朋友吗？”

这句话啪的一下仿佛一条绳子拴住了我的心臟……

我急忙去试探一下她的反应。

“别开玩笑了~~我是认真的哦。”

芥檩鼓着脸颊看上去很生气的样子，嘛~~即使是这样她也是可爱到无法复加的地步……

不过现在我也是认真的……多麼想立刻就说出来，很可惜，我不会说出一定会被拒绝的话语。

“当然想要啦~~我可也是正值青春年少的高中生啊。”

“呼呼~~那就好了，一直看你和道华君黏在一起还以为……”

“停下，停下！！不用再说了也可以的！！”

我叹了一口气，绝对不想从喜欢的女孩嘴裡听到这种玩笑……跟道华，恶心死了，以後得注意一点，不能让小檩有奇怪的想法。

“话说，小贺……快到学园祭了呢。”

“嗯，也到这个时候了呢。”

10月份了啊……时间过得真快，转眼间又快到一年的末尾了。

离毕业也只剩一年了，即使再碰巧我也不可能和小檩她考进一所大学。

毕竟她可是稳坐年级前十呢。

虽然自己也尝试过努力之类的，不过理科之类的是完全过敏啊……

果然不适合的东西就是做不来。

“怎麼样~~学园祭哪天想和哪个女孩子一起去玩吗？升到高三的话可能就没有这个餘裕了吧。”

稍微歪了一下脑袋，小檩她难道是想和我一起逛学园祭吗？

不可能吧……之前的那种反应，明显是想说别人的事情了。

不过，怀抱着一丝希望，我还是想试一下。

“小檩那天有空吗？”

“没有，没有~~甜品部那天可是很忙的呢~~”

一副敷衍的态度随意带过了……

呜呜呜呜……

“我去帮你的忙吧~~”

“不……用……了，真是的，为什麼小贺一直要缠在我身边呢！不早点脱离姐姐去找个女朋友可不行哦。”

“你什麼时候成我姐姐了！”

可恶，为什麼防御这麼滴水不漏，完全找不到给我告白的空隙。

感觉累了的我不再吐槽，默默听着青梅竹马的话。

“小涟她那天也很有空……要一起去逛逛吗，小涟可是个很可爱的孩子哦~~顺便向她告白如何？黑长髮的可爱女朋友很快就能到手哦~~”

唔，明显冲着我的喜好说出这种怂恿般的话。

但是为什麼摆着一副诱拐犯的说辞啊，嘛~~~说到这种地步，我大概也明白是怎麼回事了。

小涟她是希望我向蛭涟她告白吧，虽然不知道她为什麼会有这样的想法，可以的话我就像想这样一直沉默下去……

但是如果不回应的话被当成默认就惨了。

“不行……”

没有多余的感情，我仅仅是这样简单地回答了。

“诶……为什麼？”

“没有为什麼，不行就是不行。”

“不理解啊……小涟她相当可爱呢，其实背地裏给她寄情书的男生很多哦，不过小涟都无视了。通过今天的事情我明白了，小涟能和贺由君开心地说话，虽然最後把人家弄哭了……但是之後回教室的时候一提到小贺的事情小涟她就通红着脸呢。”

“只是因为作为普通的女高中生在异性同学面前流泪事後感到害羞而已吧……并不是你想的那样，而且高一的时候我们都做过图书管理员，所以关於书的事情才能搭上几句话，仅此而已。”

不想再被迫回答这类问题了。

我加快了自己的脚步，将小檩远远地丢在後面。

“等等……啦！”

也不知道为何这麼心血来潮，出於对同社团女生的关心？希望找一个能照顾她的男生？小檩确实是有可能做这种事情的人——她实在是太过温柔了。

现在还是快点回家让这傢伙的头脑冷静点比较好。

没错，仅此而已……

………………

…………

我从未对蛭涟有过特别的想法，今後也不会有。

但不知道为什麼我周围的人却总是将我们两人联繫到一起。

道华也好，小檩也好。

甚至那天中午之後也有女生来问我是不是和蛭涟交往了。

我不想让自己陷入这种被动的环境中被传以奇怪的流言……

蛭涟是一个可爱的女生，即使不和别人怎麼交流，她依然无法掩盖自身散发出来的魅力与吸引着男生的费洛蒙……

道华说了，她很在意我。

同班的女生也自顾自地说着是不是在交往什麼的。

就连喜欢的青梅竹马都来问我要不要向对方告白……

搞不明白啊。

我何时立下了蛭涟的FLAG？那麼为什麼小檩那边却幾年都没有让我看到一丝希望？

够了……这些都不重要……

重要的是我绝不会就此顺势做出如此肤浅的事情。

在周围人的鼓动下就做出这种举动……难以理解。

因为对方很可爱，符合自己的喜好……又是受男生欢迎的美少女，所以就要大胆地做一次肉食系？

别逗我笑了，那单单只是错觉而已。

谁也不知道蛭涟自身的想法，仅仅是针对对方行为的一些部分私自揣测了，起哄想看热闹的随意心态罢了。

…………

“我回来了~~”

“欢迎回来……”

母亲一如既往地在客厅看着电视剧，老爹他大概要等到很晚才回来吧。

由於事业的关係他最近也是非常忙碌……

“呐，老妈，一会儿如果小檩过来就说我出门了，拜托了。”

说完我就急匆匆地冲上楼梯直奔自己的房间。

“怎麼了？你们吵架了吗？”

从後方传来的声音我并没有理睬。

…………

呼~~

丢下背包，我以一个大字型的姿态躺倒在床上……

解锁了4.7英寸的智能手机，我检查着自己的信箱。

“居然有一条邮件啊……”

【未读1封from－hirusazanami@tobioto】

正文：

哼，为什麼擅自丢下我就跑掉了。━━━[○＝｀Д′＝○]━━━

明天不会再喊你一起上学了！！(#ーДー)つ绝对不会~~~

要道歉的话就趁早带着「番街」的提拉米苏蛋糕一起哦。ε=(??｀ω′??)

…………

“不好意思……稍微想起有点事情，真的是非常在意，蛋糕店现在已经关门了。改天请你吃甜点自助餐如何？”

总而言之这样子回復了，超累啊，不过这样算是为了约会找的借口吧。

很快对面也传来了

“唔~~那就……这样算了吧Ｏ(≧ロ≦)Ｏ”这样的信息。

真是容易收买啊……

不过小檩还是一样，就连信息都可爱得一塌糊涂。

看完讯息之後我把手机扔在一边……

每个男生都希望自己有个可爱的女朋友，我当然也是一样。

但是，并不是只要可爱温柔谁都可以……

恋爱绝不是那麼简单的东西。

正因为是相处了十幾年的青梅竹马，所以我才喜欢小檩，这种积淀下来的情感毫无疑问是真的，并且从未改变，只有随着时间的推移而变得更加深厚而已。

必须无时无刻告诉告诫自己的内心。

我喜欢芥檩，所以一旦决定好必须全心全意，不能有任何怨言……这是恋爱最基本的礼节。

而且现在的时间已经不再宽裕。

是时候分个胜负了。

就在这个学园祭上……