
『走在大道上的二人』

　达鲁多斯帝国。
【译：原文是ダルドス帝国.英文：Darudos】

　位於鲁特西亚*的南部，面向着海。

【译：原文：レイテシア。英文：Lutecia】

　正是因为这样的环境，在特鲁西亚中水产业和造船业最发达的国家。

　离开联合国，我们抵达帝国。
　在港口购买各种物品之後停留了一宿。

　 现在在连接着港口和目的地的大道行走着。

　 之所以称为大道，正因为比去联合国时路过的乌鲁古斯之森*都道更完善。

【译：原文：ウルグスの森。发音大概是：wurugus】

　 幾乎没有云，倾泻如注的灿烂日光让人心情舒畅。
　 妖魔也不太会出没，除了由於海风的原因让头髮稍稍有些硬直的事以外，可以说是舒适的吧。

「帝国吗……嗯嗯。大概，是魔术优秀的国家吧？」

「说起优秀的話，这盛行着魔术的研究」

　 有新的魔术的发明，一般的情况是帝国做的。
　 笼统地说的話，现在形成了帝国发明新的魔术，王国和教国做改良·简化的这样的潮流。
　 向世人传播了的"心象魔术"的存在，大概也是帝国吧。

「因为帝国是相当的实力主义，所以，聚集了从他国的叫过来的优秀的魔术师。流泽苏那小子，也被帝国叫过，自傲地摆着架子吖」

「唔咕唔……是"大魔导"吗？确实是个恰如其分的魔术师，不过，难道看不出那是个卑鄙的小人吗？」

「确实是那样，不过，魔术的手腕就是另一回事了」

　 人性姑且不论，抛开坐落在帝国的魔术师不管，被称为"最强的魔术师"的实力是真的。
　 好象现在衰弱了，不过，三十年前是确实是人类最强一人不错。
　 是作为魔术师的典型的後卫类型，不过，如果前卫不在，是不能发挥实力的傢伙。

「哈嗯……他是怎样的魔术师？」

「说广域歼灭型就行了吧。即使是打倒那个欧鲁特吉亚的威力的魔术也释放得出来」

「哇唔哈嗯……什麼」

　 正因为如此，我想那个傢伙也能做到在没有我的情况下打倒欧鲁特吉亚的吧。

「嘛，那个小子的事无所谓了」

「哇唔哈嗯……嗯？」

「……你到底在吃什麼」

　 踩着舞步前进心情愉悦的艾尔菲的双手上。
　在消灭了在同盟国买下的全部温泉包子之後，现在握住海鲜串。

　 这傢伙，迷宫出来之後，经常吃着什麼。

「不，伊织。刚捕的海鲜串很好吃啊。你也试着吃吧」

「嗯……哦，确实好吃」

　 这个东西，使用过迷宫讨伐得到的报酬购买食物以外吗？
　 我在同盟国和帝国购买冒险用的工具等的期间，也一直是在吃着什麼。

「忘记了吗？　现在的我的体质容易感到肚子饿。经常维持分身的部分，平时要消费魔力。如果消去身体，某种程度上的食欲也能控制住」

　 东张西望地环视周围之後， 艾尔菲唐突地消去了分身。
　 浮在天空中浮游的是刚砍下来的人头和，那个两腋上的二只手臂。
　 根本是一只妖怪。

　 是射击遊戏的关底BOSS那样的样子。

「你看」

「看你个头，快变回去」

　 一边这样交流的，一边在大道上走着。

◆

　走着走着，天已经黑了。
　与在乌鲁古斯之森的时候一样，二人分担做露宿的準备。

　 正因为做过一次，艾尔菲手法也变好了。
　 因为预先购买露宿用的工具，比之前更舒适地度过了。

　 使用在同盟国买的锅，放入海鲜制作了汤。
　 因为優良的食材放在艾尔菲的头里，我没对食材的新鲜感到为难。

　 虽然吃了那麼多海鲜串，艾尔菲还是可以喝下了满满的一碗汤。
　 这傢伙吃着食物时，脸色一直没有别松懈过。

「这个汤！伊织！你可以称为厨师了！」

「所以我早就说过」

　 好象她挺喜欢这些食物的。

「呼呼……」

　结束吃饭，休息一下。

　 听着虫鸣声，“沙沙”的树木摇曳声音。

「啊，伊织。你，来到这个世界之前是做什麼的？」

　 一动不动地凝视篝火的艾尔菲，唐突地问那样的事。

「怎麼说呢，你很不平衡。既有成熟的部分，又有幼稚的部分。如此的技术和实力，一般应该是无法达到这境地」

「…………」

　 这个连自己也有自觉。
　 没有用眼睛去观察，相信着朋友，沉浸在甜美的理想裡。
　 不是肉体，而是精神厲害地不成熟。

「……是学生」

「鞋森？」

「在学校……在学园裡度过」

　 来这裡之前，是十六岁，高中生。
　 总觉得像从众一般地过着每天。

【译：我觉的这种译法跟文艺，这是原文：周囲に流されて、何となくで過ごす日々。】

　 父母因事故离去……除了这一点，应该是个意志脆弱的普通的学生。

「没有战鬥吗？」

「不是完全没有，不过，至少我的国家是和平的。魔族啦，魔术啦都没有，与争鬥无缘」

　 像被谁厲害地背叛之类的经验，也没有经歷过。
　 如果从这边人类的世界来看的，我脑子裡的是花园。

「伊织住的国家，是好地方」

「……啊」

「不打算返回原来的世界吗？」

「如果全部结束，我想找回去的方法。但是……」

「……但是？」

「不明白那边和这边时间比起来会变成怎样」

　 实际是不同的，我被异世界召唤到三十年之後。
　 如果在那边也有那麼多时间流逝着，就不会有我的容身之所。
　 我的外表幾乎没有变化。

「…………」

　 沉默。
　 篝火“劈里啪啦”地發出声音。
　气氛稍微变得沉重了。

「呼」

「……艾尔菲？」

吐了口气，艾尔菲慢慢地起来了。

「这麼说来，你吸收迷宫核恢復到某种程度的力量。我来确认到什麼程度」

「……好唐突」

「正好有助于消化对吧？」

　 打破沉重的气氛，艾尔菲“嗖嗖”转动手臂。
　 嘛，是个好机会。
　 魔力恢復之後，没好好地试验过力量。

「明白了」

　 从篝火离开。
夜晚，不过多亏月光，不是很暗。

　 互相保持距离，面对面。

「呼……带着杀气过来就行」

　 一边释放魔力，艾尔菲一边浮起了大胆的笑容。
　 取回双臂，魔力增加了。
　 如字面那样，好象有相当的自信。

　 由於翡翠之太刀的效果身体素质上升了。
　 用"身体强化"， "加速"等使身体素质又提高。
　由於魔力量变多了，即使不使用魔石也变得能使用一定程度的魔术。

　 虽然是那麼说，如果使用劣化版的"魔技篡夺(spell·deprive) "、"魔毁封杀(seal·ataraxia)"，会花费大部分的魔力，使用"魔击反射(impact·mirror) "又会魔力不足。
　 至今为止力量不足这一点还是没有变化。

　 虽说对手是失去了力量，但是是原魔王。
　 我用魔术的效果增强到身体承受的极限，架起了翡翠之太刀。

「这是不对等的。我不使用手臂」

　 为显示这一点，艾尔菲抬起双臂，笑道。
　 抱住手臂，作为替代晃动着脚。
　 是想用踢技吧。

「————」

　 我用握住刀柄的手来辅助，架起了隐藏着太刀的姿势。
　就那样把腰放低，然後不动。

「…………」

　 看到不动的我的艾尔菲浮起了诧异的表情。
　然後闹别扭一般的嘟哝，

「那麼，我先来」

　 抱住手臂，气势很好地向地面一踢。
　 因为那个威力，脚下的地面陷下去，草木摇曳。
　 艾尔菲用子弹一般的速度深入，
　 如所说一般不打算使用手臂，看来似乎打算用足技攻击。
　 不仅仅是手臂，还有也不使用魔术，是因为要观察情况吧。

　 一瞬缩短间隔的远跳。
　 在艾尔菲到这边的间隔缩短到跟前时，我动手了。

「————!!」

　 居合。
　 在这边的世界也被称呼为待剑，反击的剑术。
　 在进入到适当的间隔的瞬间砍先动的对方的技能。

　 放弃威力，侧重在速度上。
　 尽可能的向全速逼近达到最高速的一闪。

　 那个一击，艾尔菲是不容易——

「啊」

「嘶」

　 不能躲闪，就接受了。
　 刃通过艾尔菲的颈部。
　 就那样，艾尔菲的头“咚”地飞到森林的什麼地方去了。
　 丢失头的躯体，在“哗喀拉”地向地面倒下。

「……………嘶？」
【译：本翻译君也傻了】

月光照射中，映出视野裡失去头的艾尔菲。
　 也不做抽搐地动作，没有声音。

　 我想该怎麼做那时。

「……啊」

　 艾尔菲的头回来了。

【译：伊织使用一叶斩，但对幽灵系的艾尔菲没有效果o((>ω< ))o】

　 与刚泪洒而别的身体完全黏在一起之後，摇摇晃晃地站起来。
　 那个脸流着像珍珠一样大小的汗。

　 粗暴喘着气，艾尔菲盯视着我，

「你想杀了我吗！」

「是你说要怀着杀气来的！？」

　 她相当好象焦急。
　 为按住心跳把手贴到胸上，并擦去汗。
　 哦，现在不是没有心臟吗？

「嘶，我差点以为我死了吗」

「……是那样吗？」

「从今天的一闪来看的話，不是恢復相当的力量吗？」

「……不，根本没有」

　 如果不牺牲威力，就不能释放出那个速度。
　 如果是人的話能杀，不过，要是妖魔和魔族那样是不可能的。
　 在那个速度下，攻击被炎龙和土魔将之类弹开就结束吧。

「……从前就开始想，伊织的剑的实力是相当的厲害啊」

「嘛，多少是那样」

　 ……我的朋友教过我一段时间。

「……没什麼这麼累啊，睡去了」

　 然後艾尔菲停止了战鬥，返回了篝火边。
　 虽说是疏忽大意，好像对被我打落了头这一事受到打击。
　 那个傢伙要是认真……先不提用魔眼让我无法挨近，原本用魔力就弹开攻击。

　 擦去汗之後，我也决定睡觉了。
　 熄灭篝火的話，像谜一般，不知为什麼艾尔菲靠得相当近了。
　 虽有疑问，但决定还是睡觉。

◆

　 次日。
　 与艾尔菲并列行走在大道上。
　 随着前进，风景也变了，从远方可以看见了草原。

「……好怀念啊」

「来过吗？」

「啊啊。三十年前，来攻略死沼迷宫的时候」

　 总觉得似曾相识。

　 在旅行的过程中，帮助了被魔物袭击的人们。
　 作为谢礼被款待着，并和他们跳舞来放鬆。
　 关系好的人多少也是有的。
　 那个村的人们现在在做什麼呢？

　 一边沉浸在那样的怀念的心情，一边走着大道的时候。

「……这是」

　 从前方，听见了马嘶。
　 不仅仅是那个，还有小爆炸声音接连着。
　 我与艾尔菲互看脸，慢慢地向听见声音的方向靠近。

「……魔物吗」

　 能看见前方马车被魔物袭击了。
　 马的御者，乘者使用着魔术和魔物战鬥。
　 袭击马车的，是叫"泥熊(mud·bruin)*"的魔物。

【译：原文：泥熊(マッド・グリズリー)。最後的发音很奇怪不过我想我没翻错。这个日文发音是：Maddo gurizurī.。。。这跟英文也差太多了。】

　 是可以从口使用土系的魔术追逼猎物，强大的魔物。
　 有六只同时袭击着马车。

「……奇怪」

　 泥熊基本是单独行动。
　如果那钟程度的数量聚集在一起的話，开始同类相残也不奇怪。

　 然而，泥熊们异样地统一着。
　 它们的呼吸很沉重，不过，也没有咆哮，联合着攻击。
　 简直象，在那裡被什麼操纵一样的不自然。

　 我正对这感到不安，眯起眼时。

　 从马车涌起了哀鸣。

　 一只也很麻烦的魔物，现在有六只。
　 袖手旁观的話，虽说同乘马车的魔术师好象实力出众，不过不会坚持很长时间。

　 该怎麼做呢？

「…………」

这时，袭击马车的泥熊的一只转头朝向这边。

「好象被注意到了」

　 六只裡面，二只停止向马车的攻击，猛然开始冲向我们。
　 因为叫熊，所以它们的移动速度是相当厲害。

「切，只有战鬥吗」

「正好。也可以确认取回的"手臂"的状态」

　 随着时间一点点流逝，和袭击的妖魔的战鬥开始了。

=======================================

作者原話：
修正前話的流泽苏的设定的 11/7

