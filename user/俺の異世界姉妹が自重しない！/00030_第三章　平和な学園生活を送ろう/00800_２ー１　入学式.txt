

由学园地基建造的雄伟建筑物。以打破这个世界常识（建成）的顶层 — 多用途礼堂那裡，聚集着四百几十名即将成为穆勒学园学生的人在。
只是，因首年从外部招募学生的缘故，突然集聚了这个量真是始料未及，导致铁管椅子来不及準备。
毕竟以往是从，一期生十七名，二期生的三十名。然後从六十，一百等阶段性地增长的，（準备不足都是一下子）突然超过了四百五十的关係呐。
因此，虽然挺对不起大家的，就先让他们随便地站着了。
顺带一提，五期生的新生们幾乎全体成为了本科生。这是因为，（相比起来）平民的识字率才只有约一成。
虽然本来预定会有约半数的学生会成为学习基本知识的预科生呐。
但各地的领主为了让孩子能尽早毕业并发展自身管辖的领地，导出将优秀孩子送来学校的结论，因此基本没有学生成为预科生。
正因如此，与作为奴隷被卖的一期生和，和有各种各样误会的二期生相比，（本届的）全员从最初就表现出充满幹劲的样子。
「不觉得这很厲害么？」
在人声嘈杂的场内，慢慢的从旁边传来了声音。
回头一看，有位没印象见过的少女往我这边看来。是比我稍微年少一点吗？ 鲜红的双眼配上绯红的头髮。稍微带有异国风情氛围的女孩子。
「呐，不觉得厲害么？」
「确实很壮观呐。我也没想到能聚集到这麼多人」（ 凄い 除了厲害也有壮观的意思，这裡是男主听错了）
注意到这是跟我搭话而回覆过去。接着少女眯起眼。不知怎的格格地笑了起来。
「不是哇。咱所指的是，以这栋建筑物为首建造了整条街的技术呦」
「哎？……啊，是，是吗」
也是呐。一般都是会先对这条街的技术感到吃惊呐。失败了。
倒不如说，这下糟了。如果在这裡暴露了我是贵族，更不用说是利昂的话，会连累到爱丽丝，将梦想中普通的学园生活葬送掉。
无论如何也要蒙混过去。
「小哥，原来是――」
仿若将我看透的锐利目光差点让我喘不过气。
但是
「在入学前已经走过这条街吧？我明白（你对这条街技术的感叹）哇」（利）（这段利昂为了掩饰也说起方言来）
「……哎？」
「咱也被这条街的魅力所吸引，二周之前已经在穆勒这逗留着哟。拜这所赐，已经差不多适应这条街的厲害之处了哟？」（利）
「……啊，是么，原来是那样啊！」
虽然不知道为啥简单的就误解了，但我想着“真走运”而继续搭话的瞬间，那女孩眯细了眼。
「原来如此呐。虽然不知道为什麼要隐藏着身份，（小哥）是贵族大人吧」
「呼啊！? 什，什什什，你指什麼呢！?」
「小哥，反应太老实了呦。咱故意露出误解的样子时你高兴过头了哦」
呜咕。正想着已经顺我所图地误导了她，没想到那本身（整个反应）竟然是圈套？这样的话我已无计可施，不禁轻叹了起来。
「（我）就那麼好懂吗？」
「嘛小哥露出的氛围算是比较易懂的一方吧。就好比，那边的那个孩子，一看就明白那散发的氛围吧？」
找找看是指谁来着，看到摇曳着一头柔顺鲜艳金髮的女孩子在那伫立着。
艳丽的头发配上宛如珍珠一样的肌肤，举止间散发的高雅感，一看就知道是非池中物……是说，那不是索菲亚吗？
明明今天是入学式，为什麼索菲亚会在这？
……难道说，是为了装作今年入学吗？
虽说残留组的本科生都不在所以没问题，但到时从预科生升上来不就会暴露了吗……嘛，是想在那之前多交些朋友吗？
「之後是那边的女孩子呐。那啵哇啵哇的感觉，你觉得怎样？」
虽然想着又是谁呢，但这次是不认识的女生。
微微天蓝色的银髮，大大的紫色双眸。确实像是受过良好教育的女生。而且与索菲亚一样年纪轻轻胸部却发育良好，加上文静的外表让形象更配合。
散发着不下於索菲亚的氛围。虽然是我不认识的女生，但是，是哪裡的大小姐这点是不用致疑的。
「看到就能明白这点我是理解了。所以，你在打什麼算盘？」
「不用那麼担心呦。并没有对外宣扬的打算」
「是那样吗？」
「咱是茜。《库拉乌特》商会当家女兒的茜。请多关照呐，小哥」
「哎？啊……那个，我是利约。这边才是多多关照呐」
突然就将话题跳跃呐。要是不多加注意对话的流向就要被对方掌握了。而且，在说过不会对外宣扬之後就“请多关照”什麼的……是那个意思吗？
「讨厌哇，就说不用那样警戒就好了嘛。对咱来说，只要能普通地友好相处就足够了呦」
「是，那样吗？」
「那看起来很意外的脸真过份呐。咱看起来就那麼的像坏人吗？」
「哎？唔唔，也是呐……」
我重新观察了茜。
年龄稍微比我少一点吧？然後说不定这才是符合年龄的平板身材。有着异国风情的味道，以及很适合穿和服的形象。还有，胸部是平板哒。
「……什麼吖，咱从视线中感到满满的恶意呦」
「是错觉吧」
嘛虽然外观与年龄相应，但咀中说出的却是独特的地方口音。
顺带一提这口音，实际上并不是关西方言混杂着京都方言的口音。但说到底，在我脑内转换成日本语后差不多就是这样的感觉啦。
「小哥，打算观察到什麼时候？咱看上去真的那麼可疑？被你这样一看，稍微受到打击了哇」
「啊，抱歉抱歉。也不是见到什麼可疑的地方呐。只是因为在我周边没怎麼见过这类型（的人）而感到有点吃惊」
「呼嗯？是那样呢。那样的话，我想友好相处（对双方）都没损失呦？」
「果然是利益得失的问题呐」
「啊…请先不要误解。虽然咱的商人魂会将利益得失作为首要考虑，但不只是那样呦。再说，如果只因得失而行动，即使受人信用也不会受人信赖呢」
「……也就是说？」
「帮你保密，与咱所说的友好相处两者之间并无关连呢」
「原来如此……」
感觉不是坏孩子吧…大概。而且要是有这麼一个与众不同的女孩作为朋友，校园生活也会变得有趣起来吧。
而且，即使知道我的身份，仍会帮我保密的朋友要是能增加的话就正合我意。（对我来说）选哪边都不是坏事。
「那麼茜，往後的一年多多关照呐」
「这边才是吖。小哥」
就这样，於入学式开始之前和茜闲谈着。在那过程中，也稍微听了一点茜家裡的情况。
据了解所知茜的老家――《库拉乌特》商会好像就是所谓的贸易公司。
以马车将全国的特产往来运输。再在其他的街道上贩卖，但因为最近以贵族为销售对象的高级商品滞销而困扰着。
稍为扯开话题，贵族和有钱人等，会买入最高级商品作为彰显权力和财力状况的手段。
也就是说，顶级的有钱人和贵族们现今追求的是穆勒的商品。与至今为止所收购过类似的商品已不屑他们一顾。
因此如果以贵族为对象交易的话，除了穆勒的商品以外别无他法，但因为穆勒之街商品生产量少的关係，与古兰歇斯家没有连系的商人是没办法入手的。
――也就是说，是那回事呐。
……总觉得，很抱歉。
要是开口道歉的话会（将身份）是利昂伯爵也暴露出来，所以只能在心中道歉……想来也对呐。（因为自身追求幸福的行动）而以这样的形式，令他人变得不幸（的情况）也是有的呐。
不再稍微考虑周围的状况来推行政策是不行的呐。
「……这，那个。你家也真不容易啊」
「确实是挺辛苦的。但赶不上时代的趋势是做不了交易的呢。而且这并不是无妄之灾，反而是一大机会啊，咱可是为了寻找买卖的灵感以及拓展人脉关係才来到这个学园的呦」
「――茜！」
「呜哇，被吓到了。忽然發出这麼大的声音是怎样了？」
「被茜的思维感动了。今後也多多关照呐！」
「呜，嗯？ 多关照，呐？」
没能明白的茜歪头感到疑问。
嘛就是那个啦。基本上努力的人我都喜欢呐――是说，并不是因为对方是女孩子才这样说的意思哦。对克雷因先生提供过剩的技术支援也是因为这原因呐。
因此，并没有打算对茜提供超出必要的帮助。单纯只是作为同班的朋友，要是有什麼机会才会略点一二――就这种程度啦。
至少现在是这样…呐。
「话说回来小哥。在隐瞒身分就是说，保持这种语气对话可以么？过後，不会以无礼等理由向我问罪的吧？」
「不会那样做的吧。不如说，知道我身份之後仍能普通相处的话我会很高兴的呐」
「那个……这是在宣言着总有一天会公布身份吗？」
「唔。嘛，可能要到毕业才分晓呐」
如果能保持友好关係的话自不用说，而且也有作为商人之间结缘的这层意义在，反正总有一天都是不得不说的呐。
…什麼的想着这些时，茜把手指贴到脸颊上哼姆哼姆地点点头。
「总觉得~能读懂小哥你的想法了呦。与其说是不得不隐瞒身分的立场，不如说作为普通人在这学园上学才是目的呢」
「……哎，竟然了解到那种程度了呐」
「别看咱这样好歹也是商人的女兒呢。单以观察力而言可是有点自信的呦」
我从茜那因自豪而挺起的平板移开了目光。
虽然是在谈（家裡）辛苦的话题，但她却以父亲的工作为傲吗。我因为前世以及今世父亲都很早去世的关係，稍微有点羡慕呐。
……嘛，我的那份（寂寞），有米莉和大家陪伴着我（来消解）呐。
「但是，要是打算上学的话，我觉得隐瞒身分这做法可是正确的呦」
「那原因是贵族被敬而远之吗？在这所学园，应该是规定了不论身份人人平等的吧」
「咱听说好像有任意为之而不单止被退学，还被遂出家门的贵族的传言呦。也就是说，一定会有某些必须遵守的条件来约束贵族对吧？」
「原来如此，确实是啊……」
像帕特里克那样做过头的话会被退学。
换句话说，有做过头（而被退学）的贵族就意味着，只要不做过头就不能将其退学的意思。
然後，被想成是这样的贵族也没办法。
「我本身并没有想过自己有多伟大呐」
「看似是那样。但对初次见面的对象仍未能準确判断呢」
「那也对啦」
即使是现在的我也，当听到有贵族想来上学时，不禁会想那贵族会否像帕特里克那货一样摆显呢。
某程度上来说被用有色眼镜来看待也是没办法的呐。
「而且啊，咱想贵族被敬而远之的理由还有一个呦」
「另一个理由？」
「大部份这裡的学生们，连同我也，都是受到父母和领主大人的期待而来到这裡的呦。看一看？大家，都有着一副身负重任的表情吧？」
啊…说起来――我再次将视线转向周边。
虽然索菲亚和刚才那啵吔啵吔的大小姐都有着沉稳的氛围，但其他的学生们身边则缠绕着一股紧张的气氛。
背负着领主和父母期待的数百名学生们。
他们身负来年毕业，将学到的技术带回故乡的期待而身在此处。因此不想跟感觉很麻烦的贵族有任何牵扯吧。
虽然说出来很不好，但我跟爱丽丝在这裡是以游玩为目的呐。
虽想过跟大家有笑有闹地度过这一年，但可能要稍微重新考虑呐。
「……哎，奇怪？即使有这原因，茜还是向我搭话了吧？」
「那是小哥你，与咱所讨厌的贵族是不同类型呢。而且咱的目的，是寻找买卖的灵感，和拓展人脉关係呢」
「原来如此……」
也就是说茜的话，就能成为爱丽丝和索菲亚朋友的意思吗？而且也不像是一肚坏水的孩子，然後事实上感觉是挺可靠的……就看究竟被她计算到什麼程度了呐。
如果（从最初就）计算到将自己介绍到我的伙伴――爱丽丝和索菲亚那裡的话，那就真的不能（对她）疏於防範呐。
说笑的，即使城府再深也没必要畏惧呐。倒不如说，（真能计算到那程度的话）请务必在买卖的意义上加深两家之间的交往呐。
反正接下来有一年时间，就让我慢慢地判断吧――正当我想着那些事时，克蕾雅姐已登上台阶。向新生们送往欢迎的问候。
=======================译者的话====================================================
今次登场的是听着萌萌的？译出来却各种吐血的方言妹子
她的家族 クラウド商会（Cloud商会）机翻出来是云商会
因为有点怪怪的，故先音译作《库拉乌特》商会
要是往後突然哪一章展述商会由来的话，也有修改的可能性，请留意！
