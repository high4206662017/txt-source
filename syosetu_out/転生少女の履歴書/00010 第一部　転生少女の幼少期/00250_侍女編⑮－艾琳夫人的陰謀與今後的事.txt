我被克勞德先生召唤，来到会客厅，不过，在房間裡艾琳夫人和克勞德先生带着紧张的气氛等待着我。

两人带着沉重的脸色。

「十分抱歉。我好象迟到了……？」

这是因为我刚用完晚餐就被叫来了，食物被急急忙忙的塞进胃裡，紧急的以竞走的方式赶来了，不过，还是晚了吗？

房間裡的气氛很可怕。

「哦，早，莉尤。请到这裡来。」

到刚才为止，带着稍微發怒的脸，不过，看到我以後，变回了平时溫柔的克勞德先生的脸，并催促我在旁边的座位坐下。

那我就恭敬不如遵命，在旁边坐下了。

「莉尤，最近时常可以听见，有个男人在寻找莉尤，你有什麼頭緒吗？」

啊啊，是關於那个話题吗？

据說最近街上有个神秘的男人，在打听各種各样關於我的事。

而且还用麻袋把脸罩了起来，问他名字也不回答。

打算抓住他的話，就拂哇地逃跑。

「恩，我想不是我认识的人。對这種隐藏来历的人没有線索」

我小声的，向着地下，像那样回答克勞德先生

「說不定是其他領土的貴族或是王族，瞄准着莉尤吗」

骗人，开玩笑的吧。

不要开这種玩笑！

很可怕哒！

寻找着我的神謎男人难道，是知道了最近我开发着各種道具的事，为了确认我的传闻而从其他領地派来的密探。

「嘛，哥哥大人，其他貴族的話还能明白。不过，王族应该不会那样悄悄地派来密探吧？」

「不對，艾琳說不定不明白。不过…王族是很可怕的。最壊的情况，莉尤說不定会被抹杀……」

喂，被抹杀

………我處於那麼危險的状态吗。

不记得

拿出过什麼危險吓人的道具，不过

……不太确定。

「討厌，哥哥大人說得那麼夸张。那样的事不太可能发生不是吗？但是，其他的貴族到最近开始售賣这些工具的我们这裡来查探倒不是不可能的事。」

「工具的提案者是莉尤应该可以掩蓋，不过…可是，即使那样……」

那样說着，坐在我旁边的克勞德先生搂着我的肩膀。

艾琳夫人发射出尖锐的視線。

「莉尤是我的東西，不会让给任何人！」

克勞德先生罕见的，以认真的表情转向艾琳夫人。

直到刚才一瞬間终於变柔和的房間气氛，又开始紧张了。

「不！莉尤是我的東西！之前不是說好了買给我的吗？」

艾琳夫人，认真地反驳克勞德先生。

好像，關於商品（我）的買賣交易，从我到房間来之前，两人就已经在争執了。

真是的，不要隨意決定我的未来啦。

年收入，福利，工作内容，工作地，工作环境

……全部都不问我！

我好像都没有什麼決定权了。

嗯，艾琳夫人和克勞德先生，哪一个比较好吗？

意思是，改变工作内容吗？

即使不管是跟谁在一起吗？

「不行，不賣！莉尤得放置在我身边。買她时也花了我１枚金币！」克勞德先生若无其事的给買價掺水。

明明是銀币３枚。

这个人真厲害。

「如果是那样的話我出金币２枚！」

「不行，不賣！已经決定这孩子得做我的女僕！」

「那麼要是白金币怎麼样？」

「白金币！？　呃，不行！无论出多少也不賣！」

克勞德先生稍微有点动摇。

「怎麼这样，哥哥大人！不是約定好我喜欢的話就賣给我的吗？」

「的确是那样，不过，因为我出乎意料的喜欢她，所以決定作为我的女僕留着。因此，如果卡丁先生回来了，莉尤就会跟隨我会商会！」

诶？　现在是在說什麼？

「那位卡丁先生，是艾琳夫人的丈夫吗？要回来了吗？」

不知不覺對两人的拍賣競争插了嘴。

「嗯嗯，就是那样，莉尤。虽然很遺憾但是父亲大人好象就那样待在王都了，不过，老公在王都的工作稍微告一段落，所以就回来了」

艾琳夫人稍微有点高兴地說着。

凱因少爺和艾倫的爸爸要回来了呢。

呐─，是好事啊。

但是，克勞德先生会在这裡，就是有着这裡的主人不在的理由。

原来如此，所以就必须返回商会了。

据說，克勞德先生的商会总部，并不是在那麼远的地方，不过，尽管如此如果不使用馬車，就不能轻鬆的往返。

如果，我成为克勞德先生的女僕，必须离开这裡吗……

與这裡的人相处得很好，工作也习惯了，如果可以的話不想改变工作崗位─。

在艾琳夫人这裡继续工作，好吗─。

好，支援艾琳夫人！

艾琳加油！

「艾琳，想怎麼在身边安置莉尤？即使是艾倫，最近脾气也比较好了，现在没有特別需要莉尤的工作吧？而且，现在有可疑的東西也在寻找莉尤，让她呆在我的商会比较安全」

「那样可能…不过我确實有需要莉尤做的事」

这，馬上艾琳夫人就变得扭扭捏捏的。

恩，是什麼。

除了现在做着的工作以为还有什麼……

新工作的起步吗？

黑心高風險的企业经常人不足呢。

「什麼？如果是那样在結束之後可以跟隨我会到商会？」

「但是这个，是不能现在馬上完成的！不拜托數年不能完成的事！」

「數年后……？具体地說到底要做什麼？」

於是艾琳夫人一瞬間與我對上眼，像是要說什麼，稍微露出烦恼的表情後，清楚地說。

「希望给兒子们施教性教育！」

呃？

现在在說什麼？

說了性教育？

…所谓的那个事？

性教育保健体育的事？

哦，說不定我听错了。

不会是那样的東西。

可是，在旁边的克勞德先生开始惊慌。

看这个着急样子，好像性教育的事是真的。

「什，你在說什麼，艾琳！那样的事是年長者…其他什麼人的任务吧！？」

是的！就是那样！克勞德先生！

那样的東西，考虑到还是有经验的人的教导比较重要！

「因为，我的时候，對方是那个克塔老先生！？虽然與男人不同女人的性教育不是真正的做（*你明白，做什麼）。不过，第一次被看见的，碰的是克塔老先生的東西！？我到现在都会发惡夢！因此我不想让兒子这麼想！艾倫和凱因都喜欢莉尤，不是挺好！」

「什，什，什，什，說什麼，艾琳！一点也不好哇！首先，莉尤的話，太年轻！经验不足吧？作为那種事的對象不适合！应该请有经验的人！」

就像說的那样！克勞德先生！我前世也，没有经验！所以不行不行！（*原文是ピュアッピュア，这 Piyua 是什麼實在不懂就改成上文那样了）

我求职的第一希望，由艾琳派变成了克勞德派。

「那样做的話，不就像哥哥大人那时候的瑪麗！」

呃，瑪麗，那个洗涤妇的

………气色不错体格好，相當年長的那位瑪麗小姐？

啊，是这样啊，克勞德先生重要的第一次的帮助。

………啊，危險（牙白），稍微想象过頭了。

「另外，瑪麗不是可以吗？」

「不行！哥哥大人到这样的年龄仍是單身的事，也是因为最初的女性是年龄相當大的瑪麗！因此一定是，变得不擅長应付，對女性提不起兴趣了！最好最初的對方是年轻的，作为關系好的孩子就決定了！不要成为討厌的回忆！」

「够了！我不是不会對其他女性有兴趣！而且，瑪麗，其實不壊。她非常…和善」

說完，脸颊稍微染紅的克勞德先生。

拜托了，別把感想說出来。

在腦内，瑪麗小姐如何溫柔的又被想起来了，所以请停止。

『熟女瑪麗的扫除指导～我的初次的穴的扫除～』

啊，危險。

連标题都出来了。

把頭腦切换啊，我！

心頭灭絶後色情（エロ）的熟女又来了！

啊，不行，完全切换不了。

「那，那年轻漂亮的佣人的話，屋子裡不是还有很多吗？譬如史黛拉不是好吗？」

咕！停止啊克勞德先生！

史黛拉小姐吗…！

不要再增加登場人物了！

「不行，她有洁癖。而且，除了莉尤和瑪麗以外的佣人，不是貴族，就是貴族的亲属。这样的事情可不能拜托。」

被那样說着的艾琳夫人怒目而視，克勞德先生，夸大的垂下肩膀，哈～叹了口气。

呃，那样就放弃了，討厌，克勞德先生！

更加地努力！

这样想时，今天第２次的认真的表情。（*指克勞德）　艾琳夫人稍微退缩了。

「如果是为了那样，请乾脆从街上带来专业的！不管怎样，莉尤放置在我的身边，那个建议不接受。莉尤你也没關係吗？」

「是！拜托您了！」

我，抖落全部的邪念，以来到这房間以来最好的回答對克勞德先生回答了。

艾琳夫人噗的可愛地鼓起脸颊。好象终於死心了。

啊，危險。

６歳就感到了将来的贞操危机。

克勞德先生Good Job！

在这之後，我，克勞德先生还有稍微不高兴的艾琳夫人一起討论今後關於我流向的问题。

虽說要去克勞德先生的商会，但也是在卡丁先生回来以後，还有点太早了。

然後，令人吃惊的是，克勞德先生打算将我作为养女迎接，预订１０歳让我入学王立学校。

在这个国家能上学是超高級待遇。

說的也是，在这个国家的教育机關，只有唯一王都王立的貴族聚集的这所学校。

以前，領土的孩子们要是文字和算術，只要有地方教就行了，對此克勞德先生說，有關教育，王攥住缰绳，不可隨意地制作像学校之类的東西。

为此，被派遣到这个房子的家庭教師也都是王立机關的介绍。

入学可以說是，从农村出身的我从不可能的事。

那麼說克勞德先生對我有着这麼高的评價，我是覺得很高兴，不过，养女就…

成为克勞德先生的养女呢。

因为王立的学校不是貴族子女的話不能入学。

克勞德先生好像是相當下級的貴族，不过拥有着商爵这个爵位。

所以理由也明白，理解着需要成为养女的事。

但是，养女，那是成为克勞德先生的家人，那个比什麼都使我的心混乱。

老實說，不擅長和家人相处。

到现在为止的经验，相當的了解了。

只是，观察克勞德先生的情况的話，清楚明白并不是希望與我成为家人的事，是类似契約那样的東西，關於养女的話也明白了。

在那时候，絶對不以父亲来称呼！

只在那点很強地主张了。

克勞德先生，也說了根本没有像这样称呼的打算，像以前一样不介意。

在某些地方稍微想了一下，成为继女也还早呢。

学校入学之前收为养女，学校毕业後呢，到克勞德先生的商会帮忙吗？

克勞德先生想让我做什麼不太明白，不过，那样不是什麼不好的人生。

只是，像这样隨波逐流生活着的我，有点對不起这个国家的学校，那样必要的教育場所，有需要的人存在。