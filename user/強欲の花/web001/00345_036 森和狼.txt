


探索开始三十分钟左右，我发现了迷宫的楼梯。


“似乎有什麼东西？不明白但小心是再好不过的了”


我踏入楼梯发动【侦查】技能。


迷宫阶层之间移动的楼梯好像是某种魔法，挂在魔物们所居住的阶层，但“侦察”和“劣化侦察”会被弹开所以不是很清楚。


侦察检查的结果是数个妖岩相同程度的魔力反应徘徊着以外，充其量就是和刚才的奥库同等级的魔物。


我判断大丈夫所以就完全从楼梯上下来进入迷宫。接下来映入我眼睛的是郁郁葱葱的森林。


“哎!？”


不由得叫出了声。洞窟裡有悬崖！有山谷！接下来是森林？哎！？


楼梯连着树洞。因为是异空间所以就能什麼都有啊。


也不知为何会有太阳出来（翻不出到底有没有太阳啊）。有点昏暗但对於拥有【夜目】能力的我来说并不受影响。


我向四周确认着情况，侦察中发现奥库（原文是橡树）同等级的魔物接近。


我架起奥加剑，从树木间缝隙出现的是一只狼。使用【鉴定】


水沃尔夫lv 60


【生命力】400 / 400
【魔力】800 / 800


◆技能
咆哮lv [ 4 ]
夜裡看lv [ 3 ]
水魔法lv [4]




◆固有技能
耐性lv2 [火焰耐性]




第一次看有魔物会用水魔法，森林中确实住着相当种类的魔物。或许，这是除了奇美拉第一次和会魔法的魔物战鬥。


我不敢大意的与水沃尔夫对峙。对方对我放出【威吓】和【咆哮】。对此我也用【威慑】和【咆哮】应付。


“オオオオオオオオオンッ！”
“啊啊啊啊啊啊啊啊啊！”


与此同时她张大嘴放出了水块。幸亏两个技能同时使用不然下个瞬间我就成了她的午饭了。


我横向避了开来身後的树木被打断啦。


“比想象的威力还高……


水的子弹贯穿敌人吧“Water Barrett”


这边也将水球作为回礼。水沃尔夫简单地避开了水球。我进一步使用魔法。


水之刃的“水刃”


魔力啊覆盖我的身体“强化”


水禾鲁夫水避开水弹后複数水刃向她袭来。避开了四个但没有有完全避开第五个体势崩坏了。


使用“身体强化”一口气说短距离然後使用“铁腕啊”用短剑向她的菊×花桶去。


水沃尔夫断气了，化为光的粒子。


下个瞬间，只剩下掉落物。在毁灭证据这之前先发动【侦察】。


“果然，被发现了吗……”


因为刚刚的动静所以注意到这了麼。看来只能让这三只永远沉默了，只要我想应该还算能打倒得的量。


我走向那三只水狼。


於是，和预想的一样，三只水沃尔夫。试着使用【鉴定】应该和刚才那只一样强吧。


从刚才的战鬥来看不用调整威力。我连续使用魔法。


水之刃的“水刃”


水的子弹贯穿敌人吧“Water Barrett”


於是，与刚才那种程度的数量所没法比的魔法飞了过去。我预计，各二十五发共五十发袭向了沃尔夫。沃尔夫用水魔法抵消或躲避应对的，但对50发到底还是没有完全抵消。


趁姿势崩溃时瞄准脖子用短剑刺死了。三个若是一起同时扑过来的话会被幹掉的吧。


我在三个魔物化成光的离子是警惕这周围。不过这次什麼也没有。


“……呼。比想的还要强很多啊（你又在装１３啦），明明感觉和奥库没多大差别，是因为会使用魔法么？”


後者比其他的强吧？不是很明白这层关係呢，办会很危险呢。再谨慎点？。


话说掉下来後还没确认状态！


【名字】成一·kisaragi 17岁


【性别】♂


【种族】人族


【水平】53


【生命力】620


【魔力】660


【力量】640


【防御】580


【持久力】550


【敏捷】490


【魔攻击】520


【防御】魔315


【运】300（开场就露骨的索要光环）


◆技能


鉴定lv [ 7 ]


伪装lv [ 7 ]


看破lv [ 6 ]


剑术lv [ 7 ]


短剑术lv [ 5 ]


魔力操作lv [8]


水魔法 lv[ 6 ]


土魔法lv[ 6 ]


无魔法lv [ 6 ]


风魔法lv [ 6 ]


火魔法lv [5 ]


光魔法lv [4 ]


回復魔法lv [ 6 ]


使役lv [ 4 ]


铁腕lv [ 5 ]


威慑lv [ 4 ]


夜裡看lv [ 6 ]


侦察lv [ 6 ]


咆哮lv [ 3 ]


魔石加工lv [5 ]


◆额外技能


贪婪的芽[ 1 ]


◆固有技能


[异种族交尾巴]


[再生lv2 ]


[耐性lv 1 ]


◆称号


[异世界渡者]


[前人未达]


水了两级，技能也成长，元素魔法除了火魔法是lv５剩下的都到了lv６。和奇美拉战鬥的时候到掉下来以後一直都在拼命的用魔法啊。


果然，还是比较在意称号吗？。好像又被套上了什麼不明觉厉的称号。


[前人未达]这应该是说先人们没有到过得地方。与迷宫内的魔物战鬥时的生命力，魔力，运之外的状态提升1.2倍


好像不知不觉入手了很便利的称号。完全没有察觉高呢。那就用着开挂福利补正顺畅的战鬥吧


“……这没说没有掉到这裡的傢伙啊。”


那就没有封印在迷宫深处的美烧酒喽，我笑啦，看来不会触发类似的剧情啦


接下来我确认一下剩下的装备。魔石和毛皮，牙一样的东西各有四套。


[水沃尔夫的魔石


沃尔夫魔石掉落率很低


注入魔力的量大概是（0 / 70 )]


[水沃尔夫的毛皮


从水沃尔夫那裡掉落的皮，是高级品。]


[水沃尔夫的牙


沃尔夫的呀掉落率更低。


轻而且结实魔力更容易流动，用这些素材制成装备能都与秘银制得装备匹敌。]


原来如此，上面的两个姑且不论牙可是相当真贵的啊，好像我的情况掉落率是100%，幸运值高的缘故吧。但即便是这些东西我也不想掉到这种地方来啊。


“现在想找那裡能够填饱肚子和找到睡得地方”（对天上喊一句就有了反正运气这麼好）


虽然在上一阶层也可以确保，但是，想在这一阶层也能够找到确保这些的场所。（这不作死么）


这裡不像山谷和悬崖可以打洞过日子。虽说可以栖息在树上但不敢保证没有会飞的魔物。


“乾脆在地上挖洞吧。但总觉得很讨厌，而且也没有学会勘测地形的技能……”


自己去特意开发？最近因为掌握了的技能堕落了啊。


（嗯，地形把握……从上面看的感觉吗？就像GPS导航那样的……魔物寻找时有魔力反应？这样的能源，确认了薄薄的魔素感受到这样的话魔素浓度的差异地形也能明白吗？）


只要有固定的影响的话就能简单学会技能吗？所以勇者技能学习快？


思考偏离了再一次重新祈祷这时第三方软件的声音流入大脑。


技能，【魔力感知】学习。


魔力感知？地形把握是不同的吧？。我说【侦察】差不多的也行啊……


“总之用一看就知道了吧，【魔力感知】！”


自己的脑中流入了大量的信息。怎麼说呢，就像被强行插上触角的感觉。与五感所感知到的，完全不同的信息强行进来了。


自己的四周——虽说只是半径30厘米左右。——魔力！能感觉。怎麼说呢，一直有像兹哇兹哇的感觉。


没办法处理吗？跟听广播时的声音一样。


“技能【思考分割】的学习。”


不明觉厉的技能又出来了。我一次我关闭【魔力感知】开始赏玩新技能。


思考分割lv１将思维分割成两份。


不明白。思考独立？怎样的事？为什麼在这个时机？


尝试和【魔力感知】一起发动。於是看到了戏剧性的变化。


“哦！？”


声音没啦。刚才，感受背上的瑟瑟感，能得到更清晰的信息。却做的事是跟刚才一样没有变化。


这个【思考分割】的技能效果实在太好啦。


如果要比喻的话……右眼睛可以独自的行动，一个人猜拳成为可能。这麼说可能比较容易理解吧？。


总之，独立的思考。这个鉴定】和【说变化啊啊。（不懂，上原文）


（总之【思考分割】是个没有副作用非常吧ｂｕｇ的技能，就把它想成劣化的夏尔吧。。。说明的部分不好翻啊】


总之托这个技能的福周围的不协调感周消失啦……话说怎麼感知地形？


“不明白啊……啊！”


鼓足幹劲使用【魔力感知】。尽管如此能感知到的範围不超过一米，有树的地方魔素的浓度会变化么。。。感了到疼痛。


意识到周围魔素浓度变化会感到疼痛。


“这个和『劣化侦察声纳』组合会变强么？”


原本劣化侦察声纳就能感知到敌人的魔力，所以应该是同样的原理。


立刻试着做然後發生了戏剧性的变化。


到刚才为止最多一米左右的侦察範围一口气变成了600米。劣化侦察声纳的精度也上升了侦察更方便了。


但是，附近有魔物会有很大的反应当只有两三米时魔力就乱套了。只要动作快一点就能躲过吧但这一点上还是侦察比较优秀吧。


总之有能力感知地形了也找到了个和藏有楼梯的差不多的大树。


“先躲在那裡吃饭吗，幸好奥库肉有富余这个垃圾一样难吃的肉感已经不想吃”


我根據肚子饿的程度看这是早饭。空腹加上疲劳的积累，吃了就睡了。


