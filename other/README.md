# toc

## 勇者「都打倒魔王了來回家吧」

- link: [勇者「都打倒魔王了來回家吧」](%E5%8B%87%E8%80%85%E3%80%8C%E9%83%BD%E6%89%93%E5%80%92%E9%AD%94%E7%8E%8B%E4%BA%86%E4%BE%86%E5%9B%9E%E5%AE%B6%E5%90%A7%E3%80%8D/)
- tags: 湯葉 , SS , 勇者 , 物語 , 魔王

### titles

- 勇者「都打倒魔王了來回家吧」
- 勇者「魔王倒したし帰るか」

## 絕對想被打倒的魔王＆絕對不想戰鬥的勇者

- link: [絕對想被打倒的魔王＆絕對不想戰鬥的勇者](%E7%B5%95%E5%B0%8D%E6%83%B3%E8%A2%AB%E6%89%93%E5%80%92%E7%9A%84%E9%AD%94%E7%8E%8B%EF%BC%86%E7%B5%95%E5%B0%8D%E4%B8%8D%E6%83%B3%E6%88%B0%E9%AC%A5%E7%9A%84%E5%8B%87%E8%80%85/)
- tags: SS , SSまとめ , あやめ速報 , 勇者 , 物語 , 魔王

### titles

- 絕對想被打倒的魔王＆絕對不想戰鬥的勇者
- 魔王「おら！勇者出て来い！！」勇者「ひぃ！！」
