

茶会是在院子裡举办是最理想的。不过，虽说是暖和的地方，2月在外边举办茶会还是有些凉飕飕的。
因此茶会，决定在客厅裡举办。
就是说，下午三点左右——我把索菲亚带到了客厅。
「利昂哥哥，在客厅裡做什麼？」
「茶会啊！」
「茶会？……难道还有其他人吗？」
牵着我的手走在一起的索菲亚停下脚步。我也不得不停了脚，看向索菲亚的脸。
「索菲亚，害怕和其他人见面吗？」
「……嗯，嗯」
「为什麼？」
「因为……父亲也好，母亲也好，都把索菲亚都当成了怪物……大家也一定会这样认为的。」
「没有那种事哦。今天茶会中的点心，都是厨房的大家为了索菲亚而準备的。」
「肯定是利昴哥哥那麼说了，所以大家才会那麼做」
索菲亚不相信我所说的话，金色的头髮因摇头而蓬乱了。
「呜呜……但是，大家看起来都不是在害怕索菲亚样子呢？还有，知道我也不害怕索菲亚的吧？」
「利昂哥哥是特别的」
「不…不是这样的」、
索菲亚突然杀害父亲的时候，我也是很害怕的。
但是，索菲亚是知道我所受的悲伤和痛苦，我知道她是代替我而愤怒。所以我不再害怕了。
总之，我是因为读不懂索菲亚在想什麼而害怕。我认为，索菲亚的父母也是这样的，所以才疏远她。
但是，要让如今的索菲亚了解这个是非常困难的。所以现在，要按照当初预定的方案来进行计划。
「索菲亚的不安我是明白的。但是不需要担心哦。因为来的是一个可以信赖的人」
「一个人？难道是克蕾尔姐姐？」
对索菲亚来说，克蕾尔姐，是可以信赖的对象啊。这是个好消息。下次茶会时，邀请克蕾尔姐吧。
但是，这次，克蕾尔姐由於撤走的工作而忙碌着。
「是爱丽丝提亚的说……还记得吗？当初和我一起潜入索菲亚房间的女孩子」
「啊……嗯。那个姐姐是一起的吗？」
「对对。和那个姐姐一起的话没问题吧？」
「呜~~·……」
看起来没有讨厌的感觉。虽然不讨厌但是有些微妙的不安啊。这样的话，在这裡一口气进攻吧。
「其实啊，爱丽丝知道各种各样的点心的食谱。因此，这次的茶会的点心也有各种各样的种类哦」
「各种各样的点心……比布丁还好吃吗？」
「那是比布丁更美味的点心哟」
「比起布丁，更美味的点心……？つ？﹏？？つ」
「啊啊啊。而且是三种」
「三…三种…」
「一个是，由甜甜的鲜奶油涂层的海绵，轻飘飘的草莓奶油蛋糕。还有的是，冰冰的，会在口中溶解出香甜的味道的香草冰淇淋。而且，还有比牛奶布丁更润滑的焦糖布丁」
「利昴哥哥！」
「嗯？怎麼了？」
「赶紧去茶会吧」
轻而易举。ヽ（╯▽╰）？
……不，这也太轻鬆了吧？这不是相当严重的创伤吗？不过这样的话，一下子治好就不是梦了。
「呐，索菲亚。如果索菲亚允许的话，可以再叫其他人吗？」
「不要！！」
「哼o（￣ヘ￣o#）……如果是爱丽丝就可以吗？」
「看到姐姐的心的时候，感觉非常的温柔。所以，那个姐姐的话，没问题……」
点心的效果和爱丽丝的信赖相乘吗。如果是这样说的理由，就不能叫上其他人了。
「我知道了，那麼就很爱丽丝一起，三人一起享受茶会吧。」
然後就这样开始了三个人的Tea Party.
「哇啊~~~这是什麼点心啊，在嘴中融化了！」
「呵呵……那叫做香草冰淇淋哦。还有，这边的蛋糕也尝尝看吧」
「嗯，好甜，非常好吃！这是什麼，这是什麼，好厲害哦，爱丽丝姐姐！」
索菲亚在专注于点心中。最初是对爱丽丝有些警戒，表情稍微有点坚硬的，不过，现在完全是被点心笼络了的形状。
虽然还没有完全地解除创伤，但是暂时还是可以摆脱只依赖於我的形式。
———一边怎麼想的一边喝起了红茶。
顺便说一下，我在前世没有自己沏红茶的爱好，所以没察觉到。不过，这个世界的红茶并不是那麼好喝。
喝了爱丽丝沏的红茶後的世界改变了……不，喝了爱丽丝的红茶後是在这个世界上重生了。
——咳咳。
总之，要沏好红茶的话，需要注重温度，对流，水等幾个重要的因素。
首先，使用沸水，从而尽可能地使温度不会在途中落下。然後和空气接触的软水，放入让对流容易發生的圆形茶壶。
这些是沏好红茶的方法……然而这个世界上有保温性的餐具之类的东西是不存在的。当然，也没有保持对流的壶。
然後最後，虽然没去调查是否存在软水，但井水不太可能包含着空气。
所以，即使使用上等的茶叶，能散發出红茶本身的味道是不可能。
——如果没有爱丽丝的外挂
通过精灵的力量，让水适当的包含空气来引起对流。然後，爱丽丝的沏茶技术，已经是可以让苏菲路的女仆长拜师的程度了。
当然，我也很在意她的沏法。
说起来，能沏出这麼好的味道的话，生产整套茶具，就可以大卖吧。而且，在古兰歇斯领土有咖啡厅什麼的也很愉快。
「那麼爱丽丝姐姐是利昴哥哥的恋人吗？」
「还在预定中哦」
……喂，在人家享受红茶的时候聊什麼呢。
「真好啊……索菲亚之前有着和利昴哥哥结婚的预定，不过现在没了啊…？？﹏？？ 」
空气明显的被冻住了
说着说着就投下了炸弹，这孩子啊。虽然结果是她说的那样，不过，这应该有着更加政治方面的说法。
话说回来，还不是爱丽丝说了奇怪的话啊。要怎样才能回到之前的气氛啊
“没关係的，打起精神来”———之类的？不，这关係大了。“你觉得你能能赢过我吗？”———挑衅干个毛啊！
那麼如果……“你能找到更好的人的”———之类的？这个完全出局了啊。不管怎麼想，在索菲亚现在的立场上说这个的话，完全就是嘲讽。
坦率地说，在这种状况下我已经被将死了。不管什麼样的答案都会踩到地雷的样子。
我正这麼觉得的时候，爱丽丝露出了满面的笑容。
「我，要不然我就和索菲亚酱合作吧？」-----这麼说
……啊，什麼意思？
爱丽丝是打算为了索菲亚而献身吗？还是说，只是以轻浮的心情说的吗？如果是这样的话，会伤害索菲亚的。
我在混乱的时候，两个人的对话也在进行着。
「爱丽丝姐姐，这样子好吗？」
「我说过了的吧。我还是预定。利昂还在对实妹出手的事感到反抗。所以，如果他对还是孩子的义妹出手了的话，他的伦理观也应该会被削弱吧。」
总感觉她们在考虑些很恐怖的事呜呜呜呜！?
话说，别那麼轻易的说出实妹啊。别人还不知道转生的事啊！
「虽然不知道怎麼回事……真的可以这样吗？」
「当然呢，我们两个人一起攻略利昴吧！」
不行，虽然我不是很清楚，但是，我还是知道这是不好的事。正想逃出这个场所的时候，突然门被敲了。
「进来吧！」
马上打了招呼。
如今索菲亚在这裡，我应该走出门到外边听事情的。
但是，我真的很焦急，转不过弯。岂止如此，我还想着有人可以打破这个含糊不清状况。
其结果，索菲亚不适着看向女仆进来。然後，从座位上站了起来，并藏在我的背後。
「……那个，可以说了吗？」
「啊，抱歉，有什麼事吗？」
「是的，帕特里克大人来拜访索菲亚大小姐来了」
在我背後的索菲亚打了个寒颤
「索菲亚？」
小声地朝着索菲亚问道。
「不想见，回去！」
「是讨厌的人吗？」
「完全不听索菲亚的话，而且只为了自己而着想。所以才讨厌。」
原来如此。好像不是很好的性格啊。而且，对现在的索菲亚是绝对不想见面的类型。
「索菲亚好像不想见他的样子，能找些理由拒绝他吗？」
我代替索菲亚告诉了女仆。
「这…这个」
「对方很麻烦吗？」
「……是的。帕特里克大人的家，卢德韦尔子爵家。本来是比伯爵家的苏菲路家还要低，不过，因为是库兰普侯爵家的分家，所以……」
偏偏是库兰普侯爵家的关係者啊。因为废弃了克蕾尔姐的婚约，所以不太想和它有什麼关联。
「知道对方来这的目的吗？」
「嗯……那个」
女仆看了一眼索菲亚。不管怎麼样这裡也是苏菲路家啊。我向在背後的索菲亚寻求，得到了许可。
「索菲亚的许可已经得到了」
「如果是这样的话，其实…」
女仆告诉我的是，帕特里克向索菲亚求婚的事实。
之前因为索菲亚有着婚约者所以很安分。不过，不知道从哪听到了订婚被废弃了後，到这裡来了。
顺便说一句，年龄听说是比我大四岁的十四岁。索菲亚还是七岁。这不是什麼政略婚姻，而是向索菲亚自身求婚，完全是萝莉控。
不是看中地位，而是索菲亚自身的话就是评价对象。不过，无论是哪个如果索菲亚讨厌的话，就出局了。
「确认一下，索菲亚不管怎麼样也不想见吗？」
「…杀了他的话就可以」
「这肯定是不行的啊」
用可爱的脸做除了如此可怕的发言的这个孩子。
而且完全听出在开玩笑，真是太可怕了。那个啊？杀了父亲後，充满了罪恶感，但是，如果是别人的话，就没关係吗？
总…总之，千万别让他们见面啊
「很抱歉，请告诉他日後再来吧。如果很困难的话，向埃里克传达现在所说的话吧」
「好的，明白了」
女仆敬了敬礼，然後离开了房间
「索菲亚？」
虽然挂着声音，但是索菲亚的表情很硬。明明已经回到了以前那开朗的表情，现在却又回到了昨天的状态。
呜。帕特里克这傢伙，来的时机太坏了。
「已经回去了，不用害怕也没问题的吧？」
「……但是，他一定会再来的。」
那是……大概是那样吧。关於我和索菲亚的婚约废弃的事是前些日子。然後就飞出来了，想必是非常执着的吧。
而且……有库兰普侯爵的靠山的说啊。因为卡洛斯刚去世，埃里克还年轻。说不定强行求婚的话也许会拒绝不了了。
放着不管的话就糟了。索菲亚对我的依存增加了，我思考着且犹豫着。不过，现在这样的话我就不得不背在背上了啊。
「索菲亚，稍微听一下吧。我明天打算回去，古兰歇斯领地。」
「嗯……利昂哥哥，你要回去了吗？你又马上会回来了吧？」
「我想，因为有各种各样的事情，所以暂时不能来这裡。」
这样说的瞬间，索菲亚快要哭了。看见了那个的我马上说「所以——」继续着
「要成为我的妹妹吗？」
「额？……怎麼回事。因为利昴哥哥要继承古兰歇斯家的事业，所以不能成为苏菲路家的养子不是吗？」
「对。所以，索菲亚要不要成为古兰歇斯家的养子呢？埃里克先生说了，“如果索菲亚希望的话，那就可以得到许可。”所以要看索菲亚了。」
「……如果成为养子的话，就可以和利昂哥哥一直在一起吗？」
「是这样的」
「那就可以，成为利昴哥哥的妹妹。然後一起回去。」
「……明白的。那麼索菲亚现在开始正式的是我的义妹。索菲亚受威胁的话，这样我会保护你。绝对，不让索菲亚伤心。」
回到古兰歇斯领後救助成为了奴隷的孩子，并且救助将要成为奴隷的孩子。
然後创造出所有人可以在古兰歇斯领幸福生活的环境。
绝对不会自重，就算是以库兰普侯爵家为对手也不怂，要让古兰歇斯领发展到那样的程度。
我的幸福一定就在那之後。
