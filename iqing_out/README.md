# toc

## 神明大人的魔法使

- link: [神明大人的魔法使](%E7%A5%9E%E6%98%8E%E5%A4%A7%E4%BA%BA%E7%9A%84%E9%AD%94%E6%B3%95%E4%BD%BF/)
- link_source: [神明大人的魔法使](../iqing/%E7%A5%9E%E6%98%8E%E5%A4%A7%E4%BA%BA%E7%9A%84%E9%AD%94%E6%B3%95%E4%BD%BF/)
- tags: iqing , node-novel , 冒险 , 奇幻 , 异界 , 白银 , 连载 , 青春

## 魔王神官和勇者美少女

- link: [魔王神官和勇者美少女](%E9%AD%94%E7%8E%8B%E7%A5%9E%E5%AE%98%E5%92%8C%E5%8B%87%E8%80%85%E7%BE%8E%E5%B0%91%E5%A5%B3/)
- link_source: [魔王神官和勇者美少女](../iqing/%E9%AD%94%E7%8E%8B%E7%A5%9E%E5%AE%98%E5%92%8C%E5%8B%87%E8%80%85%E7%BE%8E%E5%B0%91%E5%A5%B3/)
- tags: iqing , node-novel , 异界 , 无限 , 白银 , 连载
