被引燃的侯爵邸冒起了熊熊大火。背对着大火与芙兰激烈交锋的阿修德纳侯爵，还是以那一副不曾变化的能面表情發出卑劣的笑声。

「嘿哈哈哈哈！不错啊小鬼！这种痛楚我已经幾百年都没有感受过了！」

　幾百年前的痛楚？果然这傢伙不是阿修德纳侯爵本人。

「你到底是，什麼东西？」
「啊，到底是什麼啊？我们还想听你说说呢。我们到底是什麼东西啊？」

　一番让人云里雾里的言辞，却没有不可思议或是想要岔开話题的感觉。这些話简直好像是发自真心的一样。

「是100年前、500年前、还是1000年前。我已经分不清了啊，只有被神圣秩序（ホーリー・オーダー）那个**破坏的事情还依稀记在脑子裡！」

　神圣秩序？想起来了！那是爱丽丝提亚跟我们说过的神剑之一。圣灵剑神圣秩序。专为破坏狂信剑狂热者fanatics而打造的神剑。
　这下已经可以盖棺定论了。这次事件的主谋和狂热者脱不开关系。不对，岂止如此，潜藏在侯爵体内的东西，恐怕正是狂热者。
　但是还有疑问。侯爵手握的是疑似狂信剑，是赝品。还是说他把真品藏在什麼地方了吗？
　而且話又说回来，狂热者居然和我一样是智慧武器……。这样一来，或许侯爵只是傀儡，黑幕有可能是狂热者。

「哈啊啊！」
「咕嘿！」

　当然，这番对話是在极速拼杀之中进行的。是不论格鲁贝鲁多还是伊利安忒都插不上手的激烈交锋。
　剑圣术：Lv10的阿修德纳侯爵，与还未能将剑王术穷就至极的芙兰。结果，两人的剑术水平基本不相上下。

「……你就是“狂热者fanatics”吗？」
「“狂热者fanatics”？这麼一说我好像是有这麼个名字来着……？呐，我们是那个“狂热者fanatics”吗？」
「是我在问你。」
「咕哈哈哈！也是啊！」

　話说回来，“我们”？不是“我”而是“我们”？

「我、们？」
「啊啊，我们啊。数量众多但只有一个个体。一个个体却包含我们所有人。全部都是我们！嘿哈哈哈！你那把剑裡面，不也有我们在吗！」

　什麼？在我裡面？

「怎麼回事？」
「连自我都没有，不过是从我们这裡分离的碎片，但那也是我们没错！你那把魔剑，连吸收魔力都可以做到吗？」

　难道说我通过共食吸收了疑似狂信剑的力量吗？疑似狂信剑裡面恐怕渗入了些许狂热者的魔力。
　记得狂热者的能力确实是，将斩到的对手的意识和记忆与自己同一化。这样一来，拥有複数的意识也并不奇怪。
　我、真的没问题吗？

「对了！把那把剑交出来！那把剑是用山铜打造的对吧？有那把剑的話，或许能治癒我们的伤口！」
「我拒绝！」
「咔哈哈哈！話说回来，你还挺厉害的嘛！居然能和这个特别的素体缠斗到现在！」

　又岔开了話题。它到底是状态太焦躁了，还是太能说了呢，总之真的好烦。
　芙兰一边挥剑朝伯爵猛砍，一边提出疑问。侯爵一剑架住我的我的攻势，向周围散發出猛烈的冲击波，在此同时还一脸清爽的回答了提问。

「怎麼回事？」
「我们花了将近40多年调整，这傢伙可是特别的素体哟。」
「40年？调整？」
「对的。想得到具有超越性力量的素体，只有一点点花费时间，配上药物和我的能力精心改造才能制作出来的啦！真是可恶啊！以前的話，不管多强的部队都能制作出来！现在也就是给予两三个适应性较好的技能这种程度！」

　也就是说，狂热者并不是只能把别人的力量统合起来，连给予技能也可以做到吗？若是能随意给予技能和经验的話，想要量产那些最可怕的士兵当然是可行的。
　和这个阿修德纳侯爵具有同等力量的军团，在狂热者的意识之下完美的协同作战。而且考虑到狂热者的技能，这样的士兵还可以随意补充。这样一来就真的太危险了。

「这傢伙的兒子们，家臣的兒子们，再加上这个城市裡杂七杂八的傢伙，我可是做了各种各样的实验啊！」

　难道是在说赛鲁迪欧和奥古斯特他们吗？芙兰提出疑问。

「赛鲁迪欧？奥古斯特？」
「哦哦？你认得他们啊？结果给他们的精神操作系技能和独有技能都没有回收到啊！和那些可以随便补充的剑术或是魔术不同，那一手技能可是极其贵重的啊！」

　也就是说，赛鲁迪欧和奥古斯特持有的技能都是狂热者给予的。

「在这一点上，这傢伙可是花费了40年一直在改造啊！我们持有的技能大抵都能移植！嘿哈哈哈哈！」

　侯爵發出哄笑，毫无意义的多嘴多舌。好像在战鬥中不唧唧喳喳的讲話就很无聊一样。不过也多亏这一点，我们得到了大量有用的信息，不过，到底要观望到什麼时候就很难说了。虽然现在看起来势均力敌，但是考虑到侯爵使用的强夺技能以及闪华迅雷的消耗，芙兰的生命力和魔力都在一点点减少。
　必须在某个节点停止情报收集，一口气分出胜负才行。
　但是，率先行动的却是阿修德纳侯爵那边。只是互砍根本没完没了，不知是理解了这一点，还是说話说烦了，侯爵拉远了和芙兰的距离。

「比如说这种事也可以做到哟！熔岩之墙（マグマ・ウォール）！寒冰射击（アース・シューター）！然後剑之音波（ソード・ソニック）！」

　侯爵朝着冒出来的熔岩之墙猛力一击，之後爆裂的熔岩化为散弹降注在芙兰这边。而且为了施加追击，伯爵发动了剑圣术派生的冲击波技能。
　即便躲开了大地魔术产生的弹丸和冲击波，熔岩之雨带来的伤害是免不了的。发动障壁挡住攻击又会使动作迟钝。想要完全回避大範围的攻击，我们的动作一定会变得单调化。
　而侯爵则似乎正盯着我们的漏洞。
　但是，盯着对方的漏洞这一点这边也是一样。我们反过来利用了这次攻击。

「冰墙（アース・ウォール）！」

　通过大地之墙遮住视线――。

「哈啊啊！」
「嘿嘿嘿！转移吗！」
「切！」

　察知能力也和怪物一样！从背後发动的攻击也被他轻易挡开了！但是，这一点我们也考虑到了！

『芙兰！我这边準备OK！』
「嗯！哈啊啊！」

　芙兰为了封住侯爵的行动而再次斩击。侯爵因为长期战占有优势，便兴冲冲地回击过来，完全没想到会中我们的圈套。

「好啊！还想和我对砍吗？」
「不对。」
『吃我这招！』

　此时，我再次发动的康娜卡姆依降注下来。而且是经过了收束的威力增强版。

「怎，连自己都包含在内吗？」
「别想跑！」
「可恶！」

　那傢伙在和芙兰交锋的时候，根本没有餘裕防住这个攻击。芙兰拥有雷鸣无效技能，而我也可以发动次元跳跃（ディメンジョン・シフト）避开这次攻击。

「哈啊啊！」
「小鬼！」
