门锁被摆弄着發出了咯咯的响声。

刚才进房间的时候没有锁门呢……

想到这裡我连忙从床上坐起了身，用袖子擦了擦眼睛。

本来还想整理好衬衫，但是门已经被打开了。

“我来了哟……欸？克……克莉斯？！”

大大方方地从门口走了进来，就像是自己的房间一样。

这时我才发现进来的人是奥菲莉亚，她也被分到这个宿舍来了吗？

再怎麼说这也太巧了一点。

而且之前看过的名单上好像没有她的名字。

“……午安，奥菲莉亚。”

一时间不知道该说什麼的我生硬地打了招呼。

“什麼午安啊……你又哭过了吗？衣服也是皱巴巴的。”

她一下丢掉了手上的行李，跑到了我的面前。

“……并没有……只是稍微摔倒了。”

听到我的话她无言了。

皱着眉头，看上去非常生气，随後便举起了小小的拳头。

要被打了……

我下意识侧过脸闭上了眼睛。

可是想象的疼痛却迟迟没有到来，但当我再次睁开眼睛的时候……

啪~~

她纤细的手重重地扇过我的脸颊……

片刻之後脸上火辣辣地发疼。

“你，你做什麼！……奥菲莉亚。”

“……”

“呜呜，奥菲莉亚……呜呜。”

不成器地哭了出来，刚才明明还一直忍耐着，可是现在脸上的疼和心裡的委屈融合在一起变得无法忍受了。

捂着脸，泪水却沿着手指改变了流动的方向滴在了大腿上。

刹那间的凉意让整个身体都冷了下来。

“……我就那麼不值得信任吗！！？”

丝毫不在意还在宿舍或者是我现在不堪的模样，她大声咆哮着。

“……呜呜…”

“……如果是这样的话，我就离开这个国家好了！！本以为我们是同生共死过的伙伴，是战友啊！但是你居然编出这样拙劣的谎言出来，啊啊！你是个爱撒谎的女孩早就知道了。但是没想到无论对自己有利或者不利的谎言你都能编呢！”

说谎什麼的……我也有自己的私事啊！为什麼要全部告诉你啊！？

想要这样以不输给她的分贝吼出声来……

“克莉斯受到的痛苦是我的两倍……”当时作为奴隷在牢笼裡时的记忆却浮现了出来，那麼现在重重打了我的奥菲莉亚是不是也受到了加倍的疼痛呢。

“……不关你的事……奥菲莉亚不是这个宿舍的学生吧，再不出去妾身就让宿舍长赶你出去了。”

试着用正论让她离开房间。

但是奥菲莉亚一点也没有理睬的意思。

“嗯？我可没听说有哪条校规禁止女生出入女生宿舍呢。”

她交叉着双手，丝毫不在意我的威胁。

“那麼……离开妾身的房间总好了吧！！”

“不出去！”

“别缠人啊！跟你没有关係，少多管闲事啊！”

结果心裡的焦躁越来越扩散，真的喊了出来，实在是烦透了，想要一个人安静一下。

求求你了，奥菲莉亚，快离开吧……明天一定会好好向你道歉的。

“是吗？我知道了……本来早上以为你只是身体不舒服而已，现在反应这麼大我终於明白了呢……是被那个什麼艾文少爷给甩了吧！”

“！！”

她带着有些戏谑的笑容，接着说道。

“这样一想就都明白了，早上在仪式上时不时俯下身子将脑袋藏在桌子下……哼，简直像是躲避着谁的目光一样。”

她的这种语气平时只是开玩笑而已，但是现在却如同荆棘一般缠绕在我的肌肤上，让人连鸡皮疙瘩都冒了出来。

一如既往地敏锐，这样的话一定会被猜到的吧。

我怒瞪着奥菲莉亚，希望她不要接着说下去。

“看来那个少爷站在那个能看到你的位置……”

“别……说了……”

“而你则是害怕被看到，那一定是在入学式上发现了什麼吧……嚯嚯，当时被在最显眼的位置和那个皇女打得火热的男生是谁呢~~”

“不要说了……啊。”

我继续埋下头表情幾乎已经从愤怒变成哀求了，别再故意把那些话说给我听了。

“好啊，克莉斯……我们打过赌吧，现在到了该兑现我的报酬的时候了。”

她俯下腰盯着我，没有半点开玩笑的氛围。

“嗯……是妾身输了呢，你想让妾身做什麼呢……还是说想要什麼，随便拿走吧……”

“…………”

不想对上她的眼睛，仿佛连心灵也会被看穿，我沉默地低下了头。

而奥菲莉亚沉默了一会，缓缓说道。

“今天不许从这裡出去……”

“欸……？”

她转开了视线叹了一口气。

“到今天24时为止……你如果离开这个房间我们就不再是朋友了。”

说完将行李踢进了房间内，重重地带上了门。

奥菲莉亚在燃烧着，我能感觉得到……

从未见过如此愤怒的她。

虽然刚才最後一直装作冷漠的样子，但是我看见了她微微发抖的双肩。

我的脸还是很痛……

她究竟用了多大的力量去打了呢，但是自己现在却没有去照镜子的心情。

小时候多次被父亲狠狠地揍过，父亲是社长对待谁都十分严厲，从小对我就实行英才教育。

可惜我不是块好的材料，经常会惹到他。

那时的我只是咬着嘴唇，等到父亲去上班才肯一口气大哭出来。心想如果自己是女生的话，即使哭泣也不会让人觉得软弱。如果自己是个女生的话，即使软弱也会有人来安慰，如果再可爱一点的话恐怕父亲根本不忍心揍我吧。

我嫉妒着原来世界上的少女们……曾经确实是这样。

对於自己变成了女生这件事我从未有过一丝恐慌，以为即使变得任性，爱哭也会被人当做是可爱，可以无条件被原谅。

脱掉了身上的衣服，我走进了浴室。

稍微灌注了魔力，魔导器里的热水喷涌而出……

看着自己的双手，纤长而白皙，不过本来在宫廷式被仆人们护理得很好的指甲却变得有些粗糙了。

是因为冒险了一年的原因吗……

太天真了……

女孩子也有不得不承担的责任和重量，什麼都以一句自己是女孩所以可以逃避的话实在是太天真了。

“妾身……还真是弱啊。”

确实变成了女孩子了呢，身高和胸部最近也开始加速成长了起来。

低头看向胸前，究竟是什麼时候开始渐渐视野已经被隆起的山丘挡住了。

但是我依然很弱……这样软弱的不能被称为女性。

只是个没长大的孩子罢了。

以前的自己虽然是个NEET废人……但是至少还没有把自己的麻烦添给其他人。

结果现在连那时都不如了吗？

不，大概只是因为现在有人愿意接近自己而表现出了过度的依赖吧。

说着是自己的事情，却将心情写在脸上让他们担心。

我连那个时候都不如了吗……

我并不是什麼30多岁的成年人，而是个把过去都忘掉的笨蛋。

“对不起……奥菲莉亚。”

我的事情要由自己来决定。